# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.19.6"></a>
## [0.19.6](https://github.com/pbsa/streamersedge-gui/compare/v0.19.5...v0.19.6) (2020-02-27)


### Bug Fixes

* spelling mistakes ([#342](https://github.com/pbsa/streamersedge-gui/issues/342)) ([28d6ad8](https://github.com/pbsa/streamersedge-gui/commit/28d6ad8))



<a name="0.19.5"></a>
## [0.19.5](https://github.com/pbsa/streamersedge-gui/compare/v0.19.4...v0.19.5) (2020-02-24)


### Bug Fixes

* possible fix for se-146 ([#341](https://github.com/pbsa/streamersedge-gui/issues/341)) ([705ae4c](https://github.com/pbsa/streamersedge-gui/commit/705ae4c))



<a name="0.19.4"></a>
## [0.19.4](https://github.com/pbsa/streamersedge-gui/compare/v0.19.3...v0.19.4) (2020-02-21)


### Bug Fixes

* fixing Error message spacing SE-147 ([#335](https://github.com/pbsa/streamersedge-gui/issues/335)) ([db8354c](https://github.com/pbsa/streamersedge-gui/commit/db8354c))
* fixing scroll bar ([#334](https://github.com/pbsa/streamersedge-gui/issues/334)) ([bfaee54](https://github.com/pbsa/streamersedge-gui/commit/bfaee54))
* ppy min 12 characters long error ([#336](https://github.com/pbsa/streamersedge-gui/issues/336)) ([df99af0](https://github.com/pbsa/streamersedge-gui/commit/df99af0))
* remove donate button for paid challenges ([#337](https://github.com/pbsa/streamersedge-gui/issues/337)) ([07333c5](https://github.com/pbsa/streamersedge-gui/commit/07333c5))



<a name="0.19.3"></a>
## [0.19.3](https://github.com/pbsa/streamersedge-gui/compare/v0.18.0...v0.19.3) (2020-02-20)


### Bug Fixes

*  clear form on success or peerplays modal opened STRM-1090 ([#319](https://github.com/pbsa/streamersedge-gui/issues/319)) ([dc5c270](https://github.com/pbsa/streamersedge-gui/commit/dc5c270))
* add idea image before connect text ([#309](https://github.com/pbsa/streamersedge-gui/issues/309)) ([c5291d8](https://github.com/pbsa/streamersedge-gui/commit/c5291d8))
* add warning for unlinking of all accounts SE-96 ([#298](https://github.com/pbsa/streamersedge-gui/issues/298)) ([6f19e92](https://github.com/pbsa/streamersedge-gui/commit/6f19e92))
* allow not logged in user to view the challenge details STRM-1063 ([#315](https://github.com/pbsa/streamersedge-gui/issues/315)) ([1d2e7f3](https://github.com/pbsa/streamersedge-gui/commit/1d2e7f3))
* banned modal when banned user creates challenge or donates SE-123 ([#295](https://github.com/pbsa/streamersedge-gui/issues/295)) ([4c94c65](https://github.com/pbsa/streamersedge-gui/commit/4c94c65))
* difference between peerplays balance on redeem page ([#328](https://github.com/pbsa/streamersedge-gui/issues/328)) ([5678985](https://github.com/pbsa/streamersedge-gui/commit/5678985))
* display error message when updating profile ([#317](https://github.com/pbsa/streamersedge-gui/issues/317)) ([dcf089b](https://github.com/pbsa/streamersedge-gui/commit/dcf089b))
* display transfer fee received from blockchain ([#297](https://github.com/pbsa/streamersedge-gui/issues/297)) ([9e10f6f](https://github.com/pbsa/streamersedge-gui/commit/9e10f6f))
* edit account link not working SE-141 ([#307](https://github.com/pbsa/streamersedge-gui/issues/307)) ([5ca1cb8](https://github.com/pbsa/streamersedge-gui/commit/5ca1cb8))
* error message UI ([#329](https://github.com/pbsa/streamersedge-gui/issues/329)) ([60315d0](https://github.com/pbsa/streamersedge-gui/commit/60315d0))
* error message when description is more than 1000 characters ([#308](https://github.com/pbsa/streamersedge-gui/issues/308)) ([8dc1a06](https://github.com/pbsa/streamersedge-gui/commit/8dc1a06))
* error modal on peerplays account missing in challenge details SE-20 ([#303](https://github.com/pbsa/streamersedge-gui/issues/303)) ([63f69e4](https://github.com/pbsa/streamersedge-gui/commit/63f69e4))
* fix insufficient amount error SE-143 ([#306](https://github.com/pbsa/streamersedge-gui/issues/306)) ([9e4904c](https://github.com/pbsa/streamersedge-gui/commit/9e4904c))
* fixing add funds modal SE-128 ([#316](https://github.com/pbsa/streamersedge-gui/issues/316)) ([2b9ea0b](https://github.com/pbsa/streamersedge-gui/commit/2b9ea0b))
* fixing Challenge details UI SE-106 ([#320](https://github.com/pbsa/streamersedge-gui/issues/320)) ([814d664](https://github.com/pbsa/streamersedge-gui/commit/814d664))
* fixing duplicate entries SE-151 ([#326](https://github.com/pbsa/streamersedge-gui/issues/326)) ([d4b10c1](https://github.com/pbsa/streamersedge-gui/commit/d4b10c1))
* fixing error box UI SE-148 ([#324](https://github.com/pbsa/streamersedge-gui/issues/324)) ([3e0acbc](https://github.com/pbsa/streamersedge-gui/commit/3e0acbc))
* fixing error message SE-147 ([#330](https://github.com/pbsa/streamersedge-gui/issues/330)) ([3eed294](https://github.com/pbsa/streamersedge-gui/commit/3eed294))
* fixing error text SE-147 ([#323](https://github.com/pbsa/streamersedge-gui/issues/323)) ([8406447](https://github.com/pbsa/streamersedge-gui/commit/8406447))
* fixing navigation to dashboard SE-118 ([#304](https://github.com/pbsa/streamersedge-gui/issues/304)) ([69bec20](https://github.com/pbsa/streamersedge-gui/commit/69bec20))
* fixing navigation to dashboard SE-118 ([#305](https://github.com/pbsa/streamersedge-gui/issues/305)) ([8f33bc5](https://github.com/pbsa/streamersedge-gui/commit/8f33bc5))
* fixing navigation to dashboard SE-118 ([#305](https://github.com/pbsa/streamersedge-gui/issues/305)) ([779592c](https://github.com/pbsa/streamersedge-gui/commit/779592c))
* fixing profile upload error SE-146 ([#322](https://github.com/pbsa/streamersedge-gui/issues/322)) ([9625911](https://github.com/pbsa/streamersedge-gui/commit/9625911))
* fixing spaces for challenge name SE-150 ([#325](https://github.com/pbsa/streamersedge-gui/issues/325)) ([8b8e42b](https://github.com/pbsa/streamersedge-gui/commit/8b8e42b))
* fixing Usertype in admin dashboard SE-139 ([#312](https://github.com/pbsa/streamersedge-gui/issues/312)) ([75ee8ca](https://github.com/pbsa/streamersedge-gui/commit/75ee8ca))
* issue with redemption when user has logged in with peerplays global ([#318](https://github.com/pbsa/streamersedge-gui/issues/318)) ([faf8cea](https://github.com/pbsa/streamersedge-gui/commit/faf8cea))
* live icon on twitch missing ([#310](https://github.com/pbsa/streamersedge-gui/issues/310)) ([65d1033](https://github.com/pbsa/streamersedge-gui/commit/65d1033))
* logout user when unauthorized received ([#321](https://github.com/pbsa/streamersedge-gui/issues/321)) ([f944f3b](https://github.com/pbsa/streamersedge-gui/commit/f944f3b))
* only allow one decimal point with 2 digits after that in amount ([#331](https://github.com/pbsa/streamersedge-gui/issues/331)) ([fd9324b](https://github.com/pbsa/streamersedge-gui/commit/fd9324b))
* profile upload error message not displayed properly ([#314](https://github.com/pbsa/streamersedge-gui/issues/314)) ([6cbb0b4](https://github.com/pbsa/streamersedge-gui/commit/6cbb0b4))
* sorting by total donation and active challenges after search clear ([#302](https://github.com/pbsa/streamersedge-gui/issues/302)) ([67b243c](https://github.com/pbsa/streamersedge-gui/commit/67b243c))
* time before the current time is not supported ([#327](https://github.com/pbsa/streamersedge-gui/issues/327)) ([38629eb](https://github.com/pbsa/streamersedge-gui/commit/38629eb))



<a name="0.18.0"></a>
# [0.18.0](https://github.com/pbsa/streamersedge-gui/compare/v0.17.0...v0.18.0) (2020-02-11)


### Bug Fixes

* add tooltip ([#291](https://github.com/pbsa/streamersedge-gui/issues/291)) ([d023638](https://github.com/pbsa/streamersedge-gui/commit/d023638))
* adding date time to challenge card [#103](https://github.com/pbsa/streamersedge-gui/issues/103) ([#277](https://github.com/pbsa/streamersedge-gui/issues/277)) ([ea29efe](https://github.com/pbsa/streamersedge-gui/commit/ea29efe))
* close icon ([#288](https://github.com/pbsa/streamersedge-gui/issues/288)) ([a700576](https://github.com/pbsa/streamersedge-gui/commit/a700576))
* fixed report api call SE-117 ([#300](https://github.com/pbsa/streamersedge-gui/issues/300)) ([531b2db](https://github.com/pbsa/streamersedge-gui/commit/531b2db))
* fixing active challenges on profile [#99](https://github.com/pbsa/streamersedge-gui/issues/99) ([#278](https://github.com/pbsa/streamersedge-gui/issues/278)) ([dbd8211](https://github.com/pbsa/streamersedge-gui/commit/dbd8211))
* fixing arrows on firefox SE-70 ([#285](https://github.com/pbsa/streamersedge-gui/issues/285)) ([a024a60](https://github.com/pbsa/streamersedge-gui/commit/a024a60))
* fixing cancel, redeem and error modal [#22](https://github.com/pbsa/streamersedge-gui/issues/22) ([#274](https://github.com/pbsa/streamersedge-gui/issues/274)) ([f28161e](https://github.com/pbsa/streamersedge-gui/commit/f28161e))
* fixing Challenge Confirm Modal SE-106 ([#296](https://github.com/pbsa/streamersedge-gui/issues/296)) ([21b0791](https://github.com/pbsa/streamersedge-gui/commit/21b0791))
* fixing close icon SE-76 ([#280](https://github.com/pbsa/streamersedge-gui/issues/280)) ([cb9df83](https://github.com/pbsa/streamersedge-gui/commit/cb9df83))
* fixing donate input [#111](https://github.com/pbsa/streamersedge-gui/issues/111) ([#279](https://github.com/pbsa/streamersedge-gui/issues/279)) ([cacb693](https://github.com/pbsa/streamersedge-gui/commit/cacb693))
* fixing forgot password result SE-121 ([#290](https://github.com/pbsa/streamersedge-gui/issues/290)) ([28ef170](https://github.com/pbsa/streamersedge-gui/commit/28ef170))
* fixing icon cut [#107](https://github.com/pbsa/streamersedge-gui/issues/107) ([#275](https://github.com/pbsa/streamersedge-gui/issues/275)) ([4fdc064](https://github.com/pbsa/streamersedge-gui/commit/4fdc064))
* fixing peerplays create profile SE-50 ([#299](https://github.com/pbsa/streamersedge-gui/issues/299)) ([ca5ccd6](https://github.com/pbsa/streamersedge-gui/commit/ca5ccd6))
* fixing Peerplays login SE-84 ([#286](https://github.com/pbsa/streamersedge-gui/issues/286)) ([418ac22](https://github.com/pbsa/streamersedge-gui/commit/418ac22))
* fixing right menu image SE-56 ([#287](https://github.com/pbsa/streamersedge-gui/issues/287)) ([2dd065f](https://github.com/pbsa/streamersedge-gui/commit/2dd065f))
* fixing search hang SE-120 ([#283](https://github.com/pbsa/streamersedge-gui/issues/283)) ([e942bfc](https://github.com/pbsa/streamersedge-gui/commit/e942bfc))
* fixing search on admin dashboard SE-114 ([#276](https://github.com/pbsa/streamersedge-gui/issues/276)) ([b0ae662](https://github.com/pbsa/streamersedge-gui/commit/b0ae662))
* fixing sorting date SE-113 ([#284](https://github.com/pbsa/streamersedge-gui/issues/284)) ([efb5152](https://github.com/pbsa/streamersedge-gui/commit/efb5152))
* fixing text peerplays auth SE-95 ([#289](https://github.com/pbsa/streamersedge-gui/issues/289)) ([7c5aeeb](https://github.com/pbsa/streamersedge-gui/commit/7c5aeeb))
* SE-108 Show banned modal in PP Login when user is banned ([#294](https://github.com/pbsa/streamersedge-gui/issues/294)) ([cf901c5](https://github.com/pbsa/streamersedge-gui/commit/cf901c5))
* STRM-104 popular challenges not sorting properly ([#281](https://github.com/pbsa/streamersedge-gui/issues/281)) ([3a621d3](https://github.com/pbsa/streamersedge-gui/commit/3a621d3))
* strm-1051 live challenges not showing on the dashboard ([#292](https://github.com/pbsa/streamersedge-gui/issues/292)) ([da2fc93](https://github.com/pbsa/streamersedge-gui/commit/da2fc93))
* strm-1059 remove extra idea images, text change and ui fixes ([#282](https://github.com/pbsa/streamersedge-gui/issues/282)) ([5ed92ae](https://github.com/pbsa/streamersedge-gui/commit/5ed92ae))


<a name="0.17.0"></a>
# [0.17.0](https://github.com/pbsa/streamersedge-gui/compare/v0.15.0...v0.17.0) (2020-02-06)

### Bug Fixes

* add done button ([#267](https://github.com/pbsa/streamersedge-gui/issues/267)) ([c389a97](https://github.com/pbsa/streamersedge-gui/commit/c389a97))
* add reset button image ([#268](https://github.com/pbsa/streamersedge-gui/issues/268)) ([5aac622](https://github.com/pbsa/streamersedge-gui/commit/5aac622))
* adding depositOp to Redeem [#688](https://github.com/pbsa/streamersedge-gui/issues/688) ([c3d5a65](https://github.com/pbsa/streamersedge-gui/commit/c3d5a65))
* adding error message to time SE-65 ([#257](https://github.com/pbsa/streamersedge-gui/issues/257)) ([f8092f0](https://github.com/pbsa/streamersedge-gui/commit/f8092f0))
* appropriate message when amount is less than 1 ([#254](https://github.com/pbsa/streamersedge-gui/issues/254)) ([c8d3ffc](https://github.com/pbsa/streamersedge-gui/commit/c8d3ffc))
* fixing live challenges in user profile [#99](https://github.com/pbsa/streamersedge-gui/issues/99) ([#271](https://github.com/pbsa/streamersedge-gui/issues/271)) ([8a31f33](https://github.com/pbsa/streamersedge-gui/commit/8a31f33))
* fixing peerplays login validation [#85](https://github.com/pbsa/streamersedge-gui/issues/85) ([#264](https://github.com/pbsa/streamersedge-gui/issues/264)) ([1bfd22a](https://github.com/pbsa/streamersedge-gui/commit/1bfd22a))
* fixing pubg linking failed [#51](https://github.com/pbsa/streamersedge-gui/issues/51) ([#258](https://github.com/pbsa/streamersedge-gui/issues/258)) ([ed8b69f](https://github.com/pbsa/streamersedge-gui/commit/ed8b69f))
* initial commit [#688](https://github.com/pbsa/streamersedge-gui/issues/688) ([8c4e93c](https://github.com/pbsa/streamersedge-gui/commit/8c4e93c))
* integrating api [#688](https://github.com/pbsa/streamersedge-gui/issues/688) ([3735564](https://github.com/pbsa/streamersedge-gui/commit/3735564))
* left right menu ([#260](https://github.com/pbsa/streamersedge-gui/issues/260)) ([69d45ff](https://github.com/pbsa/streamersedge-gui/commit/69d45ff))
* make UI for Redeem page [#688](https://github.com/pbsa/streamersedge-gui/issues/688) ([2de1be3](https://github.com/pbsa/streamersedge-gui/commit/2de1be3))
* make UI for Redeem page [#688](https://github.com/pbsa/streamersedge-gui/issues/688) ([bd2128d](https://github.com/pbsa/streamersedge-gui/commit/bd2128d))
* making AdminDashboard boilerplate [#3](https://github.com/pbsa/streamersedge-gui/issues/3) ([fd68dd3](https://github.com/pbsa/streamersedge-gui/commit/fd68dd3))
* merging redeem page [#688](https://github.com/pbsa/streamersedge-gui/issues/688) ([ef0ff7d](https://github.com/pbsa/streamersedge-gui/commit/ef0ff7d))
* navigate to staging landing page after logout ([27becf2](https://github.com/pbsa/streamersedge-gui/commit/27becf2))
* remdemption screen [#688](https://github.com/pbsa/streamersedge-gui/issues/688) ([043a190](https://github.com/pbsa/streamersedge-gui/commit/043a190))
* removing unwanted changes ([f190aee](https://github.com/pbsa/streamersedge-gui/commit/f190aee))
* se-73: disable conditional donate when challenge.status != 'open' ([#253](https://github.com/pbsa/streamersedge-gui/issues/253)) ([6bf7637](https://github.com/pbsa/streamersedge-gui/commit/6bf7637))
* strm-1017 cosmetic changes for add funds page ([#250](https://github.com/pbsa/streamersedge-gui/issues/250)) ([c46cd8c](https://github.com/pbsa/streamersedge-gui/commit/c46cd8c))
* strm-1018 add funds error handling ([#249](https://github.com/pbsa/streamersedge-gui/issues/249)) ([0bd764d](https://github.com/pbsa/streamersedge-gui/commit/0bd764d))
* strm-1055 navigate to landing page with Dashboard link ([#259](https://github.com/pbsa/streamersedge-gui/issues/259)) ([e654054](https://github.com/pbsa/streamersedge-gui/commit/e654054))
* strm-666 and strm-633 added terms page and the links functional ([#266](https://github.com/pbsa/streamersedge-gui/issues/266)) ([2cbe74c](https://github.com/pbsa/streamersedge-gui/commit/2cbe74c))
* support 24h format on challenge grid page ([#252](https://github.com/pbsa/streamersedge-gui/issues/252)) ([02e6888](https://github.com/pbsa/streamersedge-gui/commit/02e6888))
* user ban modal avtar ([#269](https://github.com/pbsa/streamersedge-gui/issues/269)) ([e06c5bd](https://github.com/pbsa/streamersedge-gui/commit/e06c5bd))



<a name="0.16.0"></a>
# [0.16.0](https://github.com/PBSA/StreamersEdge-GUI/compare/v0.15.0...v0.16.0) (2020-01-30)

### Bug Fixes

* appropriate message when amount is less than 1 ([#254](https://github.com/PBSA/StreamersEdge-GUI/issues/254)) ([c8d3ffc](https://github.com/PBSA/StreamersEdge-GUI/commit/c8d3ffc))
* se-73: disable conditional donate when challenge.status != 'open' ([#253](https://github.com/PBSA/StreamersEdge-GUI/issues/253)) ([6bf7637](https://github.com/PBSA/StreamersEdge-GUI/commit/6bf7637))
* strm-1017 cosmetic changes for add funds page ([#250](https://github.com/PBSA/StreamersEdge-GUI/issues/250)) ([c46cd8c](https://github.com/PBSA/StreamersEdge-GUI/commit/c46cd8c))
* strm-1018 add funds error handling ([#249](https://github.com/PBSA/StreamersEdge-GUI/issues/249)) ([0bd764d](https://github.com/PBSA/StreamersEdge-GUI/commit/0bd764d))
* support 24h format on challenge grid page ([#252](https://github.com/PBSA/StreamersEdge-GUI/issues/252)) ([02e6888](https://github.com/PBSA/StreamersEdge-GUI/commit/02e6888))



<a name="0.15.0"></a>
# [0.15.0](https://github.com/pbsa/streamersedge-gui/compare/v0.14.0...v0.15.0) (2020-01-20)


### Bug Fixes

* add youtube button to user profile page ([#242](https://github.com/pbsa/streamersedge-gui/issues/242)) ([7aed6d4](https://github.com/pbsa/streamersedge-gui/commit/7aed6d4))
* strm-969 change susd to usd in the app ([#243](https://github.com/pbsa/streamersedge-gui/issues/243)) ([e20d2b8](https://github.com/pbsa/streamersedge-gui/commit/e20d2b8))
* strm-986 fixed start date ([#245](https://github.com/pbsa/streamersedge-gui/issues/245)) ([b7e50d0](https://github.com/pbsa/streamersedge-gui/commit/b7e50d0))
* strm-991 remove test modal links from header ([#246](https://github.com/pbsa/streamersedge-gui/issues/246)) ([e2465e5](https://github.com/pbsa/streamersedge-gui/commit/e2465e5))



<a name="0.14.0"></a>
# [0.14.0](https://github.com/pbsa/streamersedge-gui/compare/v0.13.0...v0.14.0) (2020-01-08)


### Bug Fixes

* strm-962 click on view all button in left menu ([#241](https://github.com/pbsa/streamersedge-gui/issues/241)) ([4c7b969](https://github.com/pbsa/streamersedge-gui/commit/4c7b969))



<a name="0.13.0"></a>
# [0.13.0](https://github.com/PBSA/StreamersEdge-GUI/compare/v0.11.0...v0.13.0) (2019-12-23)


### Bug Fixes

* strm-947: login correct user on change email ([#239](https://github.com/PBSA/StreamersEdge-GUI/issues/239)) ([c876bd6](https://github.com/PBSA/StreamersEdge-GUI/commit/c876bd6))



<a name="0.12.0"></a>
# [0.12.0](https://github.com/pbsa/streamersedge-gui/compare/v0.9.0...v0.12.0) (2019-12-18)


### Bug Fixes

* add s for popular challenges ([#218](https://github.com/pbsa/streamersedge-gui/issues/218)) ([75e077e](https://github.com/pbsa/streamersedge-gui/commit/75e077e))
* fix plural nouns ([#229](https://github.com/pbsa/streamersedge-gui/issues/229)) ([1a98d0e](https://github.com/pbsa/streamersedge-gui/commit/1a98d0e))
* fixed submit modal confusion. Added 3 OK modal type ([#222](https://github.com/pbsa/streamersedge-gui/issues/222)) ([7949fed](https://github.com/pbsa/streamersedge-gui/commit/7949fed))
* peerplays login redirects to create profile for new user ([#228](https://github.com/pbsa/streamersedge-gui/issues/228)) ([80bfa58](https://github.com/pbsa/streamersedge-gui/commit/80bfa58))
* remove validation for empty field ([#217](https://github.com/pbsa/streamersedge-gui/issues/217)) ([8ebb274](https://github.com/pbsa/streamersedge-gui/commit/8ebb274))
* renamed avatar for unix based systems ([#221](https://github.com/pbsa/streamersedge-gui/issues/221)) ([e21b166](https://github.com/pbsa/streamersedge-gui/commit/e21b166))
* uncommented blockchain init ([#231](https://github.com/pbsa/streamersedge-gui/issues/231)) ([1491003](https://github.com/pbsa/streamersedge-gui/commit/1491003))
* unlinked youtube account ([#223](https://github.com/pbsa/streamersedge-gui/issues/223)) ([844fa2f](https://github.com/pbsa/streamersedge-gui/commit/844fa2f))
* update profile redirect [#702](https://github.com/pbsa/streamersedge-gui/issues/702) ([#214](https://github.com/pbsa/streamersedge-gui/issues/214)) ([18dc54b](https://github.com/pbsa/streamersedge-gui/commit/18dc54b))
* user profile page hotfixes ([#219](https://github.com/pbsa/streamersedge-gui/issues/219)) ([433900b](https://github.com/pbsa/streamersedge-gui/commit/433900b))


### Features

* strm-521: link peerplays account from update profile ([#227](https://github.com/pbsa/streamersedge-gui/issues/227)) ([cf4496e](https://github.com/pbsa/streamersedge-gui/commit/cf4496e))
* strm-635: link/unlink pubg game ([#226](https://github.com/pbsa/streamersedge-gui/issues/226)) ([732a1ed](https://github.com/pbsa/streamersedge-gui/commit/732a1ed))



<a name="0.11.0"></a>
# [0.11.0](https://github.com/PBSA/StreamersEdge-GUI/compare/v0.9.0...v0.11.0) (2019-12-16)


### Bug Fixes

* add s for popular challenges ([#218](https://github.com/PBSA/StreamersEdge-GUI/issues/218)) ([75e077e](https://github.com/PBSA/StreamersEdge-GUI/commit/75e077e))
* remove validation for empty field ([#217](https://github.com/PBSA/StreamersEdge-GUI/issues/217)) ([8ebb274](https://github.com/PBSA/StreamersEdge-GUI/commit/8ebb274))
* renamed avatar for unix based systems ([#221](https://github.com/PBSA/StreamersEdge-GUI/issues/221)) ([e21b166](https://github.com/PBSA/StreamersEdge-GUI/commit/e21b166))
* update profile redirect [#702](https://github.com/PBSA/StreamersEdge-GUI/issues/702) ([#214](https://github.com/PBSA/StreamersEdge-GUI/issues/214)) ([18dc54b](https://github.com/PBSA/StreamersEdge-GUI/commit/18dc54b))
* user profile page hotfixes ([#219](https://github.com/PBSA/StreamersEdge-GUI/issues/219)) ([433900b](https://github.com/PBSA/StreamersEdge-GUI/commit/433900b))


<a name="0.10.0"></a>
# [0.10.0](https://github.com/PBSA/StreamersEdge-GUI/compare/v0.9.0...v0.10.0) (2019-12-16)


### Bug Fixes

* update profile redirect [#702](https://github.com/PBSA/StreamersEdge-GUI/issues/702) ([#214](https://github.com/PBSA/StreamersEdge-GUI/issues/214)) ([18dc54b](https://github.com/PBSA/StreamersEdge-GUI/commit/18dc54b))
* fix plural nouns ([#229](https://github.com/PBSA/StreamersEdge-GUI/issues/229)) ([1a98d0e](https://github.com/PBSA/StreamersEdge-GUI/commit/1a98d0e))
* fix responsiveness for create profile page ([#235](https://github.com/PBSA/StreamersEdge-GUI/issues/235)) ([bbd44d8](https://github.com/PBSA/StreamersEdge-GUI/commit/bbd44d8))
* fixed submit modal confusion. Added 3 OK modal type ([#222](https://github.com/PBSA/StreamersEdge-GUI/issues/222)) ([7949fed](https://github.com/PBSA/StreamersEdge-GUI/commit/7949fed))
* peerplays login redirects to create profile for new user ([#228](https://github.com/PBSA/StreamersEdge-GUI/issues/228)) ([80bfa58](https://github.com/PBSA/StreamersEdge-GUI/commit/80bfa58))
* uncommented blockchain init ([#231](https://github.com/PBSA/StreamersEdge-GUI/issues/231)) ([1491003](https://github.com/PBSA/StreamersEdge-GUI/commit/1491003))
* unlinked youtube account ([#223](https://github.com/PBSA/StreamersEdge-GUI/issues/223)) ([844fa2f](https://github.com/PBSA/StreamersEdge-GUI/commit/844fa2f))


### Features

* fix avatar's position ([#233](https://github.com/PBSA/StreamersEdge-GUI/issues/233)) ([7bbddfa](https://github.com/PBSA/StreamersEdge-GUI/commit/7bbddfa))
* strm-521: link peerplays account from update profile ([#227](https://github.com/PBSA/StreamersEdge-GUI/issues/227)) ([cf4496e](https://github.com/PBSA/StreamersEdge-GUI/commit/cf4496e))
* strm-635: link/unlink pubg game ([#226](https://github.com/PBSA/StreamersEdge-GUI/issues/226)) ([732a1ed](https://github.com/PBSA/StreamersEdge-GUI/commit/732a1ed))


<a name="0.9.0"></a>
# [0.9.0](https://github.com/pbsa/streamersedge-gui/compare/v0.7.0...v0.9.0) (2019-11-29)


* STRM-881 (#200) ([7012331](https://github.com/pbsa/streamersedge-gui/commit/7012331)), closes [#200](https://github.com/pbsa/streamersedge-gui/issues/200)


### Bug Fixes

* fixed create profile email incorrectly displaying error state ([#193](https://github.com/pbsa/streamersedge-gui/issues/193)) ([0c05125](https://github.com/pbsa/streamersedge-gui/commit/0c05125))
* fixed strm-771 strm-836 ([#186](https://github.com/pbsa/streamersedge-gui/issues/186)) ([d015adf](https://github.com/pbsa/streamersedge-gui/commit/d015adf))
* redirect strm742 ([#191](https://github.com/pbsa/streamersedge-gui/issues/191)) ([43712e4](https://github.com/pbsa/streamersedge-gui/commit/43712e4))
* show error message for not verified email ([#179](https://github.com/pbsa/streamersedge-gui/issues/179)) ([e6f41e0](https://github.com/pbsa/streamersedge-gui/commit/e6f41e0))
* submit modal x style changed ([#184](https://github.com/pbsa/streamersedge-gui/issues/184)) ([93e11d4](https://github.com/pbsa/streamersedge-gui/commit/93e11d4))
* **commonselector:** update the requirements to create challenge ([#207](https://github.com/pbsa/streamersedge-gui/issues/207)) ([cfd8a23](https://github.com/pbsa/streamersedge-gui/commit/cfd8a23))
* **createprofile:** x button on email confirm modal ([#198](https://github.com/pbsa/streamersedge-gui/issues/198)) ([c9620b2](https://github.com/pbsa/streamersedge-gui/commit/c9620b2))


### BREAKING CHANGES

* backend documentation missing(or the endpoint described to use does not exist yet)
so the preference page updates will always fail

STRM-881

* chore(userservice): remove code

update to timeformat uses an existing endpoint instead of a dedicated one. Remove uneeded code

STRM-881

* feat(timeformat): userservice update profile patch and preferences.jsx

use existing profile patch api endpoint function to update users' time format setting

STRM-881

* style(switch): modify style of switch

reduce width so that no whitespace can be clicked to activate the switch. Switches in material ui
are activated via clicking either the switch label(s) or the switch itself.

STRM-881



<a name="0.8.0"></a>
# [0.8.0](https://github.com/pbsa/streamersedge-gui/compare/v0.7.0...v0.8.0) (2019-11-27)


### Bug Fixes

* fixed create profile email incorrectly displaying error state ([#193](https://github.com/pbsa/streamersedge-gui/issues/193)) ([0c05125](https://github.com/pbsa/streamersedge-gui/commit/0c05125))
* redirect strm742 ([#191](https://github.com/pbsa/streamersedge-gui/issues/191)) ([43712e4](https://github.com/pbsa/streamersedge-gui/commit/43712e4))
* show error message for not verified email ([#179](https://github.com/pbsa/streamersedge-gui/issues/179)) ([e6f41e0](https://github.com/pbsa/streamersedge-gui/commit/e6f41e0))
* submit modal x style changed ([#184](https://github.com/pbsa/streamersedge-gui/issues/184)) ([93e11d4](https://github.com/pbsa/streamersedge-gui/commit/93e11d4))



<a name="0.7.0"></a>
# 0.7.0 (2019-11-20)


### Bug Fixes

* **navigation:** no navigation back to dashboard possible ([#170](https://github.com/pbsa/streamersedge-gui/issues/170)) ([f981fcd](https://github.com/pbsa/streamersedge-gui/commit/f981fcd))
* **register:** strm-459 ([#141](https://github.com/pbsa/streamersedge-gui/issues/141)) ([f34a237](https://github.com/pbsa/streamersedge-gui/commit/f34a237))
* added less spacing + larger text field to report modal ([#110](https://github.com/pbsa/streamersedge-gui/issues/110)) ([5cc8a06](https://github.com/pbsa/streamersedge-gui/commit/5cc8a06))
* **start.js:** removed os check breaking hot reloading ([#5](https://github.com/pbsa/streamersedge-gui/issues/5)) ([0a269eb](https://github.com/pbsa/streamersedge-gui/commit/0a269eb))
* bug with footer progress images appearing clickable ([#160](https://github.com/pbsa/streamersedge-gui/issues/160)) ([4ce1363](https://github.com/pbsa/streamersedge-gui/commit/4ce1363))
* callback update profile 696 ([#161](https://github.com/pbsa/streamersedge-gui/issues/161)) ([bdd0943](https://github.com/pbsa/streamersedge-gui/commit/bdd0943)), closes [#696](https://github.com/pbsa/streamersedge-gui/issues/696)
* capital letter invalid error handling for password ([#108](https://github.com/pbsa/streamersedge-gui/issues/108)) ([cbbe62c](https://github.com/pbsa/streamersedge-gui/commit/cbbe62c))
* changed ppy to susd ([#145](https://github.com/pbsa/streamersedge-gui/issues/145)) ([7536d70](https://github.com/pbsa/streamersedge-gui/commit/7536d70))
* changing cancelButton [#329](https://github.com/pbsa/streamersedge-gui/issues/329) ([#130](https://github.com/pbsa/streamersedge-gui/issues/130)) ([b127a65](https://github.com/pbsa/streamersedge-gui/commit/b127a65))
* created ban UI [#222](https://github.com/pbsa/streamersedge-gui/issues/222) ([#86](https://github.com/pbsa/streamersedge-gui/issues/86)) ([48619d8](https://github.com/pbsa/streamersedge-gui/commit/48619d8))
* error messages for profile upload [#629](https://github.com/pbsa/streamersedge-gui/issues/629) ([#135](https://github.com/pbsa/streamersedge-gui/issues/135)) ([69d6b0e](https://github.com/pbsa/streamersedge-gui/commit/69d6b0e)), closes [#131](https://github.com/pbsa/streamersedge-gui/issues/131) [#139](https://github.com/pbsa/streamersedge-gui/issues/139) [#141](https://github.com/pbsa/streamersedge-gui/issues/141)
* failed messages should start from uppercase characters ([#144](https://github.com/pbsa/streamersedge-gui/issues/144)) ([d937e32](https://github.com/pbsa/streamersedge-gui/commit/d937e32))
* fix cut off the right menu ([#163](https://github.com/pbsa/streamersedge-gui/issues/163)) ([1713b1d](https://github.com/pbsa/streamersedge-gui/commit/1713b1d))
* fix empty check ([#85](https://github.com/pbsa/streamersedge-gui/issues/85)) ([67abcb4](https://github.com/pbsa/streamersedge-gui/commit/67abcb4))
* fixed duplicate css in createprofile.scss ([#113](https://github.com/pbsa/streamersedge-gui/issues/113)) ([94f50d8](https://github.com/pbsa/streamersedge-gui/commit/94f50d8))
* fixed imports ([#101](https://github.com/pbsa/streamersedge-gui/issues/101)) ([50ca208](https://github.com/pbsa/streamersedge-gui/commit/50ca208))
* fixed invisible overlap ([#19](https://github.com/pbsa/streamersedge-gui/issues/19)) ([7aa267c](https://github.com/pbsa/streamersedge-gui/commit/7aa267c))
* fixed searchUser for invitation [#647](https://github.com/pbsa/streamersedge-gui/issues/647) ([#138](https://github.com/pbsa/streamersedge-gui/issues/138)) ([c359f6f](https://github.com/pbsa/streamersedge-gui/commit/c359f6f))
* fixed some spacing issues with donate modal ([#136](https://github.com/pbsa/streamersedge-gui/issues/136)) ([82fa52c](https://github.com/pbsa/streamersedge-gui/commit/82fa52c))
* generic validation, password & cpassword 465 ([#78](https://github.com/pbsa/streamersedge-gui/issues/78)) ([aa01a7f](https://github.com/pbsa/streamersedge-gui/commit/aa01a7f))
* login UI 549 ([#89](https://github.com/pbsa/streamersedge-gui/issues/89)) ([8e30bc9](https://github.com/pbsa/streamersedge-gui/commit/8e30bc9))
* logout navigation to root [#479](https://github.com/pbsa/streamersedge-gui/issues/479) ([#88](https://github.com/pbsa/streamersedge-gui/issues/88)) ([6210274](https://github.com/pbsa/streamersedge-gui/commit/6210274))
* navigate to dashboard [#695](https://github.com/pbsa/streamersedge-gui/issues/695) ([#142](https://github.com/pbsa/streamersedge-gui/issues/142)) ([d25060a](https://github.com/pbsa/streamersedge-gui/commit/d25060a))
* remove unused link from right menu ([#128](https://github.com/pbsa/streamersedge-gui/issues/128)) ([1cf093b](https://github.com/pbsa/streamersedge-gui/commit/1cf093b))
* removed peerplays disconnect button from se- prefix users ([#183](https://github.com/pbsa/streamersedge-gui/issues/183)) ([6eae409](https://github.com/pbsa/streamersedge-gui/commit/6eae409))
* reset password fix 576 ([#97](https://github.com/pbsa/streamersedge-gui/issues/97)) ([21b7731](https://github.com/pbsa/streamersedge-gui/commit/21b7731))
* strm-420 regex no longer checks domain as we are using masterlist ([#73](https://github.com/pbsa/streamersedge-gui/issues/73)) ([6e228c7](https://github.com/pbsa/streamersedge-gui/commit/6e228c7))
* strm-425 added required fields to register page ([#65](https://github.com/pbsa/streamersedge-gui/issues/65)) ([8fb10d0](https://github.com/pbsa/streamersedge-gui/commit/8fb10d0))
* strm-425 larger asterisk and center aligned placement ([#68](https://github.com/pbsa/streamersedge-gui/issues/68)) ([745f2a1](https://github.com/pbsa/streamersedge-gui/commit/745f2a1))
* strm-460 - properly reset login error message ([#62](https://github.com/pbsa/streamersedge-gui/issues/62)) ([c2087aa](https://github.com/pbsa/streamersedge-gui/commit/c2087aa))
* strm-478 reset input after forget password submit ([#118](https://github.com/pbsa/streamersedge-gui/issues/118)) ([32434bf](https://github.com/pbsa/streamersedge-gui/commit/32434bf))
* strm-496 remove validation from password field in login ([#121](https://github.com/pbsa/streamersedge-gui/issues/121)) ([08f9659](https://github.com/pbsa/streamersedge-gui/commit/08f9659))
* strm-496 username validation missing in Login page ([#74](https://github.com/pbsa/streamersedge-gui/issues/74)) ([94bd0d7](https://github.com/pbsa/streamersedge-gui/commit/94bd0d7))
* strm-498 username validation for capital letter signup page ([#75](https://github.com/pbsa/streamersedge-gui/issues/75)) ([f1bef23](https://github.com/pbsa/streamersedge-gui/commit/f1bef23))
* strm-563 fixed filename case-sensitivity issues on linux systems ([#91](https://github.com/pbsa/streamersedge-gui/issues/91)) ([ce1758f](https://github.com/pbsa/streamersedge-gui/commit/ce1758f))
* strm469 ([#90](https://github.com/pbsa/streamersedge-gui/issues/90)) ([37e470d](https://github.com/pbsa/streamersedge-gui/commit/37e470d))
* strm471 report modal ([#81](https://github.com/pbsa/streamersedge-gui/issues/81)) ([54ecbba](https://github.com/pbsa/streamersedge-gui/commit/54ecbba))
* strm472 ([#76](https://github.com/pbsa/streamersedge-gui/issues/76)) ([787b49f](https://github.com/pbsa/streamersedge-gui/commit/787b49f))
* strm482 ([#82](https://github.com/pbsa/streamersedge-gui/issues/82)) ([879213a](https://github.com/pbsa/streamersedge-gui/commit/879213a))
* strm535 ([#94](https://github.com/pbsa/streamersedge-gui/issues/94)) ([57a35cc](https://github.com/pbsa/streamersedge-gui/commit/57a35cc))
* strm740 strm757 ([#165](https://github.com/pbsa/streamersedge-gui/issues/165)) ([678f778](https://github.com/pbsa/streamersedge-gui/commit/678f778))
* strm833 834 ([#182](https://github.com/pbsa/streamersedge-gui/issues/182)) ([237c00a](https://github.com/pbsa/streamersedge-gui/commit/237c00a))
* username can not be blank ([#174](https://github.com/pbsa/streamersedge-gui/issues/174)) ([d905f8a](https://github.com/pbsa/streamersedge-gui/commit/d905f8a))
* youtube linking [#697](https://github.com/pbsa/streamersedge-gui/issues/697) ([#149](https://github.com/pbsa/streamersedge-gui/issues/149)) ([2a4b771](https://github.com/pbsa/streamersedge-gui/commit/2a4b771))
* **validation:** fix uppercase rule ([#143](https://github.com/pbsa/streamersedge-gui/issues/143)) ([2ab310c](https://github.com/pbsa/streamersedge-gui/commit/2ab310c))


### Features

* implemented steps progress bar in footer ([#16](https://github.com/pbsa/streamersedge-gui/issues/16)) ([cac1ae5](https://github.com/pbsa/streamersedge-gui/commit/cac1ae5))
* removed user icon from header before login ([#61](https://github.com/pbsa/streamersedge-gui/issues/61)) ([56d5e19](https://github.com/pbsa/streamersedge-gui/commit/56d5e19))
* wrapped the input text field with styling ([#18](https://github.com/pbsa/streamersedge-gui/issues/18)) ([86cd778](https://github.com/pbsa/streamersedge-gui/commit/86cd778))
* **profile upload:** add loading indicatorfor profile upload component ([#132](https://github.com/pbsa/streamersedge-gui/issues/132)) ([c653b5d](https://github.com/pbsa/streamersedge-gui/commit/c653b5d))
* **resetpw:** log user in after successful pw reset ([#164](https://github.com/pbsa/streamersedge-gui/issues/164)) ([51a38c5](https://github.com/pbsa/streamersedge-gui/commit/51a38c5))



<a name="0.6.0-v.0"></a>
## 0.6.0-v.0 (2019-09-06)

### Features
* New Signup Modal ([#79](https://github.com/PBSA/StreamersEdge-GUI/pull/79))
  * Contains the following bug fixes
    * [STRM 459](https://peerplays.atlassian.net/browse/STRM-459)
      * Clear signup after navigating away from page
    * [STRM 470](https://peerplays.atlassian.net/browse/STRM-470)
      * Fixed overlaping error messages in signup
    * [STRM 499](https://peerplays.atlassian.net/browse/STRM-499)
      * Signup throws error for min or max password
    * [STRM 501](https://peerplays.atlassian.net/browse/STRM-501)
  * Contains the following tasks
    * [STRM 510](https://peerplays.atlassian.net/browse/STRM-510)
      * Password strength indicator

### Bug Fixes
* STRM-420 ([#73](https://github.com/PBSA/StreamersEdge-GUI/pull/73))
  * Fixed regex overriding TLD domain check
* STRM-496 ([#74](https://github.com/PBSA/StreamersEdge-GUI/pull/74))
  * Added username validation to login page
* STRM-498 ([#75](https://github.com/PBSA/StreamersEdge-GUI/pull/75))
  * Added username validation for capital letter signup page
* STRM-472 ([#76](https://github.com/PBSA/StreamersEdge-GUI/pull/76))
  * Report user radio selection now has default

<a name="0.5.0-v.0"></a>
## 0.5.0-v.0 (2019-08-19)

### Features
* Donation screen visuals ([#70](https://github.com/PBSA/StreamersEdge-GUI/pull/70))
* Dummy data wrapper ([#60](https://github.com/PBSA/StreamersEdge-GUI/pull/60))
* Report user modal ([#64](https://github.com/PBSA/StreamersEdge-GUI/pull/64))
* setup for challenge redux architecture and API calls ([#58](https://github.com/PBSA/StreamersEdge-GUI/pull/58))

### Bug Fixes
* STRM-414 ([#69](https://github.com/PBSA/StreamersEdge-GUI/pull/69))
* STRM-420 ([#66](https://github.com/PBSA/StreamersEdge-GUI/pull/66))
  * Added list of accepted top level domains
* STRM-425 ([#68](https://github.com/PBSA/StreamersEdge-GUI/pull/68))
  * added asterisk to required fields in signup
* STRM-460 ([#62](https://github.com/PBSA/StreamersEdge-GUI/pull/62))
  * properly reset login error message
* STRM-429 ([#61](https://github.com/PBSA/StreamersEdge-GUI/pull/61))
  * removed user icon from header before login
* STRM-431 ([#59](https://github.com/PBSA/StreamersEdge-GUI/pull/59))
  * Add email validation to onChange.
  * Disable submit button if validation is failing
<a name="0.1.2-v.0"></a>
## 0.1.2-v.0 (2019-07-31)

### Bug Fixes

* STRM-415 ([#52](https://github.com/PBSA/StreamersEdge-GUI/issues/52))
  * Better error handling on the register form.
* STRM-416 ([#55](https://github.com/PBSA/StreamersEdge-GUI/issues/55))
  * More specific errors on the login in form.
* STRM-430 ([#53](https://github.com/PBSA/StreamersEdge-GUI/issues/5))
  * Text wrapping issue on the login form.


<a name="0.1.1"></a>
## 0.1.1 (2019-07-19)


### Bug Fixes

* **start.js:** removed os check breaking hot reloading ([#5](https://github.com/PBSA/StreamersEdge-GUI/issues/5)) ([0a269eb](https://github.com/PBSA/StreamersEdge-GUI/commit/0a269eb))
* fixed invisible overlap ([#19](https://github.com/PBSA/StreamersEdge-GUI/issues/19)) ([7aa267c](https://github.com/PBSA/StreamersEdge-GUI/commit/7aa267c))
* styling issue main site backgroun


### Features

* site version display if in dev mode ([2f6f69e](https://github.com/PBSA/StreamersEdge-GUI/commit/2f6f69e))
  * Footer component
* wrapped the input text field with styling ([#18](https://github.com/PBSA/StreamersEdge-GUI/issues/18)) ([86cd778](https://github.com/PBSA/StreamersEdge-GUI/commit/86cd778))
* Streamers Edge profile image uploading
* Custom input fields using Material UI
* Custom modals for use in
  * Login
  * Any other content can be pushed into this modal
* Streamers Edge profile creation
* Registration Page
  * Streamers Edge system registration
    * Email verification
  * Facebook OAuth registration
  * YouTube/Google OAuth registration
* Base site, routing (private/public route(s)), & repo initialization.
  * ESlint
  * Stylelint
  * Commitizen
  * Commitlint
  * SonarCloud
  * Standard-version
  * Conventional Changelog
  * Translation compatible use of strings
  * Header component
