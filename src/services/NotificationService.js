import axios from 'axios';
import {Config} from '../utility';

const apiRequest = axios.create({withCredentials: true});

const apiRoot = Config.isDev
  ? Config.devApiRoute
  : Config.prodApiRoute;

/**
 * Converts a base64 encoded string to a uint8 array.
 *
 * @param {string} base64String - Base64 encoded string.
 * @returns {Uint8Array}
 */
function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }

  return outputArray;
}

/**
 * Implements Web Push notifications.
 *
 * @class NotificationService
 */
export default class NotificationService {
  static isSupported() {
    return 'serviceWorker' in navigator && 'PushManager' in window;
  }

  static async requestPermission() {
    return await Notification.requestPermission() === 'granted';
  }

  static async initializeWorker() {
    navigator.serviceWorker.register('/sw.js');
    const serviceWorker = await navigator.serviceWorker.ready;

    const {data: {result: {publicKey}}} = await apiRequest.get(`${apiRoot}api/v1/notifications/publicKey`);

    const pushSubscription = await serviceWorker.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: urlB64ToUint8Array(publicKey)
    });

    await apiRequest.post(`${apiRoot}api/v1/notifications/subscribe`, pushSubscription);
  }
}
