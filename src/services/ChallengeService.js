import axios from 'axios';
import querystring from 'query-string';
import {Config, GenUtil} from '../utility';

const ApiHandler = axios.create({withCredentials: true});

const apiRoot = Config.apiRoute;

class PrivateChallengeService {
  /**
   * TODO: revisit this function once API has been merged to make sure it works. Code for getChallenge API hasn't been merged yet.
   *
   * @static
   * @returns {Promise} - A promise that indicates success or failure.
   * @param {object} fetchingParams -Object with fields {name,game,limit,offset,sort}.
   * @memberof ChallengeService
   */
  static getChallenges({sort='', searchText=''}) {
    const query = `${apiRoot}api/v1/challenges?order=${sort}&searchText=${searchText}`;
    return new Promise(async(resolve, reject) => {
      const response = await ApiHandler.get(query);

      if (response.data.status === 401) {
        return reject(response);
      }

      if (response.data.status !== 200) {
        return reject(response);
      }

      return resolve(response.data.result);
    });
  }
  /**
   * Works but passing conditions array returns error.
   * TODO figure out why conditions isn't working.
   *
   * @static
   * @param {object} challenge -Object with required fields: {email, endDate, game, accessRule, sUSD, and either conditionsText OR conditions}.
   * @returns {Promise} - A promise that indicates success or failure.
   * @memberof ChallengeService
   */
  static createChallenge(challenge) {
    let response;
    const query = `${apiRoot}api/v1/challenges`;
    return new Promise(async(resolve, reject) => {
      const headers = {
        headers: {
          'Content-Type': 'application/json'
        }
      };

      try {
        response = await ApiHandler.post(query, JSON.stringify(challenge), headers);
        return resolve(response.data.result);
      } catch (err) {
        return reject(err.response.data);
      }
    });
  }
  /**
   * TODO: Once API works this function will have to be revisited to make sure it works. Currrently receiving 404 challenge not found. BROKEN.
   *
   * @static
   * @param {object} invite - Object with required fields: {userId, challengeId}.
   * @returns {Promise} - A promise that indicates success or failure.
   * @memberof ChallengeService
   */
  static sendChallengeInvite(invite) {
    let response;
    const query = `${apiRoot}api/v1/challenges/invite`;
    return new Promise(async(resolve, reject) => {
      const headers = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      };

      const {userId, challengeId} = invite;
      const body = {
        userId: userId,
        challengeId: challengeId
      };

      try {
        response = await ApiHandler.post(query, querystring.stringify(body), headers);

        return resolve(response.data.result);
      } catch (err) {

        return reject(err);
      }
    });
  }
  /**
   * Returns challenge object for specified id.
   *
   * @static
   * @param {number} challengeId - Id of the challenge to retrieve.
   * @returns {Promise}
   * @memberof ChallengeService
   */
  static getChallengeById(challengeId) {
    let response;
    const query = `${apiRoot}api/v1/challenges/${challengeId}`;
    return new Promise(async(resolve, reject) => {

      try {
        response = await ApiHandler.get(query);

        return resolve(response.data.result);
      } catch (err) {
        return reject(err);
      }
    });
  }

  /**
   * Returns challenge object for specified user id.
   *
   * @static
   * @param {number} userId - Id of the user whose challenges have to be retrieved.
   * @returns {Promise}
   * @memberof ChallengeService
   */
  static getChallengesByUserId(userId) {
    let response;
    const query = `${apiRoot}api/v1/challengesbyuser/${userId}`;
    return new Promise(async(resolve, reject) => {

      try {
        response = await ApiHandler.get(query);

        return resolve(response.data.result);
      } catch (err) {
        return reject(err);
      }
    });
  }

  /**
   * Returns donation history for specified challenge id.
   *
   * @static
   * @param {number} challengeId - Id of the challenge whose donation history has to be retrieved.
   * @returns {Promise}
   * @memberof ChallengeService
   */
  static getDonationHistory(challengeId) {
    let response;
    const query = `${apiRoot}api/v1/donationhistory/${challengeId}`;
    return new Promise(async(resolve, reject) => {

      try {
        response = await ApiHandler.get(query);

        return resolve(response.data.result);
      } catch (err) {
        return reject(err);
      }
    });
  }

  static donateToChallenge(challengeId, challengeAmount, depositOp) {
    let response;
    let body;
    const query = `${apiRoot}api/v1/challenges/conditionaldonate`;
    return new Promise(async(resolve, reject) => {
      const headers = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      };

      if(depositOp) {
        body = {
          challengeId: challengeId,
          depositOp: depositOp
        };
      } else {
        body = {
          challengeId: challengeId,
          ppyAmount: challengeAmount
        };
      }

      try {
        response = await ApiHandler.post(query, querystring.stringify(body), headers);

        return resolve(response.data.result);
      } catch (err) {

        let errorObj = err.response.data.error;

        if (typeof errorObj === 'string') {
          return reject(errorObj);
        }

        return reject(errorObj[Object.keys(errorObj)][0]);
      }
    });
  }
}


class ChallengeService {
  /**
   * TODO: revisit this function once API has been merged to make sure it works. Code for getChallenge API hasn't been merged yet.
   *
   * @static
   * @returns {Promise} - A promise that indicates success or failure.
   * @param {object} fetchingParams -Object with fields {name,game,limit,offset,sort}.
   * @memberof ChallengeService
   */
  static getChallenges(fetchingParams) {
    return GenUtil.dummyDataWrapper(PrivateChallengeService.getChallenges(fetchingParams || {}));
  }

  /**
   * Works but passing conditions array returns error.
   * TODO figure out why conditions isn't working.
   *
   * @static
   * @param {object} challenge -Object with required fields: {email, endDate, game, accessRule, sUSD, and either conditionsText OR conditions}.
   * @returns {Promise} - A promise that indicates success or failure.
   * @memberof ChallengeService
   */
  static createChallenge(challenge) {
    return GenUtil.dummyDataWrapper(PrivateChallengeService.createChallenge(challenge));
  }

  /**
   * TODO: Once API works this function will have to be revisited to make sure it works. Currrently receiving 404 challenge not found. BROKEN.
   *
   * @static
   * @param {object} invite - Object with required fields: {userId, challengeId}.
   * @returns {Promise} - A promise that indicates success or failure.
   * @memberof ChallengeService
   */
  static sendChallengeInvite(invite) {
    return GenUtil.dummyDataWrapper(PrivateChallengeService.sendChallengeInvite(invite));
  }

  /**
   * Returns challenge object for specified id.
   *
   * @static
   * @param {number} challengeId - Id of the challenge to retrieve.
   * @returns {Promise}
   * @memberof ChallengeService
   */
  static getChallengeById(challengeId) {
    return GenUtil.dummyDataWrapper(PrivateChallengeService.getChallengeById(challengeId));
  }

  /**
   * Returns challenge object for specified user id.
   *
   * @static
   * @param {number} userId - Id of the user whose challenges have to be retrieved.
   * @returns {Promise}
   * @memberof ChallengeService
   */
  static getChallengesByUserId(userId) {
    return GenUtil.dummyDataWrapper(PrivateChallengeService.getChallengesByUserId(userId));
  }

  static donateToChallenge(challengeId, challengeAmount, depositOp) {
    return GenUtil.dummyDataWrapper(PrivateChallengeService.donateToChallenge(challengeId, challengeAmount, depositOp));
  }
  /**
   * Returns won challenges by user.
   *
   * @static
   * @param {number} id - Id of the user.
   * @returns {Promise}
   * @memberof ChallengeService
   */
  static getWonChallengesByUser(id) {
    const query = `${apiRoot}api/v1/challenges/wins/${id}`;
    return new Promise(async(resolve, reject) => {
      try {
        const response = await ApiHandler.get(query);
        return resolve(response.data.result);
      } catch (error) {
        return reject(error);
      }
    });
  }

  /**
   * Returns challenge donation history by challenge id.
   *
   * @static
   * @param {number} challengeId - Id of the challenge whose donation history has to be retrieved.
   * @returns {Promise}
   * @memberof ChallengeService
   */
  static getDonationHistory(challengeId) {
    return GenUtil.dummyDataWrapper(PrivateChallengeService.getDonationHistory(challengeId));
  }
}

export default ChallengeService;
