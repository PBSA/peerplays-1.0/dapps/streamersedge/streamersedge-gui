import axios from 'axios';
import {Config, GenUtil} from '../utility';

const ApiHandler = axios.create({withCredentials: true});

const apiRoot = Config.isDev
  ? Config.devApiRoute
  : Config.prodApiRoute;

/**
 * Handles all server calls related to game.
 *
 * @class PrivateAdminService
 */
class PrivateAdminService {
  /**
   * Retrieves all reports.
   *
   * @param {string} searchText - Text to search.
   * @param {number} limit - Text to search.
   * @param {number} offset - Text to search.
   * @returns {Promise} A promise that resolves to a game status.
   */
  static getReports(searchText, limit, offset) {
    let search = searchText || '';
    const query = `${apiRoot}api/v1/admin/reports?search=${search}&limit=${limit}&offset=${offset}`;

    return new Promise(async(resolve, reject) => {
      const response = await ApiHandler.get(query);

      if (response.data.status === 401) {
        return reject(response);
      }

      if (response.data.status !== 200) {
        return reject(response);
      }

      return resolve(response.data.result);
    });
  }
  /**
   * Retrieves all Banned Users.
   *
   * @param {string} searchText - Text to search.
   * @returns {Promise} A promise that resolves to a game status.
   */
  static getBannedUser(searchText) {
    let search = searchText || '';
    const query = `${apiRoot}api/v1/admin/users?flag=banned&search=${search}&limit=100`;
    return new Promise(async(resolve, reject) => {
      const response = await ApiHandler.get(query);

      if (response.data.status === 401) {
        return reject(response);
      }

      if (response.data.status !== 200) {
        return reject(response);
      }

      return resolve(response.data.result);
    });
  }
  /**
   * Ban a user.
   *
   * @param {string} id - User id to ban.
   * @returns {Promise} A promise that resolves to a game status.
   */
  static banUser(id) {
    const query = `${apiRoot}api/v1/admin/users/ban/${id}`;
    return new Promise(async(resolve, reject) => {
      const headers = {
        headers: {
          'Content-Type': 'application/json'
        }
      };

      try {
        const response = await ApiHandler.put(query,headers);
        return resolve(response.data);
      } catch (err) {
        return reject(err.response.data);
      }

    });
  }
  /**
   * UnBan a user.
   *
   * @param {string} id - Id to unban.
   * @returns {Promise} A promise that resolves to a game status.
   */
  static unBanUser(id) {
    const query = `${apiRoot}api/v1/admin/users/unban/${id}`;
    return new Promise(async(resolve, reject) => {
      const headers = {
        headers: {
          'Content-Type': 'application/json'
        }
      };

      try {
        const response = await ApiHandler.put(query,headers);
        return resolve(response.data);
      } catch (err) {
        return reject(err.response.data);
      }

    });
  }
}

/**
 * Handles all server calls related to admin actions.
 *
 * @class AdminService
 */
class AdminService {
  /**
   * Retrieves all reports.
   *
   * @param {string} searchText - Text to search.
   * @param {number} limit - Text to search.
   * @param {number} offset - Text to search.
   * @returns {Promise} A promise that resolves to a game status.
   */
  static getReports(searchText, limit, offset) {
    return GenUtil.dummyDataWrapper(PrivateAdminService.getReports(searchText, limit, offset));
  }
  /**
   * Ban a user.
   *
   * @param {string} id - User id to ban.
   * @returns {Promise} A promise that resolves to a game status.
   */
  static banUser(id) {
    return GenUtil.dummyDataWrapper(PrivateAdminService.banUser(id));
  }
  /**
   * Retrieves banned users.
   *
   * @param {string} searchText - Text to search.
   * @returns {Promise} A promise that resolves to a game status.
   */
  static getBannedUser(searchText){
    return GenUtil.dummyDataWrapper(PrivateAdminService.getBannedUser(searchText));
  }
  /**
   * UnBan a user.
   *
   * @param {string} id - User id to ban.
   * @returns {Promise} A promise that resolves to a game status.
   */
  static unBanUser(id) {
    return GenUtil.dummyDataWrapper(PrivateAdminService.unBanUser(id));
  }
}

export default AdminService;
