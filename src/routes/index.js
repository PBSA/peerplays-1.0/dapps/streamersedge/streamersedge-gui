import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from '../components/Home';
import Dashboard from '../components/Dashboard';
import CreateChallenge from '../components/CreateChallenge';
import CreateProfile from '../components/CreateProfile';
import Callback from '../components/Callback';
import ErrorCallback from '../components/ErrorCallback';
import ResetForm from '../components/ForgotPassword/ResetForm';
import Preferences from '../components/Preferences';
import {requireAuthentication} from '../components/Auth/AuthComponent';
import {RouteConstants as Routes} from '../constants';
import UpdateProfile from '../components/UpdateProfile';
import ListChallenges from '../components/ListChallenges';
import ChallengeDetails from '../components/ChallengeDetails';
import TermsOfService from '../components/TermsOfService';
import Alerts from '../components/Alerts';

import Profile from '../components/Profile';
import Redeem from '../components/Redeem';
import AdminDashboard from '../components/AdminDashboard';

// https://github.com/supasate/connected-react-router/blob/master/examples/immutable/src/routes/index.js

const routes = (
  <>
    <Switch>
      <Route exact path={ Routes.ROOT } component={ Home }/>
      <Route path={ Routes.RESET_PASSWORD } component={ ResetForm }/>
      <Route path={ Routes.TERMS } component={ TermsOfService } />
      <Route path={ Routes.ALERTS } component={ Alerts }/>
      <Route path={ Routes.PROFILE } component={ requireAuthentication(CreateProfile) }/>
      <Route path={ Routes.DASHBOARD } component={ Dashboard }/>
      <Route path={ Routes.PREFERENCES } component={ requireAuthentication(Preferences) } />
      <Route path={ Routes.CREATE_CHALLENGE } component={ requireAuthentication(CreateChallenge) } />
      <Route path={ Routes.CHALLENGES } component={ ListChallenges } />
      <Route path={ Routes.UPDATE_PROFILE } component={ requireAuthentication(UpdateProfile) } />
      <Route path={ Routes.CALLBACK } component={ Callback }/>
      <Route path={ Routes.ERROR_CALLBACK } component={ ErrorCallback } />
      <Route path={ Routes.USER_PROFILE } component={ requireAuthentication(Profile) }/>
      <Route path={ Routes.ADMIN_DASHBOARD } component={ requireAuthentication(AdminDashboard) } />
      <Route path={ Routes.REDEEM } component={ Redeem } />
      <Route path={ Routes.CHALLENGE } component={ ChallengeDetails }/>
    </Switch>
  </>
);

export default routes;
