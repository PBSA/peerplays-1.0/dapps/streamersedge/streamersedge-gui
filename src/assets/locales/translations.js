export const translationObject = {
  en: {
    cancel: 'Cancel',
    addFunds: {
      error: 'Error Adding Funds',
      success: 'Amount added successfully to your account',
      exception: 'We could not verify your order. Please contact StreamersEdge support with your PayPal transaction information handy.',
      title: 'Select a Payment Method',
      fiat: 'Pay With:',
      specify: 'Amount To Be Added:',
      sUsd: 'USD'
    },
    errors: {
      loggedOut: 'You have been logged out. Please login again.',
      username: {
        requirement: {
          length: 'Should be between 3 & 60 chars long',
          beginsWithLetter: 'Begins with letter',
          endsWithLowercaseOrDigit: 'Ends with lowercase letter or digit',
          onlyContainsLettersDigitHyphens: 'Only contains letters, digits, hyphens',
          noUppercase: 'No Uppercase',
          noBlankUsername: 'Username cannot be blank',
          noSpaceInUsername: 'No space allowed'
        },
        prefix1: 'Account name should ',
        prefix2: 'Each account segment should ',
        notEmpty: 'not be empty.',
        longer: 'be longer.',
        shorter: 'be shorter.',
        startLetter: 'start with a letter.',
        lettersDigitsDashes: 'have only lowercase letters, digits, or dashes.',
        oneDash: 'have only one dash in a row.',
        endAlphanumeric: 'end with a letter or digit.'
      },
      email: {
        invalid: 'Invalid email address',
        invalidDomain: 'Invalid top level domain name'
      },
      password: {
        requirement: {
          peerplaysLength: 'Password should be at least 22 symbols (alphanumeric)',
          length: 'Should be between 6 & 60 chars long',
          number: 'Contains at least 1 number',
          specialChar: 'Contains a special character from (.@!#$%^*)',
          unallowedSpecialChar: 'No Unallowed special character',
          noSpaces: 'No Spaces'
        },
        confirmPassword: 'Passwords must match'
      },
      search: {
        lengthRequirement: 'search text must be greater than 3 and less than 100 characters',
        noBlank: 'search text cannot be blank'
      },
      date: {
        invalid: 'not a valid date',
        beToday: 'starting date must be current date',
        startLessThanEnd: 'start date must be less than end date'
      },
      profile: {
        invalidImageType: 'Only JPEG and PNG images are supported.',
        invalidImageSize: 'Image must be less than 1 MB.',
        invalidTypeAndSize: 'Image must be JPEG or PNG \nand be less than 1 MB.'
      },
      streams: {
        addError: 'Could not retrieve stream %{id}'
      },
      general: {
        beNumber: 'must be a number',
        beDecimalOrInt: 'must be either a decimal or integer'
      },
      challengeName: {
        requirement: {
          length: 'Maximum 50 characters long',
          required: 'This field is required'
        }
      },
      addFunds: {
        zero: 'Amount cannot be zero',
        amountHigh: 'Amount cannot be greater than 10000. Please enter a lower amount.'
      }
    },
    userProfile: {
      active: 'Active ',
      challenge: 'Challenge',
      challenges: 'Challenges',
      won: 'Won',
      noActive: 'This user does not have an active challenge.',
      noChallenges: 'There are no challenges to display.'
    },
    redeem: {
      header: 'Redeem Funds',
      currency: 'Currency',
      USD: 'USD',
      balance: 'Balance',
      email: 'Your PayPal Account Is:',
      emailSuccess: 'Your Paypal account updated successfully.',
      success: 'Request to redeem was successful. You’ll get the payout soon.',
      error: 'Some error occurred.',
      errorAmountZero: 'Amount to redeem should be greater than zero',
      errorInsufficientFunds: 'Insufficient Funds',
      errorLinkPaypal: 'Please link your Paypal Account using Edit Account link'
    },
    adminPanel: {
      banSuccess: 'User has been successfully banned.',
      unbanSuccess: 'User has been successfully unbanned.',
      bannedAlready: 'User is already banned',
      banFailure: 'Error occurred while banning the user',
      unbanFailure: 'Error occurred while unbanning the user'
    },
    preferences: {
      header: 'PREFERENCES',
      time: {
        twenty4: '24 Hour Format'
      },
      invites: {
        header: 'Invites',
        option1: 'Everyone can send me an invite',
        option2: 'Receive invites from specific users',
        option3: 'Receive invites for specific games',
        option4: 'Don\'t receive invites from anyone',
        bounty: 'Minimum bounty for invite',
        ppy: 'PPY',
        sUSD: 'USD',
        searchUsersPlaceholder: 'Search users - Begin typing username',
        searchGamesPlaceholder: 'Search games - Begin typing game',
        bountyPlaceholder: '0',
        errors: {
          user: {
            alreadyAdded: 'User already added',
            notFound: 'User not found'
          },
          game: {
            alreadyAdded: 'Game already added',
            notFound: 'Game not found'
          }
        }
      },
      notifications: {
        generalHeader: 'General',
        header: 'Notifications',
        option1: 'Get notifications for all challenges',
        option2: 'Don\'t receive any notifications'
      },
      modal: {
        errorHeader: 'Unexpected Error',
        errorSubText: 'Failed to update preferences',
        successHeader: 'Your preferences have been succesfully updated',
        redirectToDashboard: 'You will be redirected to dashboard in 15 seconds, otherwise,',
        redirectClickHere: 'click here',
        clickHere: 'click here'
      }
    },
    login: {
      header: 'Login',
      welcome: 'Hello, welcome again!',
      enterUsername: 'Enter your StreamersEdge Username or Email',
      enterPassword: 'Enter your StreamersEdge Password',
      dontHaveAccount: 'Don\'t have a StreamersEdge account?',
      register: 'Register',
      forgotPass: 'Forgot your password?',
      orLoginWith: 'or Login using our Sign In Partners',
      invalidPassword: 'The password you\'ve entered is incorrect',
      error: 'Login Error',
      verify: 'Please verify your email',
      privacyPolicy: 'Privacy Policy',
      terms: 'Terms and Conditions'
    },
    guestUser:{
      header: 'Unauthorized Access',
      message: 'You need to login before proceeding further'
    },
    register: {
      createAccount: 'Create an Account',
      createAccountSubHeader: 'Welcome, you will be surprised here!',
      enterEmail: 'Enter your email',
      enterUsername: 'Enter your username',
      enterPassword: 'Enter your password',
      confirmPassword: 'Confirm your password',
      alreadyHaveAccount: 'Already have a StreamersEdge account?',
      login: 'Login',
      passwordStrength: {
        veryWeak: 'Very Weak',
        weak: 'Weak',
        medium: 'Medium',
        strong: 'Strong'
      },
      responses: {
        errorMissing: 'All of the required fields must be filled.',
        confirmSent: 'Confirmation email sent.'
      }
    },
    header: {
      login: 'Log In',
      logout: 'Log Out',
      signup: 'Sign Up',
      menu: 'Menu',
      popular: 'Popular'
    },
    forgotPassword: {
      header: 'Forgot Your Password?',
      subHeader: 'Enter your email to reset your StreamersEdge password.',
      enterEmail: 'Enter your StreamersEdge Email',
      resultText: {
        success:'If an account exists that matches the email provided, an email will be sent to reset your password.',
        cooldown:'You have attempted to reset your password too many times, please wait before trying again.',
        invalidEmail:'Please enter a valid email.'
      },
      resetForm: {
        header: 'Reset your password',
        subHeader: 'Please choose a new password to finish signing in.',
        newPassword: 'Enter your new password',
        confirmPassword: 'Confirm your new password',
        noMatch: 'Passwords do not match.',
        expired: 'The reset link has expired or is no longer valid.',
        passwordStrength: {
          veryWeak: 'Very Weak',
          weak: 'Weak',
          medium: 'Medium',
          strong: 'Strong'
        }
      }
    },
    peerplays: {
      login: 'Login with Peerplays Global',
      enterUsername: 'Enter your username',
      enterPassword: 'Enter your password',
      dontHaveAccount: 'Don\'t have a Peerplays Global account?',
      register: 'Register',
      authenticate: 'Authenticate Your Peerplays Account',
      username: 'Username',
      password: 'Password',
      information: {
        title: 'Information',
        content: 'The username you enter is powered by the Peerplays blockchain. If you already have a username at Peerplays, ' +
        'please enter it here to link your StreamersEdge profile to it. If you don\'t have one, simply proceed to ',
        register: 'register your account'
      },
      passwordLengthError: 'Password should be at least 12 characters long'
    },
    dashboard: {
      challenge: {
        starting: 'Starting ',
        started: 'Started '
      },
      featured: {
        header: 'Active Challenges',
        desc: 'StreamersEdge Rivals brings streamers and esports together! Competitors in the StreamersEdge Rivals Fortnite Showdown will compete against each other numerous game modes: FFA, ' +
        'Skirmish and Horde mode.'
      },
      genres: {
        shooter: 'SHOOTER',
        strategy: 'STRATEGY'
      },
      games: {
        fortnite: 'Fortnite',
        pubg: 'Pubg',
        lol: 'Lol'
      },
      recommended: 'Recommended',
      live: 'Live ',
      streams: 'Live Streams',
      category: 'Category: ',
      categories: 'Categories',
      challenges: 'Challenges',
      streaming: 'STREAMING',
      accepted: 'ACCEPTED',
      join: 'JOIN',
      susd: 'USD',
      donate: 'Donate'
    },
    previewCard: {
      status: {
        live: 'LIVE',
        join: 'JOIN'
      }
    },
    reportUser: {
      report: 'REPORT',
      selectableReasons: 'Selectable reasons for reports:',
      giveDescription: 'Please give a brief description..',
      attachLink: 'Attach a link to reported video:',
      sent: 'Report Sent.',
      error: {
        emptyField: 'Please ensure all fields are filled.',
        length: 'The description must be at least 24 characters long and less than 1000 characters.'
      },
      reasons: {
        one: 'Offends my religious sentiments',
        two: 'Offensive profile pic',
        three: 'Other'
      }
    },
    createProfile: {
      header: 'Create Profile',
      accountTypes: 'gamer,viewer,sponsor',
      defaultAccountType: 'viewer',
      modal: {
        header: 'Confirmation email sent please check your email',
        subText: 'You will be redirected to step 2'
      }
    },

    listChallenges: {
      sortTypes: ' Challenges by name, Challenges by game, Challenges by Start Date, Challenges by Creation Date, Challenges by Total Donation, Active Challenges',
      failed: 'Failed to retrieve challenges',
      headerText: 'Current Challenges',
      live: 'LIVE',
      starts: 'STARTS',
      usd: 'USD: ',
      ending: 'Ending',
      streaming: 'Streaming',
      accepted: 'Accepted',
      streamed: 'Streamed',
      category: 'Category: ',
      threeChars: 'Please enter at least 3 characters',
      hundredChars: 'Maximum 100 characters allowed',
      noResults: 'No Results'
    },

    updateProfile: {
      userInfo: {
        header: 'User Info',
        userType: 'Select account type',
        email: 'Enter your email',
        avatar: 'Customize Avatar',
        updateHeader: 'Update Your Profile',
        editUserType: 'Edit Account Type',
        editEmail: 'Edit StreamersEdge Account Email',
        updatedSuccessfully: 'You have successfully updated your profile',
        confirmEmail: 'An email has been sent to the email id that you specified. Please click on the verification link in the email to confirm your email.',
        updateFailed: 'Profile update failed.'
      },
      accountConnections: {
        cryptoHeader: 'Peerplays Wallet',
        cryptoSelect: 'SELECT YOUR CRYPTOCURRENCY ACCOUNT',
        cryptoDescription: 'Connect your Peerplays wallet and unlock special Streamer Edge integrations',
        cryptoLabel: 'Wallet Address',
        connectPlus: 'Connecting your Twitch and PUBG accounts lets StreamersEdge know when your live-stream begins and ends.',
        socialHeader:'Social Connections',
        gameHeader: 'Game Connections',
        connectionSelect: 'Connect these accounts and unlock special StreamersEdge integrations',
        connectionDescription: 'CONNECT YOUR ACCOUNTS',
        connectionLabel: 'Account Name'
      }
    },
    donate: {
      donateTo: 'Donate to: ',
      balance: 'Balance: ',
      usd: 'USD',
      fee: 'Fee: ',
      insufficientHeader: 'Not enough funds',
      insufficientSubHeader: 'We recommend adding funds to your account',
      successHeader: 'Success!',
      successSubHeader: 'Your donation was completed successfully',
      error: 'Error!',
      errorZero: 'Donation amount should be greater than zero'
    },
    ban: {
      primary: 'You have been banned',
      secondary: 'You have been banned for violating the ',
      secondaryUnderlined: 'StreamersEdge Terms Of Service.',
      contact: 'Contact Support',
      orSend: 'Send us an email to ',
      email: 'support@streamersedge.com'
    },
    link: {
      header: 'Confirm you want to link your account',
      unlinkHeader: 'Confirm you want to unlink your account',
      unlinkWarning: 'Warning: You won’t be able to win any challenge in which you have participated if your Peerplays account is not linked.',
      unlinkWarning2: 'If you don\'t have any accounts linked to StreamersEdge, you won\'t be able to login. Please make sure that you have a way to login.',
      terms: 'Terms & Conditions',
      pubgLinkFailed: 'Pubg Account Linking Failed',
      pubgLinkFailedText: 'The Pubg Nickname is invalid',
      pubgNickname: 'PUBG Nickname',
      pubgError: 'PUBG Nickname should be at least 4 characters long'
    },
    search: {
      resultFor: 'Result For ',
      viewall: 'View All',
      challenge: {
        label: 'Challenges',
        noChallenge: 'There are no challenges...'
      }
    },
    category: {
      title: 'Category',
      select: 'Select category'
    },
    leftMenu: {
      links: {
        challenges: 'All Challenges',
        categories: 'Categories',
        live: 'Live Challenges',
        popular: 'Popular Challenges',
        filters: 'Filters'
      },
      search: {
        placeholder: 'Search'
      },
      reset: 'Reset',
      defaultStatus: 'Open',
      categoryText: 'Category: ',
      challenger: 'Challenger: '
    },
    rightMenu: {
      links: {
        update: 'Update Profile',
        preferences: 'Preferences',
        create: 'Create Challenge',
        addFunds: 'Add Funds',
        redeem: 'Redeem Balance'
      },
      invite: {
        accept: 'Accepted Challenge Invites',
        new: 'New Invites'
      }
    },
    createChallenge: {
      header: 'Create Challenge',
      name: {
        label: 'Challenge Name',
        placeholder: 'Enter A Name For This Challenge'
      },
      date: {
        subHeader: 'This is the date and time your challenge will start',
        startDate: 'Start Date'
      },
      condition: {
        label: 'Challenge Conditions',
        add: 'Add',
        bounty: 'Bounty',
        conditions: {
          must: 'The User Must',
          and: 'And',
          or: 'Or'
        }
      },
      confirm: {
        header: 'Confirm Challenge',
        streamer: 'Streamer',
        startTime: 'Time To Start',
        rules: 'Rules'
      },
      errors: {
        name: {
          required: 'A challenge name is required',
          maximum: 'A challenge name is no longer than 50'
        },
        game: {
          required: 'A challenge game is required'
        },
        condition: {
          required: 'Add at least one condition',
          maximum: 'A maximum of 3 conditions can be added for a challenge'
        },
        invite: {
          required: 'Add at least one invited account'
        },
        deny: 'Link your Twitch and PUBG account to start creating challenges',
        timeToStart: 'Time to start must be greater than current time.'
      },
      modal: {
        failedHeader: 'Unexpected error',
        failedTransaction: 'Create transaction failed',
        error: 'Error!'
      },
      success: {
        header: 'Congratulations, challenge created',
        subHeader: 'Share it with your friends!',
        link: {
          label: 'Challenge Link'
        },
        redirect: {
          dashboard: 'You will be automatically redirected to the dashboard in 10 seconds, otherwise,',
          here: 'click here'
        }
      }
    },
    challengeDetails: {
      header: 'Challenge Details',
      streamer: 'Streamer',
      rules: 'Rules',
      sUSD: 'USD',
      currentBounty: 'Current Bounty',
      livestream: 'Live Stream',
      startDate: 'Start Date',
      previousDonations: 'Previous Donations',
      share: {
        header: 'Share This Challenge!'
      },
      errorPeerplaysHeader: 'Peerplays Acccount Missing',
      errorLinkPeerplaysAccount: 'Please connect a Peerplays Account by clicking the Update Profile link in the Right Menu to proceed',
      noDonations: 'No donations for current challenge'
    },
    tooltip: {
      createChallenge: 'Link your Twitch and PUBG accounts to create a challenge',
      searchChallenge: 'The Search text must be at least 3 characters long.'
    },
    copyright: 'Peerplays Global©',
    companyName: 'Peerplays Global',
    companyAddress1: '12 Mount Havelock',
    companyAddress2: 'Douglas, Isle of Man  IM1 2QG',
    terms: {
      heading: 'Terms Of Service',
      overview: {
        heading: 'Overview',
        // eslint-disable-next-line max-len
        content: 'This website for StreamersEdge App is operated by Peerplays Global Limited. Throughout the site, the terms “we”, “us”, and “our” refer to StreamersEdge. Peerplays Global offers this website, including all information, tools, and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies, and notices stated here. \n\nBy visiting our site and/or using our service, you engage in our “Service” and agree to be bound by the following terms and conditions (“Terms of Service”, “Terms”), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms of Service apply to all users of the site, including without limitation users who are browsers, vendors, customers, donators, merchants, and/or contributors of content. \n\nPlease read these Terms of Service carefully before accessing or using our website. By accessing or using any part of the site, you agree to be bound by these Terms of Service. If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any services. If these Terms of Service are considered an offer, acceptance is expressly limited to these Terms of Service. \n\nAny new features or tools which are added to the service shall also be subject to the Terms of Service. You can review the most current version of the Terms of Service at any time on this page. We reserve the right to update, change or replace any part of these Terms of Service by posting updates and/or changes to our website. It is your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.'
      },
      intro: {
        heading: 'Introduction',
        // eslint-disable-next-line max-len
        content: 'StreamersEdge is the #1 challenge creation tool for content creators. StreamersEdge offerings span live streaming desktop software, mobile application, Merch, and offers services for the Creator to customize, monetize, and optimize their live stream. \n\nStreamersEdge offers a free service. StreamersEdge is a service that allows viewers to send and receive tokenized value ("donations") through third-party payment processors. Our service also features a variety of tools and widgets that can utilize various data from our services, and data from authorized third parties.'
      },
      userTerms: {
        heading: 'User Terms',
        // eslint-disable-next-line max-len
        content: 'The StreamersEdge Services are not available to persons under the age of 13. If you are between the ages of 13 and 18 (or between 13 and the age of legal majority in your jurisdiction of residence), you may only use the StreamersEdge Services under the supervision of a parent or legal guardian who agrees to be bound by these Terms of Service. \n\nThe StreamersEdge Services are also not available to any users previously removed from the StreamersEdge Services by StreamersEdge. Finally, the StreamersEdge Services are not available to any persons barred from receiving them under the laws of the Isle of Man or applicable laws in any other jurisdiction.You may not use our service for any illegal or unauthorized purpose nor may you, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws). You must not transmit any worms or viruses or any code of a destructive nature. A breach or violation of any of the Terms will result in an immediate termination of your Services. \n\nBY DOWNLOADING, INSTALLING, OR OTHERWISE USING THE StreamersEdge SERVICES, YOU REPRESENT THAT YOU ARE AT LEAST 13 YEARS OF AGE, THAT YOUR PARENT OR LEGAL GUARDIAN AGREES TO BE BOUND BY THESE TERMS OF SERVICE IF YOU ARE BETWEEN 13 AND THE AGE OF LEGAL MAJORITY IN YOUR JURISDICTION OF RESIDENCE, AND THAT YOU HAVE NOT BEEN PREVIOUSLY REMOVED FROM OR PROHIBITED FROM RECEIVING THE StreamersEdge SERVICES.'
      },
      generalConditions: {
        heading: 'General Conditions',
        // eslint-disable-next-line max-len
        content: 'StreamersEdge App reserves the right to refuse service to anyone for any reason at any time. \n\nYou understand that your content, may be transferred unencrypted and involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices. \n\nYou agree not to reproduce, duplicate, copy, sell, resell, or exploit any portion of the Service, use of the Service, or access to the Service or any contact on the website through which the service is provided, without express written permission by us. \n\nThe headings used in this Agreement are included for convenience only and will not limit or otherwise affect these Terms.'
      },
      accuracy: {
        heading: 'Accuracy, Completeness and Timeliness of Information',
        // eslint-disable-next-line max-len
        content: 'We are not responsible if information made available on this site is not accurate, complete, or current. The material on this site is provided for general information only and should not be relied upon or used as the sole basis for making decisions without consulting primary, more accurate, more complete or timelier sources of information. Any reliance on the material on this site is at your own risk. \n\nThis site may contain certain historical information. Historical information, necessarily, is not current and is provided for your reference only. We reserve the right to modify the contents of this site at any time, but we have no obligation to update any information on our site. You agree that it is your responsibility to monitor changes to our site.'
      },
      modification: {
        heading: 'Modification to the Service and Prices',
        // eslint-disable-next-line max-len
        content: 'Prices and/or fees for our Service are subject to change without notice. \n\nWe reserve the right at any time to modify or discontinue the Service (or any part or content thereof) without notice at any time. \n\nWe shall not be liable to you or to any third-party for any modification, price change, suspension, or discontinuance of the Service.'
      },
      donations: {
        heading: 'Sending Donations',
        // eslint-disable-next-line max-len
        content: 'We allow you to send donations through this Service using multiple payment methods. We reserve the right to change these payment methods at any time. We reserve the right to impose limits on the number of transactions you can send through our Service. When sending donations, the recipient is not required to accept or acknowledge them. You agree that you will not hold us liable for any unclaimed or unacknowledged donations. \n\nBy sending a donation to the recipient, you agree that the card is your own and authorize us to charge each donation transaction in full. This charge is non-refundable, non-profitable, and/or exchangeable and cannot be withdrawn or charged back. You acknowledge that you are not receiving any goods/services in return for this donation. StreamersEdge App does not store any credit card information. \n\nYou, the Tipper, have the option to pay for the processing fee at the point of the donation transaction. By checking the fee option, you agree to authorize us to deduct any associated processing fees as needed by the payment processor. Due to currency exchange discrepancy from our API and our payment processor\'s API the total charge to the donator may off by a minimal amount and any overcharged fees will be given to the Creator. If you decide not to opt in to pay the fee, the Creator is fully responsible for it.'
      },
      thirdParty: {
        heading: 'Third Party Site Promotions',
        // eslint-disable-next-line max-len
        content: 'When using StreamersEdge, you may have the option to utilize a third party site to earn credits for the use of StreamersEdge as well as other promotional offers. You authorize StreamersEdge to share information with these third party sites if you choose to take part in the promotion. You additionally agree to the Terms of Service of these third party sites when utilizing them, as well as the Terms of Service of StreamersEdge when leaving the StreamersEdge site to take part in the third party site\'s interface to earn these credits or promotions. StreamersEdge and the third party service reserve the right to withhold points earned for any reason. StreamersEdge reserves the right to remove your account credit balance if you are in a breach of the Terms of Service, or suspected of a breach in the Terms of Service, on the third party site or on StreamersEdge.'
      },
      receivingTips: {
        heading: 'Receiving Tips',
        // eslint-disable-next-line max-len
        content: 'We reserve the right to collect a fee for donations received. Fees are subject to change without prior notification, it is your responsibility as the user to stay updated on Fees and changes to the Fees. When receiving donations, you are liable for any chargebacks or disputes that may occur thereafter in association with those transactions. We are not liable for any charges that may be incurred from these chargebacks or disputes. At any point our payment processors determine you are incurring excessive Chargebacks, your StreamersEdge account may result in additional controls and restrictions on your balance. \n\nYou agree to send any type of identification that is asked for in order to complete a withdrawal request. You agree to pay a fee for any withdrawal. If bank account information and country of issuance is incorrect, you are subject to a delay in your withdrawal and/or account deletion. If the country of issued bank account is incorrect, you will not be able to change it. You must contact StreamersEdge support immediately. \n\nBy accepting this agreement, you authorize us to hold, receive, and disburse funds on your behalf when such funds from the Card Networks are settled into your account. We may make available to you information in the StreamersEdge management dashboard regarding anticipated settlement amounts received on your behalf from the Card Networks and are being held pending settlement. This settlement information does not constitute a deposit or other obligation of StreamersEdge or our payment processor to you. This settlement information reflected in the StreamersEdge management dashboard is for reporting and informational purposes only, and you are not entitled to, and have no ownership or other rights in settlement funds, until such funds are credited to your designated bank settlement account. Your authorizations set forth herein will remain in full force and effect until your StreamersEdge account is closed or terminated. \n\nYou agree to pay all fees assessed by us to you for providing our payment processors services. Tippers have the option to pay for your fee at the point of the donation transaction. Due to currency exchange discrepancy from StreamersEdge\' API and our payment processor\'s API the total charge to the donator may off by a minimal amount and overcharged fees will be given to the Creator. If they decide not to opt in to pay the fee, you are fully responsible for it.',
        registration: {
          heading: '\u25CF Registration',
          // eslint-disable-next-line max-len
          content: 'To register your account to start receiving credit card donations, you must provide your personal information (which cannot be changed after registration) and bank account information. To enable withdrawals and transfers from your account, you must submit valid personal and bank information which will be sent for verification to our payment processor. This private information is never saved on StreamersEdge\' website. You will be required to submit valid government identification if requested by our payment processor for further verification. In consideration of use of the Service, you agree to maintain and update true, accurate, current and complete Registration Data. If you provide any information that is untrue, inaccurate, not current or incomplete, or if our payment processor has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, our payment processor may suspend or terminate your account and refuse any and all current or future use of the Service or any portion thereof. Failure to submit required information may limit your ability to withdraw and/or transfer your pending balance. \n\nTo register your account to start receiving PayPal donations, you must provide your email associated with your Paypal account. Failure to submit the correct information may limit your ability to receive donations.'
        },
        chargebacks: {
          heading: '\u25CF Chargebacks',
          // eslint-disable-next-line max-len
          content: 'In the event a Chargeback is issued, you are immediately liable for the full amount of the transaction related to the Chargeback. You are also liable for any associated fees, fines, expenses, or penalties. You agree that any associated fees, fines, or expenses will be deducted from your StreamersEdge balance (see Reserves). \n\nFor credit card donations, StreamersEdge will be elected to contest Chargebacks assessed against you. You agree to provide us with the necessary information, in a timely manner and at your expense, to investigate or help resolve any Chargeback. You also grant us permission to share records or other information required with financial institutions and Card Networks to help resolve any disputes. You acknowledge that your failure to provide us with complete and accurate information in a timely manner may result in an irreversible Chargeback being assessed. If the Chargeback is resolved in your favor, the Chargeback amount and any associated fees will be recovered to your StreamersEdge balance. \n\nAt any point our payment processors determine you are incurring excessive Chargebacks, your StreamersEdge account may result in additional controls and restrictions on your balance. \n\nFor Paypal donations, you are solely responsible for contesting Chargebacks and disputes. You are liable for any chargebacks or disputes that may occur thereafter in association with those transactions. We are not liable for any charges that may be incurred from these chargebacks or disputes.'
        },
        reserves: {
          heading: '\u25CF Reserves',
          // eslint-disable-next-line max-len
          content: 'You agree that a 60 day reserve may be accounted for on your balance to cover any Chargebacks at the point of credit card donation sign up. This reserve may decrease or increase depending on the history and activity of your account. \n\nIn certain circumstances, we may determine that a Reserve on your account is necessary to provide the payment services to you. You agree that StreamersEdge, in its sole discretion, will set the terms of a Reserve on your account, where needed. StreamersEdge will notify you of such terms, which may require that a certain amount (including the full amount) of the funds received for your transaction is held for a period of time or that additional amounts are held in a Reserve Account.'
        },
        withdrawal: {
          heading: '\u25CF Withdrawal/Transfers',
          // eslint-disable-next-line max-len
          content: 'You may transfer funds from your available account balance once your bank account information is completely filled on your StreamersEdge account. Funds from credit card donations are only available for transfer through bank accounts. You agree to send any type of identification that is asked for in order to complete a withdrawal request. You agree to pay a fee for any withdrawal. \n\nIf bank account information and country of issuance is incorrect, you are subject to a delay in your withdrawal and/or account deletion. If the country of issued bank account is incorrect, you will not be able to change it. You must contact StreamersEdge support immediately.'
        },
        handlingOfFunds: {
          heading: '\u25CF Handling of Funds',
          // eslint-disable-next-line max-len
          content: 'By accepting this agreement, you authorize us to hold, receive, and disburse funds on your behalf when such funds from the Card Networks are settled into your account. We may make available to you information in the StreamersEdge management dashboard regarding anticipated settlement amounts received on your behalf from the Card Networks and are being held pending settlement. This settlement information does not constitute a deposit or other obligation of StreamersEdge or our payment processor to you. This settlement information reflected in the StreamersEdge management dashboard is for reporting and informational purposes only, and you are not entitled to, and have no ownership or other rights in settlement funds, until such funds are credited to your designated bank settlement account. Your authorizations set forth herein will remain in full force and effect until your StreamersEdge account is closed or terminated.'
        },
        fees: {
          heading: '\u25CF Fees',
          // eslint-disable-next-line max-len
          content: 'You agree to pay all fees assessed by us to you for providing our payment processors services. Tippers have the option to pay for your fee at the point of the donation transaction. Due to currency exchange discrepancy from StreamersEdge\' API and our payment processor\'s API the total charge to the donator may off by a minimal amount and overcharged fees will be given to the streamer. If they decide not to opt in to pay the fee, you are fully responsible for it.'
        },
        privacy: {
          heading: '\u25CF Your Privacy',
          content: 'Your privacy is important to us, and you acknowledge that you have read in full agreement to our Privacy Policy.'
        }
      },
      tippers: {
        heading: 'Tippers',
        registration: {
          heading: '\u25CF Registration',
          // eslint-disable-next-line max-len
          content: 'By sending a donation to the recipient, you agree that the card is your own and authorize StreamersEdge to charge each donation transaction in full. This charge is non-refundable, non-profitable, and/or exchangeable and cannot be withdrawn or charged back. You acknowledge that you are not receiving any goods/services in return for this donation.'
        },
        donations: {
          heading: '\u25CF Sending Tips/Donations',
          // eslint-disable-next-line max-len
          content: 'StreamersEdge allows users to send donations through third party payment processors. We reserve the right to add or remove support for these third party payment processors at any time. The recipient of donations are not required to acknowledge or accept them. You agree that you will not hold StreamersEdge liable for any unclaimed or unacknowledged donations.'
        },
        fees: {
          heading: '\u25CF Fees',
          // eslint-disable-next-line max-len
          content: 'You, the Tipper, have the option to pay for the processing fee at the point of the donation transaction. By checking the fee option, you agree to authorize StreamersEdge to deduct any associated processing fees as needed by the payment processor. Due to currency exchange discrepancy from StreamersEdge\' API and our payment processor\'s API the total charge to the donator may off by a minimal amount and any overcharged fees will be given to the Creator. If you decide not to opt in to pay the fee, the Creator is fully responsible for it.'
        },
        privacy: {
          heading: '\u25CF Your Privacy',
          content: 'Your privacy is important to us, and you acknowledge that you have read in full agreement to our Privacy Policy.'
        },
        accountTermination: {
          heading: '\u25CF Account Termination',
          // eslint-disable-next-line max-len
          content: 'Participation in the Program is a privilege granted to Program Members, and as such can be suspended, revoked, or terminated at any time by us for any reason. If your Program Membership is terminated, all Points associated with your Program Membership and any Membership Benefits will be forfeited immediately upon termination. If your Program Membership is terminated due to fraudulent activity or noncompliance with these Program Terms, or the StreamersEdge Terms, in addition to forfeiting all Points and Membership Benefits associated with your Program Membership immediately, you cannot participate in or rejoin the Program.'
        },
        programChanges: {
          heading: '\u25CF Program Changes and Termination',
          // eslint-disable-next-line max-len
          content: 'We reserve the right to change or terminate the Program, or any part thereof, at any time without notice and without further obligations to Program Members, including, but not limited to modifications which: a) govern how Points are earned on and after the date of change; or b) change the value of benefits. No Points will be earned or redeemed after the effective date of termination. Any and all changes and/or amendments to these Program Terms will become binding upon all members immediately.'
        }
      },
      twitchAPI: {
        heading: 'Twitch API',
        // eslint-disable-next-line max-len
        content: 'StreamersEdge utilizes Twitch\'s API, and as a user of StreamersEdge, you are agreeing to be bound to Twitch\'s Terms of Service and Privacy Policy. You can find more information on Twitch\'s terms at https://www.twitch.tv/p/legal/terms-of-service/.'
      },
      youtubeAPI: {
        heading: 'Youtube API',
        // eslint-disable-next-line max-len
        content: 'StreamersEdge utilizes YouTube\'s API, and as a user of StreamersEdge, you are agreeing to be bound to YouTube\'s Terms of Service and Privacy Policy. You can find more information on YouTube\'s terms at https://www.youtube.com/static?template=terms, and Google\'s terms at https://policies.google.com/terms?hl=en&gl=us.'
      },
      facebookAPI: {
        heading: 'Facebook API',
        // eslint-disable-next-line max-len
        content: 'StreamersEdge utilizes Facebook\'s API, and as a user of StreamersEdge, you are agreeing to be bound to Facebook\'s Terms of Service and Privacy Policy. You can find more information on Facebook\'s terms at https://www.facebook.com/terms.php.'
      },
      steamAPI: {
        heading: 'Steam API',
        // eslint-disable-next-line max-len
        content: 'StreamersEdge utilizes Steam\'s API, and as a user of StreamersEdge, you are agreeing to be bound to Steam\'s Terms of Service and Privacy Policy. You can find more information on Steam\'s terms at https://store.steampowered.com/subscriber_agreement/.'
      },
      creditCardTerms: {
        heading: 'Credit Card Terms',
        // eslint-disable-next-line max-len
        content: 'StreamersEdge utilizes third party service Paypal to process credit cards. By agreeing to StreamersEdge\'s Terms of Service, you are also agreeing to Paypal\'s Terms of Service, located at https://www.paypal.com/ca/webapps/mpp/ua/useragreement-full. \n\nIn the event a Chargeback is issued, you are immediately liable for the full amount of the transaction related to the Chargeback. You are also liable for any associated fees, fines, expenses, or penalties. You agree that any associated fees, fines, or expenses will be deducted from your StreamersEdge or Campaign balance. \n\nAt any point our payment processors determine you are incurring excessive Chargebacks, your StreamersEdge account may result in additional controls and restrictions on your balance.'
      },
      merch: {
        heading: 'StreamersEdge MERCH',
        userContent: {
          heading: '\u25CF User Content',
          // eslint-disable-next-line max-len
          content: 'You grant StreamersEdge a license to use the User Content and materials you post to the Site. By posting, downloading, displaying, performing, transmitting, or otherwise distributing User Content to the Site, you are granting StreamersEdge, its affiliates, officers, directors, employees, consultants, agents, and representatives a license to use User Content in connection with the operation of the Internet business of StreamersEdge, its affiliates, officers, directors, employees, consultants, agents, and representatives, including without limitation, a right to copy, distribute, transmit, publicly display, publicly perform, reproduce, edit, translate, and reformat User Content. You agree that StreamersEdge may publish or otherwise disclose your name in connection with your User Content. By posting User Content on the Site, you warrant and represent that you own the rights to the User Content or are otherwise authorized to post, distribute, display, perform, transmit, or otherwise distribute User Content.'
        },
        complianceIPR: {
          heading: '\u25CF Compliance with Intellectual Property Laws',
          // eslint-disable-next-line max-len
          content: 'When accessing or using the Site, you agree to obey the law and to respect the intellectual property rights of others. Your use of the Site is at all times governed by and subject to laws regarding copyright, trademark, patent, and trade secret ownership and use of intellectual property. You agree not to upload, download, display, perform, transmit, or otherwise distribute any information or Content in violation of any party\'s copyrights, trademarks, patents, trade secrets, or other intellectual property or proprietary rights. You agree to abide by laws regarding copyright, trademark, patent, and trade secret ownership and use of intellectual property, and you shall be solely responsible for any violations of any laws and for any infringements of any intellectual property rights caused by any Content you provide, post, or transmit, or that is provided or transmitted using your user name or user ID. The burden of proving that any Content does not violate any laws or intellectual property rights rests solely with you.'
        },
        ownership: {
          heading: '\u25CF Intellectual Property Ownership',
          // eslint-disable-next-line max-len
          content: 'All StreamersEdge Content included on the Site and Service, such as text, graphics, logos, button icons, images, audio and/or video media, digital downloads, data compilations, and Software, is the property of StreamersEdge and is protected by Isle of Man and international intellectual property laws. The compilation of all content on this Site is the exclusive property of StreamersEdge and protected by Isle of Man and international copyright laws. All software used on this site is the property of StreamersEdge or its software suppliers and protected by Isle of Man and international intellectual property laws. You may not frame or utilize framing techniques to enclose any trademark, logo, or other proprietary information (including images, text, page layout, or form) of StreamersEdge and our affiliates without express written consent. You may not use any meta-tags or any other "hidden text" utilizing StreamersEdge name or trademarks without the express written consent of StreamersEdge. You may not use any direct linking or source-calling of any media presented on this website.'
        },
        infringementIPR: {
          heading: '\u25CF Infringement of Intellectual Property Rights',
          // eslint-disable-next-line max-len
          content: 'StreamersEdge has in place certain legally mandated procedures regarding allegations of copyright and other forms of intellectual property infringement occurring on the Site. StreamersEdge\'s policy is to investigate any allegations of intellectual property infringement brought to its attention. If you have evidence, know, or have a good faith belief that your rights or the rights of a third party have been violated and you want StreamersEdge to delete, edit, or disable the material in question, you must provide StreamersEdge with all of the following information: \n(a) a physical or electronic signature of a person authorized to act on behalf of the owner of the exclusive right that is allegedly infringed; \n(b) identification of the subject work claimed to have been infringed, or, if multiple works are covered by a single notification, a representative list of such works; \n(c) identification of the material that is claimed to be infringed or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit StreamersEdge to locate the material; \n(d) information reasonably sufficient to permit StreamersEdge to contact you, such as an address, telephone number, and if available, an electronic mail address at which you may be contacted; \n(e) a statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and \n(f) a statement that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed. For this notification to be effective, you must provide it to StreamersEdge\'s designated agent at: legal@streameredge.com'
        },
        prohibitedContent: {
          heading: '\u25CF Prohibited Content',
          // eslint-disable-next-line max-len
          content: 'You shall not make the following types of Content available. You agree not to upload, download, display, perform, transmit, or otherwise distribute any Content that:\n\u25CF is libelous, defamatory, obscene, pornographic, abusive, or threatening, or that you know is false or misleading; \n\u25CF advocates or encourages conduct that could constitute a criminal offense, give rise to civil liability, or otherwise violate any applicable local, state, national, or foreign law or regulation; \n\u25CF is patently offensive or promotes or otherwise incites racism, bigotry, hatred or physical harm of any kind against any group or individual; \n\u25CF harasses or advocates harassment of another person; \n\u25CF exploits people in a sexual or violent manner; \n\u25CF contains nudity, excessive violence, or offensive subject matter or contains a link to an adult website; \n\u25CF solicits or is designed to solicit personal information from or about any minor; \n\u25CF contains information that poses or creates a privacy or security risk to any person; \n\u25CF constitutes or promotes an illegal or unauthorized copy of another person\'s copyrighted work, such as providing pirated computer programs or links to them, providing information to circumvent manufacturer-installed copy-protect devices, or providing pirated music or links to pirated music files; \n\u25CF involves the transmission of "junk mail," "chain letters," or unsolicited mass mailing, instant messaging, "spimming," or "spamming"; \n\u25CF contains restricted or password only access pages or hidden pages or images (those not linked to or from another accessible page); \n\u25CF solicits or is designed to solicit passwords or personal identifying information from other Users; \n\u25CF involves commercial activities and/or sales without prior written consent from StreamersEdge; \n\u25CF includes a photograph or video of another person that you have posted without that person\'s consent; or \n\u25CF violates or attempts to violate the privacy rights, publicity rights, intellectual property rights, contract rights, or any other rights of any person. \n\nStreamersEdge reserves the right to terminate your receipt, transmission, or other distribution of any such material using the Site or Service, and, if applicable, to delete any such material from its servers. StreamersEdge intends to cooperate fully with any law enforcement officials or agencies in the investigation of any violation of these Terms of Use or of any applicable laws.'
        },
        channelBilling: {
          heading: '\u25CF Channel Initiated Billing',
          // eslint-disable-next-line max-len
          content: 'StreamersEdge utilizes PayPal\'s Channel Initiated Billing, to process Merch payments. Should you choose to opt-in to Channel Initiated Billing, you agree to be bound to PayPal\'s Terms and Privacy Policy.'
        }
      },
      prohibitedConduct: {
        heading: 'Prohibited Conduct',
        // eslint-disable-next-line max-len
        content: 'You are also prohibited from engaging in the following activities, or assisting others in engaging in the following activities, in using the Site or Services: \n\u25CF threatening, stalking, defrauding another person, or inciting, harassing, or advocating the harassment of another person, or otherwise interfering with another user\'s use of the Site; \n\u25CF using the Site in a manner that may create a conflict of interest, such as trading reviews with other business owners or writing or soliciting shill reviews; \n\u25CF using the Site to promote bigotry or discrimination; \n\u25CF using the Site to solicit personal information from minors or to harm or threaten to cause harm to minors; \n\u25CF using the site for commercial or promotional purposes, advertising or otherwise solicits funds or is a solicitation for goods or services, displaying an unauthorized commercial advertisement, or accepting payment or anything of value from a third person in exchange for your performing any commercial activity through the unauthorized or impermissible use of the Site or Service on behalf of that person, such as placing commercial content in a product review, placing links to e-commerce sites not authorized by StreamersEdge in a product review, placing links to blogs or forums with a commercial purpose, or otherwise attempting to post messages or advertisements with a commercial purpose; \n\u25CF engaging in criminal or tortious activity, including, but not limited to, fraud, harassment, defamation, stalking, spamming, spimming, sending of viruses or other harmful files, copyright infringement, or theft of trade secrets; \n\u25CF accessing content or data not intended for you, or logging onto a server or account that you are not authorized to access; \n\u25CF attempting to probe, scan, or test the vulnerability of the Site or any associated system or network, or to breach security or authentication measures without proper authorization, including circumventing or modifying, attempting to circumvent or modify, or encouraging or assisting any other person in circumventing or modifying any security technology or software that is part of the Site or Service; \n\u25CF interfering or attempting to interfere with service to any User, host, or network, including, without limitation, by means of submitting a virus to the Site, overloading, "flooding," "spamming," "mail bombing," or "crashing;" \n\u25CF using the Site to send unsolicited e-mails, including, without limitation, promotions, or advertisements for products or services; \n\u25CF forging any TCP/IP packet header or any part of the header information in any e-mail or in any posting; \n\u25CF attempting to modify, reverse-engineer, decompile, disassemble, or otherwise reduce or attempt to reduce to a human-perceivable form any of the source code used by StreamersEdge in providing the Site; \n\u25CF using the Site for keyword spamming or to otherwise attempt to manipulate natural search results; recording, processing, or mining information about other users; \n\u25CF using any viruses, bots, worms, or any other computer code, files or programs that interrupt, destroy or limit the functionality of any computer software or hardware, or otherwise permit the unauthorized use of or access to a computer or a computer network, or using any other automated system in order to harvest e-mail addresses or other data from the Site or Service for the purposes of sending unsolicited or unauthorized material; \n\u25CF modifying, copying, distributing, downloading, scraping or transmitting in any form or by any means, in whole or in part, any Content from the StreamersEdge Services other than your User Content which you legally post on, through or in connection with your use of the Site; \n\u25CF providing or using "tracking" or monitoring functionality in connection with the Site or Service, including, without limitation, to identify other Users\' views, actions or other activities on the Site; \n\u25CF interfering with, disrupting, or creating an undue burden on the StreamersEdge\'s Site or the networks or services connected to the StreamersEdge\'s Site; \n\u25CF impersonating or attempting to impersonate StreamersEdge or a StreamersEdge employee, administrator or moderator, another User, or person or entity (including, without limitation, the use of e-mail addresses associated with or of any of the foregoing); \n\u25CF using or distributing any information obtained from the StreamersEdge\'s Site in order to harass, abuse, or harm another person or entity, or attempting to do the same; \n\u25CF using invalid or forged headers to disguise the origin of any Content transmitted to or through StreamersEdge\'s computer systems, or otherwise misrepresenting yourself or the source of any message or Content; \n\u25CF engaging in, either directly or indirectly, or encouraging others to engage in, click-throughs generated through any manner that could be reasonably interpreted as coercive, incentivized, misleading, malicious, or otherwise fraudulent; or \n\u25CF using the Site in a manner inconsistent with any and all applicable laws and regulations.'
      },
      accuracyOfBilling: {
        heading: 'Accuracy of Billing and Account Information',
        // eslint-disable-next-line max-len
        content: 'You agree to provide current, complete, and accurate purchase and account information for all transactions occurring on our Service. You agree to promptly update your account and other information so that we can complete your transactions and contact you as needed.'
      },
      taxes: {
        heading: 'Taxes',
        // eslint-disable-next-line max-len
        content: 'You are responsible to determine if any taxes apply to the donations sent or received using our Service. You are responsible for any taxes applicable to earnings through the StreamersEdge platform.'
      },
      optionalTools: {
        heading: 'Optional Tools',
        // eslint-disable-next-line max-len
        content: 'We may provide you with access to third-party tools of which we neither monitor nor have control over. \n\nYou acknowledge and agree that we provide access to such tools “as is” and “as available” without any warranties, representations or conditions of any kind and without any endorsement. We shall have no liability whatsoever arising from or relating to your use of optional third-party tools. \n\nAny use by you of optional tools offered through the site is entirely at your own risk and discretion and you should ensure that you are familiar with and approve of the terms on which tools are provided by the relevant third-party provider(s). \n\nWe may also, in the future, offer new services and/or features through the website (including, the release of new tools and resources). Such new features and/or services shall also be subject to these Terms of Service.'
      },
      thirdPartyLinks: {
        heading: 'Third Party Links',
        // eslint-disable-next-line max-len
        content: 'Certain content and services available via our Service may include materials from third-parties. \n\nThird-party links on this site may direct you to third-party websites that are not affiliated with us. We are not responsible for examining or evaluating the content or accuracy and we do not warrant and will not have any liability or responsibility for any third-party materials or websites, or for any other materials, products, or services of third-parties. \n\nWe are not liable for any harm or damages related to the purchase or use of goods, services, resources, content, or any other transactions made in connection with any third-party websites. Please review carefully the third-party\'s policies and practices and make sure you understand them before you engage in any transaction. Complaints, claims, concerns, or questions regarding third-party products should be directed to the third-party.'
      },
      userComments: {
        heading: 'User Comments, Feedback and Other Submissions',
        // eslint-disable-next-line max-len
        content: 'If, at our request, you send certain specific submissions (for example contest entries) or without a request from us you send creative ideas, suggestions, proposals, plans, or other materials, whether online, by email, by postal mail, or otherwise (collectively, \'comments\'), you agree that we may, at any time, without restriction, edit, copy, publish, distribute, translate and otherwise use in any medium any comments that you forward to us. We are and shall be under no obligation (1) to maintain any comments in confidence; (2) to pay compensation for any comments; or (3) to respond to any comments. \n\nWe may, but have no obligation to, monitor, edit or remove content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party\'s intellectual property or these Terms of Service. \n\nYou agree that your comments will not violate any right of any third-party, including copyright, trademark, privacy, personality or other personal or proprietary right. You further agree that your comments will not contain libelous or otherwise unlawful, abusive or obscene material, or contain any computer virus or other malware that could in any way affect the operation of the Service or any related website. You may not use a false e-mail address, pretend to be someone other than yourself, or otherwise mislead us or third-parties as to the origin of any comments. You are solely responsible for any comments you make and their accuracy. We take no responsibility and assume no liability for any comments posted by you or any third-party.'
      },
      personalInfo: {
        heading: 'Personal Information',
        content: 'Your submission of personal information through the service is governed by our Privacy Policy, which is available on our Privacy Policy.'
      },
      errorAndOmissions: {
        heading: 'Errors, Inaccuracies and Omissions',
        // eslint-disable-next-line max-len
        content: 'Occasionally there may be information on our site or in the Service that contains typographical errors, inaccuracies or omissions that may relate to descriptions, pricing, promotions, offers, and availability. We reserve the right to correct any errors, inaccuracies or omissions, and to change or update information or cancel orders if any information in the Service or on any related website is inaccurate at any time without prior notice (including after you have submitted your order). \n\nWe undertake no obligation to update, amend or clarify information in the Service or on any related website, including without limitation, pricing information, except as required by law. No specified update or refresh date applied in the Service or on any related website, should be taken to indicate that all information in the Service or on any related website has been modified or updated.'
      },
      prohibitedUses: {
        heading: 'Prohibited Uses',
        // eslint-disable-next-line max-len
        content: 'In addition to other prohibitions as set forth in the Terms of Service, you are prohibited from using the site or its content: \n(a) for any unlawful purpose; \n(b) to solicit others to perform or participate in any unlawful acts; \n(c) to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances; \n(d) to infringe upon or violate our intellectual property rights or the intellectual property rights of others; \n(e) to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability; \n(f) to submit false or misleading information; \n(g) to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related website, other websites, or the Internet; \n(h) to collect or track the personal information of others; \n(i) to spam, phish, pharm, pretext, spider, crawl, or scrape; \n(j) for any obscene or immoral purpose; or \n(k) to interfere with or circumvent the security features of the Service or any related website, other websites, or the Internet. \n(l) for any campaign that you partake in as a publisher, we reserve the right to review the campaign, your content, your materials in connection with the campaign, and reserve the right to remove you from said campaign at our sole discretion. \n(m) for viewbotting in connection to a Campaign, if we determine or assume that your account has taken part in viewbotting, whether by third parties our yourself, we reserve the right to ban your account and remove your earnings at our sole discretion.\n\nWe reserve the right to terminate your use of the Service, Campaign or any related website for violating any of the prohibited uses.'
      },
      disclaimerOfWarranties: {
        heading: 'Disclaimer of Warranties; Limitation of Liability',
        // eslint-disable-next-line max-len
        content: 'We do not guarantee, represent or warrant that your use of our service will be uninterrupted, timely, secure or error-free. \n\nWe do not warrant that the results that may be obtained from the use of the service will be accurate or reliable. \n\nYou agree that from time to time we may remove the service for indefinite periods of time or cancel the service at any time, without notice to you. \n\nYou expressly agree that your use of, or inability to use, the service is at your sole risk. The service and all products and services delivered to you through the service are (except as expressly stated by us) provided \'as is\' and \'as available\' for your use, without any representation, warranties or conditions of any kind, either express or implied, including all implied warranties or conditions of merchantability, merchantable quality, fitness for a particular purpose, durability, title, and non-infringement. \n\nIn no case shall StreamersEdge Inc., our directors, officers, employees, affiliates, agents, contractors, interns, suppliers, service providers or licensors be liable for any injury, loss, claim, or any direct, indirect, incidental, punitive, special, or consequential damages of any kind, including, without limitation lost profits, lost revenue, lost savings, loss of data, replacement costs, or any similar damages, whether based in contract, tort (including negligence), strict liability or otherwise, arising from your use of any of the service or any products procured using the service, or for any other claim related in any way to your use of the service or any product, including, but not limited to, any errors or omissions in any content, or any loss or damage of any kind incurred as a result of the use of the service or any content (or product) posted, transmitted, or otherwise made available via the service, even if advised of their possibility.'
      },
      indemnification: {
        heading: 'Indemnification',
        // eslint-disable-next-line max-len
        content: 'You agree to indemnify, defend and hold harmless StreamersEdge, Inc. and our parent, subsidiaries, affiliates, partners, officers, directors, agents, contractors, licensors, service providers, subcontractors, suppliers, interns and employees, harmless from any claim or demand, including reasonable attorneys\' fees, made by any third-party due to or arising out of your breach of these Terms of Service or the documents they incorporate by reference, or your violation of any law or the rights of a third-party.'
      },
      severability: {
        heading: 'Severability',
        // eslint-disable-next-line max-len
        content: 'In the event that any provision of these Terms of Service is determined to be unlawful, void or unenforceable, such provision shall nonetheless be enforceable to the fullest extent permitted by applicable law, and the unenforceable portion shall be deemed to be severed from these Terms of Service, such determination shall not affect the validity and enforceability of any other remaining provisions.'
      },
      termination: {
        heading: 'Termination',
        // eslint-disable-next-line max-len
        content: 'The obligations and liabilities of the parties incurred prior to the termination date shall survive the termination of this agreement for all purposes. \n\nThese Terms of Service are effective unless and until terminated by either you or us. You may terminate these Terms of Service at any time by notifying us that you no longer wish to use our Services, or when you cease using our site. \n\nIf in our sole judgment you fail, or we suspect that you have failed, to comply with any term or provision of these Terms of Service, we also may terminate this agreement at any time without notice and you will remain liable for all amounts due up to and including the date of termination; and/or accordingly may deny you access to our Services (or any part thereof).'
      },
      entireAgreement: {
        heading: 'Entire Agreement',
        // eslint-disable-next-line max-len
        content: 'The failure of us to exercise or enforce any right or provision of these Terms of Service shall not constitute a waiver of such right or provision. \n\nThese Terms of Service and any policies or operating rules posted by us on this site or in respect to The Service constitutes the entire agreement and understanding between you and us and govern your use of the Service, superseding any prior or contemporaneous agreements, communications and proposals, whether oral or written, between you and us (including, but not limited to, any prior versions of the Terms of Service). \n\nAny ambiguities in the interpretation of these Terms of Service shall not be construed against the drafting party.'
      },
      contactInfo: {
        heading: 'Contact Information',
        content: 'Questions about the Terms of Service should be sent to community@streamersedge.com.'
      }
    }
  }
};
