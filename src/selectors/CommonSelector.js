import {createSelector} from 'reselect';

const getCurrentAccount = (state) => state.getIn(['profiles', 'currentAccount']);

const hasUserUnlockedChallengeCreate = createSelector(
  [getCurrentAccount], ((currentUser) => {
    let unlocked = false;

    if (currentUser) {
      const userType = currentUser.get('userType');
      unlocked = userType.toLowerCase() === 'gamer';
    }

    return unlocked;
  })
);

const CommonSelecter = {
  getCurrentAccount,
  hasUserUnlockedChallengeCreate
};

export default CommonSelecter;