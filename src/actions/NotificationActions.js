import ActionTypes from './ActionTypes';
import {NotificationService} from '../services';
import {Action} from 'redux';

/**
 * Public actions related to Notification actions.
 *
 * @class NotificationActions
 */
class NotificationActions {
  /**
   * Initializes the web push notifications.
   *
   * @static
   * @memberof NotificationActions
   * @returns {Action}
   */
  static initialize() {
    return async (dispatch) => {
      if (!NotificationService.isSupported()) {
        await dispatch(NotificationActions.setNotSupported());
        return;
      }

      const permission = await NotificationService.requestPermission();
      await dispatch(NotificationActions.setPermissionGranted(permission));

      if (!permission) {
        return;
      }

      await NotificationService.initializeWorker();
    };
  }

  /**
   * Sets whether notifications are supported by the user agent.
   *
   * @static
   * @memberof NotificationActions
   * @returns {Action}
   */
  static setNotSupported() {
    return {type: ActionTypes.NOTIFICATION_NOT_SUPPORTED};
  }

  /**
   * Sets whether the user has granted permission to show notifications.
   *
   * @static
   * @memberof NotificationActions
   * @param {boolean} isGranted - Whether permission has been granted.
   * @returns {Action}
   */
  static setPermissionGranted(isGranted) {
    return {type: isGranted ? ActionTypes.NOTIFICATION_PERMISSION_GRANTED : ActionTypes.NOTIFICATION_PERMISSION_DENIED};
  }
}

export default NotificationActions;
