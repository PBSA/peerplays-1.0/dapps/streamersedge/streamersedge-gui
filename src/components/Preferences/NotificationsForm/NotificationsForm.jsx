import React, {Component} from 'react';
import {ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import styles from '../MUI.css';
import {withStyles} from '@material-ui/core/styles';

class NotificationsForm extends Component {

  render() {
    const {classes, notificationText, notificationIcon} = this.props;
    return(
      <div className='notifications'>
        <ExpansionPanel className={ classes.expansion } defaultExpanded>
          <ExpansionPanelSummary
            expandIcon={ <ExpandMoreIcon className='expand-icon'/> }
            aria-controls='panel1a-content'
            id='panel1a-header'
            classes={ {content: classes.header} }
          >
            <img className='notifications__image' src={ notificationIcon } alt=''/>
            <p className='notifications__text'>{notificationText}</p>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className='details'>
            {this.props.children}
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </div>
    );
  }
}

export default withStyles(styles)(NotificationsForm);