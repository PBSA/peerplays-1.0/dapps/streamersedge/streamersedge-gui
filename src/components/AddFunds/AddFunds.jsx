import React, {Component} from 'react';
// import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {ModalActions} from '../../actions';
import {GenUtil,StorageUtil} from '../../utility';
import {CancelButton, PaypalLogo, PaypalArrow} from '../../assets/images';

import {PaypalService} from '../../services';
import CircularProgress from '@material-ui/core/CircularProgress';
import {TextField, Button} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';
import styles from './Mui.css';
import {ModalTypes} from '../../constants';

const translate = GenUtil.translate;


class AddFunds extends Component {
  state = {
    loading: false,
    amount: ''
  }

  componentWillUnmount() {
    this.setState({loading: false});
  }

  handleChange = (event) => {
    const {name, value} = event.target;

    if (name === 'amount') {
      const newValue = value
        .replace(/[^\d.]/g, '')             // numbers and decimals only
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
        .replace(/(\.[\d]{2})./g, '$1');    // not more than 2 digits after decimal
      this.setState({
        [name]: newValue
      });
    } else {
      this.setState({
        [name]: value
      });
    }
  }

  handleCancel = () => {
    // TODO: when implementing this for production, revisit the logic here for the following use cases:
    // - when the "CANCEL" button needs to revert to the previous modal
    // - when the "CANCEL" button needs to redirect to another route
    // CURRENTLY, will only reload the previous modal as that is the only method of getting to the AddFunds modal type.

    this.props.toggleModal();
  }

  onError = (error) => {
    console.error('Erroneous payment OR failed to load script!', error);
    this.props.setModalData({headerText: translate('addFunds.error'), subText: error});
    this.props.setModalType(ModalTypes.ERROR);
  }

  Submit = async () => {
    if(!this.state.amount || Number(this.state.amount)<=0) {
      this.onError(translate('errors.addFunds.zero'));
      return;
    }

    if(Number(this.state.amount) > 10000) {
      this.onError(translate('errors.addFunds.amountHigh'));
      return;
    }

    this.setState({
      loading:true
    });

    try{
      const approvalUrl = await PaypalService.createPaypalPayment(this.state.amount,'USD');
      StorageUtil.set('se-page', this.props.path);
      window.location.href = approvalUrl;
    }catch(err) {
      this.setState({loading:false});

      if(err.message && err.message.includes('401')) {
        this.props.setModalType(ModalTypes.ERROR);
        this.props.setModalData({
          headerText: translate('errors.loggedOut')
        });
        return;
      }

      this.onError(err.message);
    }
  }

  render () {
    const {classes} = this.props;
    let content;

    if (this.state.loading) {
      content = <div className='add-funds'><CircularProgress className='paypal__loader'/></div>;
    } else {
      content = <>
        <div className='add-funds'>
          <p className='add-funds__left--italic'>{translate('addFunds.specify')}</p>
          <div className='funds__input'>
            <TextField
              name='amount'
              placeholder={ '100.00' }
              variant='outlined'
              className={ 'funds__textbox' }
              onChange={ this.handleChange }
              value={ this.state.amount }
              InputProps={ {
                classes: {
                  input: classes.donateTextbox,
                  notchedOutline: classes.donateBorder
                }
              } }
            />
            <p className='funds__label'>{translate('addFunds.sUsd')}</p>
          </div>
        </div>
        <div className='add-funds'>
          <p className='add-funds__left--italic'>{translate('addFunds.fiat')}</p>
          <Button onClick={ this.Submit } >
            <img src={ PaypalLogo } alt='Paypal' className='add-funds__paypal--paypal-img'/>
            <img src={ PaypalArrow } alt='arrow' className='add-funds__paypal--arrow-img'/>
          </Button>
        </div>
        <div className='add-funds__footer'>
          <img src={ CancelButton } alt='cancel button' className='btn--cancel' onClick={ this.handleCancel }/>
        </div>
      </>;
    }

    return (<>{content}</>);
  }
}

const mapStateToProps = (state) => {
  return {
    path: state.getIn(['router', 'location', 'pathname'])
  };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(AddFunds));