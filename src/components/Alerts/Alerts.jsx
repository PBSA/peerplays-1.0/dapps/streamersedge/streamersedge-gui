import React, {Component} from 'react';
import ChallengeService from '../../services/ChallengeService';
import moment from 'moment';
import {SELogoStacked} from '../../assets/images';

class Alerts extends Component {
  state = {
    userId: null,
    joinedUsers: []
  }

  componentDidMount() {
    const path = this.props.pathname;
    const userId = path.split('/')[2];

    this.fetchAndUpdateJoinedUsers(userId);
    this.timer = setInterval(()=> this.fetchAndUpdateJoinedUsers(userId), 5000);
  }

  componentDidUpdate() {
    const path = this.props.pathname;
    const userId = path.split('/')[2];

    // eslint-disable-next-line eqeqeq
    if(this.state.userId != userId) {
      clearInterval(this.timer);
      this.fetchAndUpdateJoinedUsers(userId);
      this.timer = setInterval(()=> this.fetchAndUpdateJoinedUsers(userId), 5000);
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer);
    this.timer = null;
  }

  fetchAndUpdateJoinedUsers(userId) {
    this.setState({userId});

    ChallengeService.getChallengesByUserId(userId).then((res) => {
      let joinedUsers = [];
      res.forEach((challenge) => {
        challenge.joinedUsers.map((donation)=> {
          if(moment(donation.createdAt).diff(moment()) > -5000) {
            joinedUsers.push({challenge: challenge.name, donation: donation});
          }

          return true;
        });
      });

      this.setState({
        joinedUsers
      });
    });
  }

  render() {
    const {joinedUsers} = this.state;

    return (<div className='alerts'>
      <div>
        <img src={ SELogoStacked } alt='StreamersEdge'/>
      </div>
      {joinedUsers.map((joinedUser,key)=>{
        return (
          <div key={ key } className='alerts__joinedUsers'>
            <span className='alerts__joinedUsers-username'>
              {joinedUser.donation.user.username}
            </span>
            <span className='alerts__joinedUsers-text'>{' donated '}</span>
            <span className='alerts__joinedUsers-amount'>{`$${joinedUser.donation.ppyAmount}`}</span>
            <span className='alerts__joinedUsers-text'>{' to StreamerEdge challenge '}</span>
            <span className='alerts__joinedUsers-challenge'> {joinedUser.challenge}</span>
          </div>);
      })}
    </div>);
  }
}

export default Alerts;