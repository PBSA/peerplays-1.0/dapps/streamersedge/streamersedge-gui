import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import classNames from 'classnames';
import UserDetails from './UserDetails';
import NavLink from '../NavLink';
import {GenUtil} from '../../utility';
import {ModalTypes} from '../../constants';
import {ModalActions} from '../../actions';

const trans = GenUtil.translate;

class RightMenu extends Component {
  state = {
    links: [
      {title: trans('rightMenu.links.update'), href: '/update-profile'},
      {title: trans('rightMenu.links.preferences'), href: '/preferences'},
      {title: trans('rightMenu.links.create'), href: '/create-challenge'},
      {title: trans('rightMenu.links.redeem'), href: '/redeem'},
      {title: trans('rightMenu.links.addFunds'), click: (e) => this.handleAddFunds(e)}
    ]
  };

  handleAddFunds = (e) => {
    e.preventDefault();
    this.props.setModalType(ModalTypes.ADD_FUNDS);
    this.props.toggleModal();
  }

  render() {
    return (
      <>
        <div className={ classNames('right-menu', {'right-menu__open': this.props.open}) }>
          <UserDetails />
          <NavLink links={ this.state.links } className='right-menu-links'/>
        </div>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    setModalType: ModalActions.setModalType,
    toggleModal: ModalActions.toggleModal
  },
  dispatch
);

export default connect(null, mapDispatchToProps)(RightMenu);
