import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import styles from './MUI.css';
import {withStyles} from '@material-ui/core/styles';
import UserInfo from './UserInfo';
import AccountConnections from './AccountConnections';
import {ProfileService} from '../../services';
import {ModalActions, NavigateActions, AccountActions} from '../../actions';
import {ModalTypes} from '../../constants';
import {GenUtil, ValidationUtil} from '../../utility';
import {PeerplaysIcon, YoutubeIcon, TwitchIcon, FacebookIcon, PubgIcon} from '../../assets/images/updateProfile';
import CommonSelecter from '../../selectors/CommonSelector';
const translate = GenUtil.translate;

class UpdateProfile extends Component {
  constructor(props) {
    super(props);

    const {twitchUsername, youtubeUsername, facebookUsername, pubgUsername, peerplaysAccountName} = props;
    const path = this.props.location.pathname;
    const pathAry = path.split('/')[2];

    this.state = this.constructState(twitchUsername, youtubeUsername, facebookUsername, pubgUsername, peerplaysAccountName,pathAry);
  }

  componentDidMount() {
    ProfileService.getProfile().then((res) => {
      this.props.setAccount(res);
    }).catch((err) => {
      this.show401Error(err);
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props.account !== prevProps.account) {
      const {twitchUsername, youtubeUsername, facebookUsername, pubgUsername, peerplaysAccountName} = this.props;
      const path = this.props.location.pathname;
      const pathAry = path.split('/')[2];
      this.setState(this.constructState(twitchUsername, youtubeUsername, facebookUsername, pubgUsername, peerplaysAccountName,pathAry));
    }
  }

  constructState = (twitchUsername, youtubeUsername, facebookUsername, pubgUsername, peerplaysAccountName,pathAry) => {
    return {
      currentStep: pathAry || '1',
      email: this.props.email || this.props.twitch || this.props.youtube || this.props.facebookUsername || '',
      emailValid: true,
      userType: this.props.userType || translate('createProfile.defaultAccountType'),
      connections: {
        peerplays: {
          header: translate('updateProfile.accountConnections.cryptoHeader'),
          connections: [
            {
              name: 'peerplays',
              username: peerplaysAccountName,
              headerIcon: PeerplaysIcon
            }
          ]
        },
        social: {//twitch, facebook, youtube
          header: translate('updateProfile.accountConnections.socialHeader'),
          connections: [
            {//twitch
              name: 'twitch',
              headerIcon: TwitchIcon,
              username: twitchUsername
            },
            {//facebook
              name: 'facebook',
              headerIcon: FacebookIcon,
              username: facebookUsername
            },
            {//youtube
              name: 'youtube',
              username: youtubeUsername,
              headerIcon: YoutubeIcon
            }
          ]
        },
        game: {//fortnite, pubg, league of legends
          header: translate('updateProfile.accountConnections.gameHeader'),
          headerLabel: translate('updateProfile.accountConnections.connectionSelect'),
          headerDescription: translate('updateProfile.accountConnections.connectionDescription'),
          connections: [
            {//pubg
              name: 'pubg',
              headerIcon: PubgIcon,
              bodyIcon: null,
              username: pubgUsername
            }
          ]
        }
      }
    };
  }

  show401Error(err) {
    if(err.status === 401) {
      this.props.setModalType(ModalTypes.ERROR);
      this.props.setModalData({
        headerText: translate('errors.loggedOut')
      });
      this.props.toggleModal();
      return;
    }
  }

  handleEmailChange = (email) => {
    const validation = ValidationUtil.seEmail(email).success;
    this.setState({email: email, emailValid: validation});
  }

  handleUserTypeChange = (userType) => {
    this.setState({userType: userType});
  }

  openLinkAccountModal = (authRoute) => {
    if(authRoute === 'peerplays') {
      this.props.setModalType(ModalTypes.PEERPLAYS_AUTH);
    } else {
      this.props.setModalType(ModalTypes.LINK_ACCOUNT);
    }

    if (authRoute === 'youtube'){
      this.props.setModalData('google');
    } else if(authRoute === 'peerplays') {
      this.props.setModalData('update');
    } else {
      this.props.setModalData(authRoute);
    }

    this.props.toggleModal();
  }

  openUnlinkAccountModal = (authRoute) => {
    this.props.setModalType(ModalTypes.UNLINK_ACCOUNT);
    this.props.setModalData(authRoute);
    this.props.toggleModal();
  }

  closeLinkAccountModal = () => {
    this.props.toggleModal();
  }

  submitHandler = () => {
    const {email, userType} = this.state;
    let account = {userType: userType, email: email};

    ProfileService.updateProfile(account).then((res) => {
      const {twitchUsername, youtubeUsername, facebookUsername, pubgUsername, peerplaysAccountName} = this.props;
      const path = this.props.location.pathname;
      const pathAry = path.split('/')[2];
      const originState = this.constructState(twitchUsername, youtubeUsername, facebookUsername, pubgUsername, peerplaysAccountName,pathAry);

      this.props.setAccount(res);
      this.props.setModalType(ModalTypes.SUBMIT);

      if (originState.email !== email) {
        this.props.setModalData({headerText: translate('updateProfile.userInfo.confirmEmail'), subText: translate('preferences.modal.redirectToDashboard'), redirect: '/dashboard'});
      } else {
        this.props.setModalData({headerText: translate('updateProfile.userInfo.updatedSuccessfully'), subText: translate('preferences.modal.redirectToDashboard'), redirect: '/dashboard'});
      }

      this.props.toggleModal();
    }).catch((err) => {
      console.log(JSON.stringify(err));

      this.show401Error(err);

      const entries = Object.entries(err.error);
      this.props.setModalType(ModalTypes.ERROR);

      if(entries && entries.length > 0) {
        this.props.setModalData({headerText: translate('updateProfile.userInfo.updateFailed'), subText: `${entries[0][0]}: ${entries[0][1]}`});
      }else {
        this.props.setModalData({headerText: translate('updateProfile.userInfo.updateFailed')});
      }

      this.props.toggleModal();
    });
  }

  render() {
    const {connections} = this.state;
    return (
      <div className='update-profile__wrapper'>
        <form className='update-profile-form' onSubmit={ this.handleSubmit }>
          <UserInfo
            hasUnlockedChallenges={ this.props.hasUnlockedChallengeCreate }
            handleEmailChange={ this.handleEmailChange }
            email={ this.state.email }
            handleUserTypeChange={ this.handleUserTypeChange }
            submit={ this.submitHandler }
          />
          <AccountConnections connections={ connections } openLinkAccountModal={ this.openLinkAccountModal } openUnlinkAccountModal={ this.openUnlinkAccountModal }
            closeLinkAccountModal={ this.closeLinkAccountModal } peerplaysAccountName = { this.props.peerplaysAccountName } />
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData,
    setAccount: AccountActions.setAccountAction,
    navigateToDashboard: NavigateActions.navigateToDashboard
  },
  dispatch
);

const mapStateToProps = (state) => {
  const account = CommonSelecter.getCurrentAccount(state);
  return {
    account,
    username: account.get('username'),
    email: account.get('email'),
    userType: account.get('userType'),
    twitchUsername: account.get('twitchUserName'),
    youtubeUsername: account.get('googleName'),
    facebookUsername: account.get('facebook'),
    pubgUsername: account.get('pubgUsername'),
    peerplaysAccountName: account.get('peerplaysAccountName'),
    twitch: account.get('twitch'),
    youtube: account.get('youtube'),
    hasUnlockedChallengeCreate: CommonSelecter.hasUserUnlockedChallengeCreate(state)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(UpdateProfile));
