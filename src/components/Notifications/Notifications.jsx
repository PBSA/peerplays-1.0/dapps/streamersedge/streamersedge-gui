import {Component} from 'react';
import {connect} from 'react-redux';
import {NotificationActions} from '../../actions';

export class Notifications extends Component {
  componentDidMount() {
    this.props.dispatch(NotificationActions.initialize());
  }

  render() {
    return null;
  }
}

export default connect(
  null,
  (dispatch) => ({dispatch})
)(Notifications);
