import React, {Component} from 'react';
import {avatarFrame, profileDefault} from '../../assets/images/avatar';

class Avatar extends Component {
  render() {
    const avatar = this.props.avatar ? this.props.avatar : profileDefault;
    const frame = this.props.isChallengeAvatar ? null : <img className='avatar__frame' src={ avatarFrame } alt='' />;
    const challengeFrameClass = this.props.isChallengeAvatar ? '-challenge' : '';

    return (
      <div className='avatar__wrapper'>
        {frame}
        <img className={ `avatar__img${challengeFrameClass}` } src={ avatar } alt={ '' }/>
      </div>
    );
  }
}

export default Avatar;
