/**
 * Callback Handler on the front end for redirects initiated from the backend.
 */

import React, {Component} from 'react';
import {NavigateActions, AccountActions, ModalActions} from '../../actions';
import {ProfileService, AuthService} from '../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {StorageUtil, GenUtil} from '../../utility';
import {ModalTypes} from '../../constants';
import queryString from 'query-string';

const translate = GenUtil.translate;

class Callback extends Component {
  componentDidMount() {
    this.handleCallback();
  }

  state = {
    error: ''
  };

  // Handle callback based on type passed
  handleCallback = async () => {
    const path = this.props.location;
    const pathAry = path.split('/');
    let lastAccessedPage = StorageUtil.get('se-page');
    let cb = pathAry[2] ? pathAry[2] : lastAccessedPage.split('/')[1];
    let createProfileStep = 1;

    if (!pathAry[2] && lastAccessedPage.split('/')[1] === 'profile') {
      createProfileStep = 2;
    }

    try {
      switch (cb) {
        case 'paypal-return':
          const {token} = queryString.parse(this.props.query);

          try{
            await AuthService.paypal(token);
            this.props.navigate(lastAccessedPage);
            this.props.setModalData({headerText: translate('addFunds.success')});
            this.props.setModalType(ModalTypes.OK);
            this.props.toggleModal();
          }catch(err) {
            this.props.navigate(lastAccessedPage);
            this.handleError(err);
          }

          break;

        case 'paypal-cancel':
          this.props.navigate(lastAccessedPage);
          this.handleError('Payment Cancelled');
          break;

        case 'confirm-email': {
          const account = await AuthService.confirmEmail(pathAry[3]);
          this.props.setAccount(account);
          this.props.setLoggedIn(true);
          this.props.navigateToCreateProfile('1');
          break;
        }

        case 'change-email': {
          const account = await ProfileService.changeEmail(pathAry[3]);
          this.props.setAccount(account);
          this.props.setLoggedIn(true);
          this.props.navigateToDashboard();
          break;
        }

        case 'reset-password': {
          this.props.navigateToPasswordReset(pathAry[3]);
          break;
        }

        case 'profile': {
          const profile = await ProfileService.getProfile();
          this.props.setAccount(profile);
          this.props.setLoggedIn(true);
          this.props.navigateToCreateProfile(createProfileStep);
          break;
        }

        case 'login': {
          break;
        }

        case 'update-profile': {
          const res = await ProfileService.getProfile();
          this.props.setAccount(res);
          this.props.setLoggedIn(true);
          this.props.navigateToUpdateProfile();
          break;
        }

        default: {
          const response = await ProfileService.getProfile();
          this.props.setAccount(response);
          this.props.setLoggedIn(true);
          this.props.navigateToDashboard();
          break;
        }
      }
    }catch (e) {
      console.log(e);
    }
  };

  render() {
    return (
      <>
        <div className='callback-page'>
          <div className='callback-page__content'>{this.state.error}</div>
        </div>
      </>
    );
  }

  handleError(err) {
    const errString = err.error ? err.error : err.message ? err.message : err;

    if(errString.includes('banned')) {
      this.props.setModalType(ModalTypes.BAN);
      this.props.toggleModal();
    } else {
      this.props.setModalData({headerText: translate('createChallenge.modal.error'), subText: errString});
      this.props.setModalType(ModalTypes.ERROR);
      this.props.toggleModal();
    }
  }
}

const mapStateToProps = (state) => ({
  location: state.getIn(['router', 'location', 'pathname']),
  query: state.getIn(['router', 'location', 'search'])
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    setLoggedIn: AccountActions.setIsLoggedInAction,
    navigateToDashboard: NavigateActions.navigateToDashboard,
    navigateToUpdateProfile: NavigateActions.navigateToUpdateProfile,
    navigateToPasswordReset: NavigateActions.navigateToPasswordReset,
    navigateToCreateProfile: NavigateActions.navigateToCreateProfile,
    navigate: NavigateActions.navigate,
    setAccount: AccountActions.setAccountAction,
    setModalData: ModalActions.setModalData,
    toggleModal: ModalActions.toggleModal,
    setModalType: ModalActions.setModalType
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Callback);

