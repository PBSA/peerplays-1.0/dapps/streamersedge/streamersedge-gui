import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {GenUtil} from '../../../utility';
import {DonateConstants, ModalTypes} from '../../../constants';
import {ModalActions} from '../../../actions';
import insufficient_funds from '../../../assets/images/donate/insufficient_funds.png';
import add_funds_btn from '../../../assets/images/donate/add_funds_btn.png';

const translate = GenUtil.translate;

class InsufficientFunds extends React.Component {
  addFunds = () => {
    this.props.setModalType(ModalTypes.ADD_FUNDS);
  }

  render() {
    return(
      <div className='insufficient'>
        <img className='insufficient__img' src={ insufficient_funds } alt='insufficient_funds'/>
        <span onClick={ () => this.props.updatePage(DonateConstants.DONATE) } className='insufficient__header'>{translate('donate.insufficientHeader')}</span>
        <span className='insufficient__subHeader'>{translate('donate.insufficientSubHeader')}</span>
        <img className='donate__submit'
          src={ add_funds_btn }
          alt='add funds'
          onClick={ this.addFunds }
        />
      </div>
    );
  }
}


const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    setModalType: ModalActions.setModalType
  },
  dispatch
);

export default connect(null, mapDispatchToProps)(InsufficientFunds);