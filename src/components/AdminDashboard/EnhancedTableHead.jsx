import React, {Component} from 'react';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Tooltip from '@material-ui/core/Tooltip';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import PropTypes from 'prop-types';

class EnhancedTableHead extends Component {
  createSortHandler = (property) => (event) => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const {order, orderBy,rows} = this.props;

    return (
      <TableHead className='table-th'>
        <TableRow>
          {rows.map(
            (row , index) => (
              <TableCell
                key={ row.id }
                align={ row.numeric ? 'right' : 'left' }
                padding={ row.disablePadding ? 'none' : 'default' }
                sortDirection={ orderBy === row.id ? order : false }
                className={ 'th table-col-'+index }
              >
                <Tooltip
                  title='Sort'
                  placement={ row.numeric ? 'bottom-end' : 'bottom-start' }
                  enterDelay={ 300 }
                >
                  <TableSortLabel
                    active={ orderBy === row.id }
                    direction={ order }
                    onClick={ this.createSortHandler(row.id) }
                  >
                    {row.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            ),
            this,
          )}
        </TableRow>
      </TableHead>
    );
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  rows: PropTypes.array.isRequired
};


export default EnhancedTableHead;
