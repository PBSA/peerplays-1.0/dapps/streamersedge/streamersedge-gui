import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import {fade} from '@material-ui/core/styles/colorManipulator';
import {withStyles} from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import ProfilesTable from './ProfilesTable';
import AdminService from '../../services/AdminService';
import moment from 'moment';
import {RouteConstants} from '../../constants';
import {connect} from 'react-redux';

const reportRows = [
  {id: 'username', numeric: false, disablePadding: false, label: 'Username'},
  {id: 'email', numeric: true, disablePadding: false, label: 'Email'},
  {id: 'bannedDate', numeric: true, disablePadding: false, label: 'Reported Date'},
  {id: 'reportedBy', numeric: true, disablePadding: false, label: 'Reported By'},
  {id: 'reason', numeric: true, disablePadding: false, label: 'Reason : Description'}
];

const bannedRows = [
  {id: 'username', numeric: false, disablePadding: false, label: 'Username'},
  {id: 'email', numeric: true, disablePadding: false, label: 'Email'},
  {id: 'reportedBy', numeric: true, disablePadding: false, label: 'Reported By'},
  {id: 'bannedDate', numeric: true, disablePadding: false, label: 'Date Banned'},
  {id: 'reason', numeric: true, disablePadding: false, label: 'Reason : Description'}
];


const styles = (theme) => ({
  root: {
    width: '100%'
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  title: {
    color:'white',
    fontSize:'3rem',
    [theme.breakpoints.down(1400)]: {
      display: 'block',
      fontSize: '2.5rem'
    }
  },
  tabLabel:{
    color: 'white'
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto'
    }
  },
  searchIcon: {
    width: theme.spacing.unit * 5,
    color: 'white',
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer'
  },
  inputRoot: {
    color: 'inherit',
    width: '100%'
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: '8rem',
    transition: theme.transitions.create('width'),
    fontSize:'2rem',
    width: '24rem',
    [theme.breakpoints.down(1400)]: {
      width:'16rem',
      paddingLeft: '6rem'
    }
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex'
    }
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },
  appBar:{
    backgroundColor:'#004FCB'
  },
  tabs:{
    color:'#FFFFFF !important',
    backgroundColor:'#FFFFFF !important'
  },
  tab:{
    color:'#FFFFFF !important'
  },
  tabText:{
    fontSize:'1.8rem'
  }
});

const TabContainer = (props) => {
  return (
    <Typography component='div'>
      {props.children}
    </Typography>
  );
};

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

class AdminDashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
      mobileMoreAnchorEl: null,
      value:0,
      reportedUsers:[],
      filteredReportedUsers: [],
      bannedUsers:[],
      filteredBannedUsers: [],
      searchText:'',
      reportedUsersLoaded:false,
      bannedUsersLoaded:false
    };
  }

  handleChange = (event, value) => {
    this.setState({value,searchText:''});
    this.setState({
      reportedUsers: [],
      filteredReportedUsers: [],
      bannedUsers: [],
      filteredBannedUsers: []
    });
    this.refresh();
  };

  componentDidMount() {
    if(this.props.userType !== 'admin'){
      this.props.navigate(RouteConstants.DASHBOARD);
      return;
    }

    this.refresh();
  };

  refresh = () => {
    this.getReports();
    this.getBannedUsers();
  };

  _getReports = (limit, offset) => {
    AdminService.getReports('', limit, offset).then((data) => {
      if(data.length <=0 ){
        this.setState({
          reportedUsersLoaded:true
        });
        this.filterReports(this.state.searchText);
        return;
      } else {
        let reportedUsers = this.state.reportedUsers;
        data.forEach((reportedUser) => {
          let date = reportedUser['reportedDate'];
          let reportedDate = moment(date).format('MMMM DD,YYYY');
          reportedUsers.push({
            id: reportedUser.id,
            username: reportedUser.troublemaker.username || '',
            email: reportedUser.troublemaker.email || '',
            reportedBy: reportedUser.reporter.username || '',
            reportedUserId: reportedUser.reportedUserId,
            reportedDate,
            userType: reportedUser.troublemaker.userType || '',
            avatar: reportedUser.troublemaker.avatar,
            reason: `${reportedUser.reason} : ${reportedUser.description}`
          });
        });
        this.setState({
          reportedUsers,
          filteredReportedUsers: this.state.reportedUsers
        }, () => {
          this._getReports( 100, offset+100);
        });
      }
    });
  };

  getReports = () => {
    this.setState({
      reportedUsers: [],
      filteredReportedUsers: [],
      reportedUsersLoaded:false
    }, () => {
      this._getReports(100,0);
    });
  };

  getBannedUsers = (searchText) => {
    AdminService.getBannedUser(searchText).then((data) => {
      let bannedUsers = [];
      data.forEach((bannedUser) => {
        let date = bannedUser['ban-histories.bannedAt'];
        let bannedDate = moment(date).format('MMMM DD,YYYY');
        bannedUsers.push({
          id: bannedUser.id,
          avatar: bannedUser.avatar,
          username: bannedUser.username || '',
          email: bannedUser.email || '',
          bannedDate,
          reportedUserId: bannedUser.id,
          reportedBy:bannedUser.reportInfo.reporter.username,
          reason: `${bannedUser.reportInfo.reason} : ${bannedUser.reportInfo.description}`
        });
      });
      this.setState({
        bannedUsers,
        filteredBannedUsers: bannedUsers,
        bannedUsersLoaded:true
      }, () => {
        this.filterBannedUsers(this.state.searchText);
      });
    });
  };

  filterReports = (searchText) => {
    let filteredReports = this.state.reportedUsers.filter((report) => {
      return !!(report.reportedDate.toLowerCase().includes(searchText) ||
        report.email.toLowerCase().includes(searchText) ||
        report.username.toLowerCase().includes(searchText) ||
        report.reportedBy.toLowerCase().includes(searchText) ||
        report.reason.toLowerCase().includes(searchText));
    });
    this.setState({
      filteredReportedUsers: filteredReports
    });
  };

  filterBannedUsers = (searchText) => {
    let filteredBannedUsers = this.state.bannedUsers.filter((user) => {
      return !!(user.username.toLowerCase().includes(searchText) ||
        user.email.toLowerCase().includes(searchText) ||
        user.reportedBy.toLowerCase().includes(searchText) ||
        user.bannedDate.toLowerCase().includes(searchText) ||
        user.reason.toLowerCase().includes(searchText));
    });
    this.setState({
      filteredBannedUsers: filteredBannedUsers
    });
  };


  getSearchValue = (e) => {
    if(this.state.searchText.length < 50 || e.target.value.length < 50){
      this.setState({
        searchText: e.target.value
      },()=>{
        this.state.value === 0 ?
          this.filterReports(this.state.searchText.toLowerCase())
          : this.filterBannedUsers(this.state.searchText.toLowerCase());
      });
    }
  };

  render() {
    const {classes} = this.props;
    const {value} = this.state;
    return (
      <div className='adminDashboard' >
        <AppBar position='static' className={ classes.appBar }>
          <Toolbar>
            <Typography className={ classes.title } variant='h6' color='inherit' noWrap>
              Admin
            </Typography>
            <div className={ classes.grow } />
            <div className={ classes.search }>
              <div className={ classes.searchIcon }>
                <SearchIcon />
              </div>
              <InputBase
                placeholder='Search…'
                value={ this.state.searchText }
                classes={ {
                  root: classes.inputRoot,
                  input: classes.inputInput
                } }
                onChange={ (e) => this.getSearchValue(e) }
              />
            </div>
          </Toolbar>
          <div className='tabs'>
            <Tabs value={ value } onChange={ this.handleChange } classes={ {indicator: classes.tabs,flexContainer:classes.tab} } >
              <Tab label='REPORTED PROFILES' classes= { classes.tabText } />
              <Tab label='BANNED USERS' classes= { classes.tabText } />
            </Tabs>
          </div>

        </AppBar>
        {value === 0 && <TabContainer>
          <ProfilesTable
            type={ 'Reports' }
            rows={ reportRows }
            dataLoaded={ this.state.reportedUsersLoaded }
            data={ this.state.filteredReportedUsers }
            refresh={ this.refresh }/>
        </TabContainer>}
        {value === 1 && <TabContainer>
          <ProfilesTable
            type={ 'Ban Users' }
            rows={ bannedRows }
            dataLoaded={ this.state.bannedUsersLoaded }
            data={ this.state.filteredBannedUsers }
            refresh={ this.refresh }/>
        </TabContainer>}
      </div>
    );
  }
}

AdminDashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
  return {
    userType: state.getIn(['profiles', 'currentAccount', 'userType'])
  };
};

export default connect(
  mapStateToProps,
  null
)(withStyles(styles)(AdminDashboard));
