import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Modal from '@material-ui/core/Modal';
import UserBanModal from './UserBanModal';
import {getSorting, stableSort} from '../../utility/GeneralUtils';
import EnhancedTableHead from './EnhancedTableHead';
import Avatar from '@material-ui/core/Avatar';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import {NavigateActions} from '../../actions';
import {bindActionCreators} from 'redux';

const styles = (theme) => ({
  root: {
    width: '100%',
    borderRadius: '0'
  },
  table: {
    minWidth: 1020
  },
  tableWrapper: {
    overflowX: 'auto'
  },
  selectIcon: {
    top:'.6rem',
    fontSize:'2.4rem',
    [theme.breakpoints.down(1400)]: {
      top: '.4rem'
    }
  },
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none'
  },
  menuItem: {
    fontSize: '2rem',
    color: '#5e5f61',
    paddingVertical: 5,
    height: 20,
    width: 10,
    margin: 0,
    [theme.breakpoints.down(1400)]: {
      fontSize: '1.8rem'
    }
  },
  avatarContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: 10,
    '& img': {
      width: 30,
      height: 30,
      borderRadius: 15
    }
  }
});

class ProfilesTable extends React.Component {
  state = {
    order: 'asc',
    orderBy: 'username',
    selected: [],
    data: [],
    page: 0,
    rowsPerPage: 15,
    modalContent: {},
    open: false
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({order, orderBy});
  };

  handleChangePage = (event, page) => {
    this.setState({page});
  };

  handleChangeRowsPerPage = (event) => {
    this.setState({rowsPerPage: event.target.value});
  };

  openModal = (modalContent) => {
    this.setState({
      modalContent,
      open: true
    });
  };

  handleClose = () => {
    this.setState({open: false});
  };

  render() {
    const {data, classes, refresh, rows, type,dataLoaded} = this.props;
    const {order, orderBy, selected, rowsPerPage, page} = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

    return (
      <Paper className={ classes.root }>
        <Modal
          aria-labelledby='simple-modal-title'
          aria-describedby='simple-modal-description'
          open={ this.state.open }
          onClose={ this.handleClose }
        >{type === 'Reports' ?
            <UserBanModal title={ 'Ban User' } modalContent={ this.state.modalContent } refresh={ refresh } handleClose={ this.handleClose }/> :
            <UserBanModal title={ 'Unban User' } modalContent={ this.state.modalContent } refresh={ refresh } handleClose={ this.handleClose }/>
          }
        </Modal>
        <div className={ classes.tableWrapper }>
          <Table className={ classes.table + ' table' } aria-labelledby='tableTitle'>
            <EnhancedTableHead
              numSelected={ selected.length }
              order={ order }
              orderBy={ orderBy }
              onRequestSort={ this.handleRequestSort }
              rowCount={ data.length }
              rows={ rows }
            />
            {dataLoaded ?
              <TableBody>
                {stableSort(data, getSorting(order, orderBy, type))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((n) => {
                    return (
                      <TableRow
                        hover
                        tabIndex={ -1 }
                        key={ n.id }
                        onClick={ () => {
                          this.openModal(n);
                        } }
                      >
                        {
                          type === 'Reports' ? <>
                              <TableCell align='left' className='td table-cell table-col-0'>
                                <div className={ classes.avatarContainer } onClick={ (e) => {
                                  e.stopPropagation();
                                  this.props.navigate(`/user-profile/${n.reportedUserId}`);
                                } }>
                                  <Avatar src={ n.avatar } className={ classes.avatar } sizes={ '20' }/>
                                  <span className='username-text'>{n.username}</span>
                                </div>
                              </TableCell>
                              <TableCell align='right' className='td table-cell table-col-1'>{n.email}</TableCell>
                              <TableCell align='right' className='td table-cell table-col-2'>{n.reportedDate}</TableCell>
                              <TableCell align='right' className='td table-cell table-col-3'>{n.reportedBy}</TableCell>
                              <TableCell align='right' className='td table-cell table-col-4'>{n.reason}</TableCell>
                            </> :
                            <>
                              <TableCell align='left' className='td table-cell table-col-0'>
                                <div className={ classes.avatarContainer } onClick={ (e) => {
                                  e.stopPropagation();
                                  this.props.navigate(`/user-profile/${n.reportedUserId}`);
                                } }>
                                  <Avatar src={ n.avatar } className={ classes.avatar } sizes={ '20' }/>
                                  <span className='username-text'>{n.username}</span>
                                </div>
                              </TableCell>
                              <TableCell align='right' className='td table-cell table-col-1'>{n.email}</TableCell>
                              <TableCell align='right' className='td table-cell table-col-2'>{n.reportedBy}</TableCell>
                              <TableCell align='right' className='td table-cell table-col-3'>{n.bannedDate}</TableCell>
                              <TableCell align='right' className='td table-cell table-col-4'>{n.reason}</TableCell>
                            </>
                        }
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={ {height: 49 * emptyRows} }>
                    <TableCell colSpan={ 6 }/>
                  </TableRow>
                )}
              </TableBody> : null
            }
          </Table>
        </div>
        <div className='table-pagination'>
          <TablePagination
            classes={ {
              menuItem: classes.menuItem,
              selectIcon:classes.selectIcon
            } }
            rowsPerPageOptions={ [15, 20, 25] }
            component='div'
            count={ data.length }
            rowsPerPage={ rowsPerPage }
            page={ page }
            backIconButtonProps={ {
              'aria-label': 'Previous Page'
            } }
            nextIconButtonProps={ {
              'aria-label': 'Next Page'
            } }
            onChangePage={ this.handleChangePage }
            onChangeRowsPerPage={ this.handleChangeRowsPerPage }
          />
        </div>
      </Paper>
    );
  }
}

ProfilesTable.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.array.isRequired,
  refresh: PropTypes.func.isRequired,
  rows: PropTypes.array.isRequired,
  dataLoaded: PropTypes.bool.isRequired,
  type: PropTypes.string.isRequired
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    navigate: NavigateActions.navigate
  },
  dispatch
);

export default connect(
  null,
  mapDispatchToProps
)(withStyles(styles)(withRouter(ProfilesTable)));
