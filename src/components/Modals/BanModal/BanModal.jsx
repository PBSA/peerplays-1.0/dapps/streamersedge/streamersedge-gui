import React, {Component} from 'react';
import {BannedIcon, CrossIcon} from '../../../assets/images/ban/index';
import {ModalActions, NavigateActions} from '../../../actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {GenUtil} from '../../../utility';
import {BanConstants} from '../../../constants';

const translate = GenUtil.translate;

class BanModal extends Component {
  handleTermsClicked = () => {
    this.props.navigateToTermsAndConditions();
    this.props.toggleModal();
  }

  render() {
    return (
      <div className='ban-card__wrapper'>
        <div className='ban-card'>
          <div className='ban-card__cross' onClick={ this.props.toggleModal }>
            <img className='ban-card__cross-img' src={ CrossIcon } alt=''/>
          </div>
          <div className='ban-card__icon'>
            <img className='ban-card__icon-img' src={ BannedIcon } alt=''/>
          </div>
          <div className='ban-card-text'>
            <p className='ban-card-text__primary'>{translate('ban.primary')}</p>
            <p className='ban-card-text__secondary'>{translate('ban.secondary')}<span onClick={ this.handleTermsClicked }
              className='ban-card-text__underlined'>{translate('ban.secondaryUnderlined')}</span>
            </p>
            <p className='ban-card-text__secondary'>
              {translate('ban.orSend')}<a href={ `mailto:${BanConstants.SUPPORT_EMAIL}` } className='ban-card-text__white'>{translate('ban.email')}</a>
            </p>
          </div>
        </div>
      </div>
    );
  }
}


const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    navigateToTermsAndConditions: NavigateActions.navigateToTermsAndConditions
  },
  dispatch
);

export default connect(
  null,
  mapDispatchToProps
)(BanModal);

