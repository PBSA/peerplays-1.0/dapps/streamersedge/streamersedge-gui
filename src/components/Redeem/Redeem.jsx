import React, {Component} from 'react';
import {ModalActions, NavigateActions} from '../../actions';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {bindActionCreators} from 'redux';
import {GenUtil} from '../../utility';
import paypalLogo from '../../assets/images/PaypalLogo.svg';
import {AuthService, ProfileService} from '../../services';
import RedeemButton from '../../assets/images/Redeem_Btn.svg';
import CancelButton from '../../assets/images/Cancel.svg';
import TransactionService from '../../services/TransactionService';
import {ModalTypes} from '../../constants';

const translate =   GenUtil.translate;
class Redeem extends Component{
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      redeemAmount: 0
    };
  }

  componentDidMount() {
    ProfileService.getProfile().then((res) => {
      this.setState({
        email: res.paypalEmail
      });
    }).catch((e) => {
      console.log(e);

      if(e.status === 401) {
        this.props.setModalType(ModalTypes.ERROR);
        this.props.setModalData({
          headerText: translate('errors.loggedOut')
        });
        this.props.toggleModal();
      }
    });
  }

  changeHandler = (e) => {
    const newValue = e.target.value
      .replace(/[^\d.]/g, '')             // numbers and decimals only
      .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1');
    this.setState({
      [e.target.name]: newValue
    });
  };

  editPaypalAccount = () => {
    AuthService.changePaypalAccount('/redeem').then((res) => {
      window.location.assign(res);
    }).catch((e) => {
      console.log(e);

      if(e.includes('401')) {
        this.props.setModalData({
          headerText: translate('errors.loggedOut')
        });
      }else {
        this.props.setModalData({
          subText: translate('redeem.error')
        });
      }

      this.props.setModalType(ModalTypes.ERROR);
      this.props.toggleModal();
    });
  };

  openPeerplaysAuthentication = () => {
    const modalData = {
      peerplaysAccountName: this.props.peerplaysAccountName,
      peerplaysPassword: this.props.peerplaysPassword,
      ppyAmount: Number(this.state.redeemAmount)
    };

    this.props.setModalType(ModalTypes.PEERPLAYS_AUTH);
    this.props.setModalData(modalData);
    this.props.toggleModal();
  }

  redeemAmount = () => {
    if(!this.state.email && this.props.peerplaysAccountName.startsWith('se-')) {
      this.handleError(translate('redeem.errorLinkPaypal'));
      return;
    }

    if(this.props.peerplaysBalance < Number(this.state.redeemAmount) + this.props.transactionFee) {
      this.handleError(translate('redeem.errorInsufficientFunds'));
      return;
    }

    if(this.props.redeemAmount === 0) {
      this.handleError(translate('redeem.errorAmountZero'));
      return;
    }

    if(this.props.peerplaysAccountName && this.props.peerplaysAccountName.startsWith('se-')){
      TransactionService.redeem(Number(this.state.redeemAmount)).then(() => {
        this.setState({
          redeemAmount: 0
        });
        this.handleSuccess();
      }).catch((e) => {
        this.handleError(e);
      });
    } else {
      this.setState({
        redeemAmount: 0
      });
      this.openPeerplaysAuthentication();
    }
  };

  handleSuccess = () => {
    this.props.setModalType(ModalTypes.OK);
    this.props.setModalData({
      subText: translate('redeem.success')
    });
    this.props.toggleModal();
  };

  handleError = (e) => {
    let sub;

    if(e.status === 401) {
      this.props.setModalType(ModalTypes.ERROR);
      this.props.setModalData({
        headerText: translate('errors.loggedOut')
      });
      this.props.toggleModal();
      return;
    }

    if(e.data) {
      sub = '';
    }else {
      sub = e;
    }

    this.props.setModalType(ModalTypes.ERROR);
    this.props.setModalData({
      headerText: translate('redeem.error'),
      subText: sub
    });
    this.props.toggleModal();
  };

  cancelHandler = () => {
    this.props.navigateToDashboard();
  };

  render() {
    return(
      <div className='redeem'>
        <div className='redeem__header'>
          {translate('redeem.header')}
        </div>
        <div className='redeem-info'>
          <div className='redeem-info-balance'>
            <div className='redeem-info-balance-currency'>
              <span className='redeem-info-balance-currency__title'>{translate('redeem.currency')}</span>
            </div>
            <div className='redeem-info-balance-info'>
              <span className='redeem_button'>{translate('redeem.USD')}</span>
              <div className='redeem-info-balance-info-flex'>
                <div className='redeem-info-balance-info__title'>{translate('redeem.balance')+': '+ translate('redeem.USD')}</div>
                <div className='redeem-info-balance-info__price'>{(Math.round(this.props.peerplaysBalance*100)/100).toFixed(2)}</div>
              </div>
            </div>
          </div>
          <div className='redeem-paypal'>
            <div className='redeem-paypal-logo'>
              <img src={ paypalLogo } className='redeem-paypal-logo__img' alt='paypal logo' />
            </div>
            <div className='redeem-paypal-info'>
              <p className='redeem-paypal-info__text'>
                {translate('redeem.email')}<strong>{this.state.email}</strong>
              </p>
              <span className='redeem-paypal-info__edit' onClick={ this.editPaypalAccount }> Edit Account</span>
            </div>
          </div>
          <div className='redeem-amount'>
            <div className='redeem-amount-container'>
              <div className='redeem-amount-title-container'>
                <span className='redeem-amount-title-container__title'> Amount</span>
              </div>
              <div className='redeem-amount-info'>
                <input className='redeem_button' value={ this.state.redeemAmount } name='redeemAmount' onChange={ this.changeHandler }/>
                <span className='redeem-amount-info-currency_type'> USD </span>
              </div>
            </div>
            <div className='redeem-amount-flex-col'>
              <div className='redeem-amount-flex'>
                <span className='tile'> Withdrawal Fee</span>
                <span className='amount'> {(Math.round(5 * Number(this.state.redeemAmount))/100).toFixed(2)} </span>
              </div>
              <div className='redeem-amount-flex'>
                <span className='tile'> Transaction Fee</span>
                <span className='amount'> {this.state.redeemAmount > 0 ? (Math.round(this.props.transactionFee*100)/100).toFixed(2) : Math.round(0).toFixed(2)} </span>
              </div>
              <div className='redeem-amount-flex'>
                <span className='tile'> Remaining Balance</span>
                <span className='amount'> {this.state.redeemAmount > 0 ?
                  (Math.round((this.props.peerplaysBalance - Number(this.state.redeemAmount) - this.props.transactionFee)*100)/100).toFixed(2)
                  : (Math.round(this.props.peerplaysBalance * 100)/100).toFixed(2)} </span>
              </div>
              <div className='redeem-amount-flex-receive'>
                <div className='redeem-amount-flex-receive-text-container'>
                  <span className='tile'> What You Will Receive</span>
                  <span className='amount'> {(Math.round(95 * Number(this.state.redeemAmount))/100).toFixed(2)} </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='redeem-footer'>
          <div><img className='redeem-footer__cancel' src={ CancelButton } alt='' onClick={ this.cancelHandler }/></div>
          <div>
            <img className='redeem-footer__redeem' src={ RedeemButton } alt='' onClick={ this.redeemAmount }/>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalData: ModalActions.setModalData,
    setModalType: ModalActions.setModalType,
    navigateToDashboard: NavigateActions.navigateToDashboard
  },
  dispatch
);

const mapStateToProps = (state) => ({
  modalData: state.getIn(['modal', 'data']),
  peerplaysBalance: state.getIn(['peerplays', 'balance']),
  peerplaysAccountName: state.getIn(['profiles','currentAccount','peerplaysAccountName']),
  peerplaysPassword: state.getIn(['peerplays','password']),
  transactionFee: state.getIn(['peerplays','transferFee'])
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Redeem));
