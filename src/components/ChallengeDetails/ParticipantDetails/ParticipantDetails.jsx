import React, {Component} from 'react';
import {profileDefault} from '../../../assets/images/avatar';
import susdImg from '../../../assets/images/dashboard/susd_2x.png';
import {translate} from '../../../utility/GeneralUtils';
/* showing streaming icons and stats has been descoped for now */
// import {twitchIcon, youtubeIcon, facebookIcon, viewsIcon, followersIcon} from '../../../assets/images/challengeDetails';
class ParticipantDetails extends Component {

  renderAvatar = (avatar) => {
    let avatarImg = avatar;

    if(!avatar) {
      avatarImg = profileDefault;
    }

    return (
      <img className='participants-information__container-avatar' src={ avatarImg } alt={ '' } />);
  }

  formatDate(startDate) {
    //format date to output in the following format: October 29, 2019, hh:mm
    const {timeFormat} = this.props;
    let date, options, formattedDate;
    date = new Date(startDate);
    options = {year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric'};

    if(timeFormat === '24h') {
      formattedDate = date.toLocaleDateString('en-ZA', options);
    } else {
      formattedDate = date.toLocaleDateString('en-US', options);
    }

    return formattedDate;
  }

  render() {
    const {joinedUsers} = this.props;

    return (
      <div className='participants'>
        {joinedUsers && joinedUsers.length > 0 ?
        <>
          <div className='participants-header'>
            Donation History
          </div>
          <div className='participants-header__row'>
            <div className='participants-header__col1'>
              User
            </div>
            <div className='participants-header__col2'>
              Date
            </div>
            <div className='participants-header__col3'>
              Amount
            </div>
          </div>
          {joinedUsers.map((account,index) => (
            <div className='participants-container__row' key={ index }>
              <div className='participants-information'>
                <div className='participants-information__container' onClick={ () => this.props.onAvatarClick(account.user.id) }>
                  {this.renderAvatar(account.user.avatar)}
                  <span className='participants-information__container-username'>{account.user.username}</span>
                </div>
                {/* This info has been descoped for now */}
                {/* <div className='participants-information__socials'>
                <div className='participants-information__socials-icon'>
                  <img src={ facebookIcon } alt=''/>
                  <div className='participants-information__socials-icon__stats'>
                    <span className='participants-information__socials-icon__stats-icon'><img src={ viewsIcon } alt=''/>test1</span>
                    <span className='participants-information__socials-icon__stats-icon--red'><img src={ followersIcon } alt=''/>test2</span>
                  </div>
                </div>
                <div className='participants-information__socials-icon'>
                  <img src={ youtubeIcon } alt=''/>
                  <div className='participants-information__socials-icon__stats'>
                    <span className='participants-information__socials-icon__stats-icon'><img src={ viewsIcon } alt=''/>test1</span>
                    <span className='participants-information__socials-icon__stats-icon--red'><img src={ followersIcon } alt=''/>test2</span>
                  </div>
                </div>
                <div className='participants-information__socials-icon'>
                  <img src={ twitchIcon } alt=''/>
                  <div className='participants-information__socials-icon__stats'>
                    <span className='participants-information__socials-icon__stats-icon'><img src={ viewsIcon } alt=''/>test1</span>
                    <span className='participants-information__socials-icon__stats-icon--red'><img src={ followersIcon } alt=''/>test2</span>
                  </div>
                </div>
              </div> */}
              </div>
              <div className='participants-date'>
                {this.formatDate(account.createdAt)}
              </div>
              <div className='participants-amount'>
                <img className='participants-usd' src={ susdImg } alt=''></img>
                <div>
                  {`${translate('challengeDetails.sUSD')} ${account.ppyAmount}`}
                </div>
              </div>
            </div>
          ))}
        </>:
          <div className='participants--no-donations'>{translate('challengeDetails.noDonations')}</div> }
      </div>
    );
  }
}

export default ParticipantDetails;