import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withStyles} from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import {AppActions, ModalActions, NavigateActions} from '../../actions/';
import {ModalTypes} from '../../constants';
import {GenUtil} from '../../utility';
import LoginForm from '../Login/LoginForm';
import PeerplaysLogin from '../PeerplaysLogin';
import ForgotPassword from '../ForgotPassword';
import ReportUser from '../ReportUser';
import Donate from '../Donate';
import Register from '../Register';
import BanModal from '../Modals/BanModal';
import LinkAccountModal from '../Modals/LinkAccountModal';
import UnlinkAccountModal from '../Modals/UnlinkAccountModal';
import SubmitModal from '../Modals/SubmitModal';
import ChallengeConfirm from '../CreateChallenge/ChallengeConfirm';
import ChallengeSuccess from '../CreateChallenge/ChallengeSuccess';
import CategoryModal from '../Modals/CategoryModal';
import PeerplaysAuth from '../Modals/PeerplaysAuth';
import AddFunds from '../AddFunds';

import styles from './MUI.css';

const translate = GenUtil.translate;
class RootModal extends Component {

  handleClose = () => {
    this.props.setErrorText('');
    this.props.toggleModal();
    this.props.setModalData();
    this.setState({open: false});
  };

  openPasswordRecovery = () => {
    this.props.setErrorText('');
    this.props.setModalType(ModalTypes.FORGOT);
  };

  openPrivacyPolicy = () => {
    this.props.setErrorText('');
    this.props.navigateToPrivacyPolicy();
  }

  openTerms = () => {
    this.props.setErrorText('');
    this.props.navigateToTermsAndConditions();
    this.handleClose();
  }

  toggleModalAndRegister = () => {
    this.props.setErrorText('');
    this.props.setModalType(ModalTypes.SIGN_UP);
  };

  render() {
    const {classes} = this.props;

    // Default modal content - a user should NEVER see this in production.
    let modalContent = null;
    let modalClass = classes.root;

    // Some modals have a pill element that must render outside and above the modal itself.
    let pillContent = null;

    // Specify your modals here
    switch (this.props.modalType) {
      case ModalTypes.ADD_FUNDS: {
        modalContent = <AddFunds />;
        modalClass = 'modal-addFunds';
        break;
      }

      case ModalTypes.ADD_FUNDS_DIRECT: {
        modalContent = <AddFunds directFunds />;
        modalClass = 'modal-addFunds';
        break;
      }

      case ModalTypes.LOGIN: {
        modalContent = <LoginForm recoverPassword={ this.openPasswordRecovery } goRegister={ this.toggleModalAndRegister } handleLogin={ this.props.login }
          errorText={ this.props.errorText } openPrivacyPolicy={ this.openPrivacyPolicy } openTerms={ this.openTerms }/>;
        modalClass = 'modal-login';
        break;
      }

      case ModalTypes.SIGN_UP: {
        modalContent = <Register />;
        modalClass = 'modal-register';
        break;
      }

      case ModalTypes.FORGOT: {
        modalContent = <ForgotPassword goRegister={ this.toggleModalAndRegister } prev={ this.props.previousModal } setModalType={ this.props.setModalType }/>;
        modalClass = 'modal-forgot';
        break;
      }

      case ModalTypes.REPORT_USER: {
        modalContent = <ReportUser />;
        modalClass = 'modal-report';
        break;
      }

      case ModalTypes.DONATE: {
        modalContent = <Donate />;
        modalClass = 'modal-donate';
        break;
      }

      case ModalTypes.BAN: {
        modalContent = <BanModal />;
        modalClass = 'modal-ban';
        break;
      }

      case ModalTypes.LINK_ACCOUNT: {
        modalContent = <LinkAccountModal />;
        modalClass = 'modal-link';
        break;
      }

      case ModalTypes.UNLINK_ACCOUNT: {
        modalContent = <UnlinkAccountModal />;
        modalClass = 'modal-link';
        break;
      }

      case ModalTypes.PEERPLAYS_LOGIN: {
        modalContent = <PeerplaysLogin goRegister={ this.toggleModalAndRegister }/>;
        modalClass = 'modal-peerplays';
        break;
      }

      case ModalTypes.ERROR: {
        modalContent = <SubmitModal/>;
        modalClass = 'modal-error';
        break;
      }

      case ModalTypes.OK:

      // eslint-disable-next-line no-fallthrough
      case ModalTypes.SUBMIT: {
        modalContent = <SubmitModal />;
        modalClass = 'modal-success';
        break;
      }

      case ModalTypes.CHALLENGE_CONFIRM: {
        modalContent = <ChallengeConfirm />;
        modalClass = 'modal-challengeConfirm';
        break;
      }

      case ModalTypes.SHARE_CHALLENGE:

      // eslint-disable-next-line no-fallthrough
      case ModalTypes.CREATE_CHALLENGE_SUCCESS: {
        modalContent = <ChallengeSuccess />;
        modalClass = 'modal-challengeSuccess';
        break;
      }

      case ModalTypes.SELECT_CATEGORY: {
        modalContent = <CategoryModal />;
        modalClass = 'modal-category';
        break;
      }

      case ModalTypes.PEERPLAYS_AUTH: {
        modalContent = <PeerplaysAuth />;
        modalClass = 'modal-category';
        break;
      }

      default: {
        break;
      }
    }

    switch (modalClass) {
      case 'modal-addFunds' :
        pillContent = (
          <div className='modal-pill'>
            <span className='add-funds__pill'>{translate('addFunds.title')}</span>
          </div>
        );
        break;
        // no default
    }

    return (
      <>
        {/* <Dialog open={ this.state.open } onClose={ this.handleClose } aria-labelledby='form-dialog-title' classes={ {paper: classes.dialog-paper__root} }> */}
        <Dialog open={ this.props.isModalOpen } onClose={ this.handleClose } aria-labelledby='form-dialog-title' maxWidth={ 'md' } PaperProps={ {classes: {root: modalClass}} }>
          {pillContent}
          <DialogContent>
            {modalContent}
          </DialogContent>
        </Dialog>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  isModalOpen: state.getIn(['modal', 'isOpen']),
  modalType: state.getIn(['modal', 'type']),
  previousModal: state.getIn(['modal', 'previous']),
  errorText: state.getIn(['profiles', 'loginErrorText'])
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData,
    login: AppActions.login,
    setErrorText: AppActions.setLoginError,
    navigateToPrivacyPolicy: NavigateActions.navigateToPrivacyPolicy,
    navigateToTermsAndConditions: NavigateActions.navigateToTermsAndConditions
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(RootModal));
