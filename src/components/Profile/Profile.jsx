import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {ModalActions} from '../../actions';
import {GenUtil} from '../../utility';
import ChallengeGrid from '../Dashboard/ChallengeGrid';
import {ChallengeService, UserService} from '../../services';
import {ModalTypes} from '../../constants';
import Avatar from '../Avatar';
import CircularProgress from '@material-ui/core/CircularProgress';
import ReportButton from '../../assets/images/userprofile/report_icon.svg';
import FacebookButton from '../../assets/images/userprofile/facebook_btn.png';
import TwitchButton from '../../assets/images/userprofile/twitch_btn.png';
import Live from '../../assets/images/userprofile/Live.svg';
import YoutubeButton from '../../assets/images/userprofile/youtube_btn.png';
import PropTypes from 'prop-types';
import ActiveChallenge from '../Profile/ActiveChallenge';

const translate = GenUtil.translate;

class Profile extends Component {

  state = {
    challengesWon: [],
    challenges: [],
    user: {},
    loading: {
      user: true,
      challenges: true
    }
  };

  componentDidMount = () => {
    const {match: {params}} = this.props;
    this.fetchUser(params.user);
  }

  componentDidUpdate(prevProps) {
    if(prevProps.match.params.user !== this.props.match.params.user) {
      const {match: {params}} = this.props;
      this.fetchUser(params.user);
    }
  }

  openReportModal = () => {
    this.props.setModalType('REPORT_USER');
    this.props.toggleModal();
  }

  openSocial = (platform) => {
    if (platform === 'Facebook') {
      window.location.href = this.state.user.facebookLink;
    } else if (platform === 'Twitch') {
      window.location.href = `http://www.twitch.tv/${this.state.user.twitchUserName}`;
    } else if (platform === 'Youtube') {
      window.location.href = this.state.user.youtube;
    }
  }

  fetchUser = async (id) => {
    try {
      const user = await UserService.getUserById(id);
      this.props.setUser(user);
      this.setState((prevState) => ({
        user,
        loading: {
          ...prevState.loading,
          user: false
        }
      }));
      this.fetchChallenges(id, user);
    } catch(error) {
      console.log(error);

      if(error.message && error.message.includes('401')) {
        this.props.setModalType(ModalTypes.ERROR);
        this.props.setModalData({
          headerText: translate('errors.loggedOut')
        });
        this.props.toggleModal();
        return;
      }
    }
  }

  fetchChallenges = async (id, user) => {
    ChallengeService.getWonChallengesByUser(id).then((challengesWon)=> {
      this.setState({
        challengesWon
      });
    }).catch((err) => {
      console.log(err);

      if(err.status === 401) {
        this.props.toggleModal();
        this.props.setModalType(ModalTypes.ERROR);
        this.props.setModalData({
          headerText: translate('errors.loggedOut')
        });
        this.props.toggleModal();
      }
    });
    ChallengeService.getChallenges().then((res)=> {
      const challenges = res.filter((challenge) => {
        return challenge.status === 'live' && challenge.user.username && challenge.user.username === user.username;
      }).slice(0,1);
      this.setState({
        challenges
      });
    }).catch((err) => {
      console.log(err);
    });
  }

  render() {
    let profileContent;

    if (this.state.loading.user) {
      profileContent = <CircularProgress/>;
    } else {
      let user = this.state.user;
      let profileButtons = (user.id !== this.props.currentUser.get('id')) ? (
        <div className='profile__report-button' onClick={ this.openReportModal }>
          <img
            className='profile__report-image'
            src={ ReportButton }
            alt={ 'Report User' }
          />
          <div className='profile__report'>
            Report User
          </div>
        </div>
      ) : null;

      profileContent = (
      <>
        <div className='profile__avatar'>
          <Avatar avatar={ user.avatar }/>
        </div>
        <div className='profile__user-data'>
          <h2 className='profile__user-name'>{ user.username }</h2>
        </div>
        <div className='profile__buttons' >
          {profileButtons}
        </div>
        <div className='profile__user-socials'>
          {user.facebook ? <img
            className='profile__button-facebook'
            src={ FacebookButton }
            alt={ 'Facebook' }
            onClick={ () => this.openSocial('Facebook') }
          /> : null }

          {user.twitchUserName ?
            <div className='profile__button-container' onClick={ () => this.openSocial('Twitch') }>
              <img
                className='profile__button-twitch'
                src={ TwitchButton }
                alt={ 'Twitch' }
              />
              {this.state.challenges && this.state.challenges.length > 0 ? <img className='profile__button-live'
                src={ Live }
                alt={ 'Live' }
              />: null}
            </div>
            : null
          }

          {user.youtube ? <img
            className='profile__button-youtube'
            src={ YoutubeButton }
            alt={ 'Youtube' }
            onClick={ () => this.openSocial('Youtube') }
          /> : null }
        </div>
    </>
      );
    }

    return (
      <>
      <div className='user-profile'>
        <div className='profile__info'>
          {profileContent}
        </div>
        <ActiveChallenge challenges={ this.state.challenges } user={ this.state.user }/>
        <div className='dashboard__challenges'>
          <div className='dashboard__header'>
            <span className='dashboard__header-txt'>
              {translate('userProfile.challenges')}
              <span className='challenges'> {translate('userProfile.won')}</span>
            </span>
            <div className='dashboard__header-bar'>
              <div className='dashboard__header-bar--green'/>
            </div>
            <ChallengeGrid challenges={ this.state.challengesWon }/>
          </div>
        </div>
      </div>
      </>
    );
  }
}

Profile.propTypes = {
  challenges: PropTypes.array
};

const mapStateToProps = (state) => {
  return {
    currentUser: state.getIn(['profiles', 'currentAccount'])
  };
};


const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData,
    toggleModal: ModalActions.toggleModal,
    setUser: ModalActions.selectModalUser
  },
  dispatch
);


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
