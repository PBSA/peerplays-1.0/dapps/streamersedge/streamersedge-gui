import React, {Component} from 'react';
import ChallengeService from '../../services/ChallengeService';
import YellowBtn from '../../assets/images/listChallenges/Grupo 85.png';
import classNames from 'classnames';
import Paginate from 'react-paginate';
import SearchIcon from '@material-ui/icons/Search';
import Dropdown from '../Dropdown';
import {ChallengeUtil, GenUtil} from '../../utility';
import {ModalActions, NavigateActions} from '../../actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {ModalTypes} from '../../constants';
import {profileDefault} from '../../assets/images/avatar';
import {Tooltip} from '@material-ui/core';


const translate = GenUtil.translate;
class ListChallenges extends Component{
  state = {
    challenges : [],
    sort: ' Challenges by Start Date',
    orderText: 'timeToStart',
    searchText: '',
    offset: 0,
    perPage: 9
  };
  componentDidMount() {
    ChallengeService.getChallenges({sort: this.state.orderText}).then((res) => {
      this.setState({
        challenges: res
      });
    }).catch((e) => {
      console.error(e);
      this.props.setModalType(ModalTypes.SUBMIT);
      this.props.setModalData({headerText: translate('listChallenges.failed'), type: 'error'});
      this.props.toggleModal();
    });
  }

  handleChange = (value) => {
    let order = value.trim().toLowerCase();
    let temp;

    if (order === 'challenges by total donation') {
      this.setState({
        sort: value,
        orderText: ''
      });
      ChallengeService.getChallenges({sort: '', searchText: this.state.searchText.trim().substring(0,100)}).then((res) => {
        let challenges = res;
        challenges.sort(function (challenge1,challenge2) {
          return ChallengeUtil.getTotalDonations(challenge2) - ChallengeUtil.getTotalDonations(challenge1);
        });
        this.setState({
          challenges
        });
      });
    } else if(order === 'active challenges') {
      this.setState({
        sort: value,
        orderText: ''
      });
      ChallengeService.getChallenges({sort: '', searchText: this.state.searchText.trim().substring(0,100)}).then((res) => {
        let challenges = res.filter((card) => {
          return card.status === 'live';
        });

        this.setState({
          challenges
        });
      });
    } else {
      if (order === 'challenges by name') {
        temp = 'name';
      } else if (order === 'challenges by game') {
        temp = 'game';
      } else if (order === 'challenges by start date') {
        temp = 'timeToStart';
      } else if (order === 'challenges by creation date') {
        temp = 'createdAt';
      }

      this.setState({
        sort: value
      });
      ChallengeService.getChallenges({sort: temp, searchText: this.state.searchText.trim().substring(0, 100)}).then((res) => {
        this.setState({
          challenges: res,
          sort: value,
          orderText: temp
        });
      }).catch(() => {
        this.props.setModalType(ModalTypes.SUBMIT);
        this.props.setModalData({headerText: translate('listChallenges.failed'), type: 'error'});
        this.props.toggleModal();
      });
    }
  };

  formatDate(startDate) {
    //format date to output in the following format: October 29, 2019, hh:mm
    let date, options, formattedDate;
    date = new Date(startDate);
    options = {year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric'};

    if(this.props.timeFormat === '24h') {
      formattedDate = date.toLocaleDateString('en-ZA', options);
    } else {
      formattedDate = date.toLocaleDateString('en-US', options);
    }

    return formattedDate;
  }

  handleSearch = (event) => {
    this.setState({
      searchText: event.target.value.trim()
    });
  };

  handleSearchClick = () => {
    if(this.state.searchText.length >= 3 || this.state.searchText.length <= 0) {
      if(this.state.searchText.length <= 0){
        this.setState({
          searchText: ''
        });
      }

      let order = this.state.sort.trim().toLowerCase();

      if (order === 'challenges by total donation') {
        ChallengeService.getChallenges({sort: '', searchText: this.state.searchText.trim().substring(0,100)}).then((res) => {
          let challenges = res;
          challenges.sort(function (challenge1,challenge2) {
            return ChallengeUtil.getTotalDonations(challenge2) - ChallengeUtil.getTotalDonations(challenge1);
          });
          this.setState({
            challenges
          });
        });
      } else if(order === 'active challenges') {
        ChallengeService.getChallenges({sort: '', searchText: this.state.searchText.trim().substring(0,100)}).then((res) => {
          let challenges = res.filter((card) => {
            return card.status === 'live';
          });

          this.setState({
            challenges
          });
        });
      } else {
        ChallengeService.getChallenges({sort: this.state.orderText,searchText: this.state.searchText.trim().substring(0,100)}).then((res) => {
          this.setState({
            challenges: res
          });
        }).catch(() => {
          this.props.setModalType(ModalTypes.SUBMIT);
          this.props.setModalData({headerText: translate('listChallenges.failed'), type: 'error'});
          this.props.toggleModal();
        });
      }
    }
  };

  handlePageChange = ({selected}) => {
    this.setState({
      offset: selected * 9,
      perPage: 9
    });
  };

  handleKeyPress = (e) => {
    if(e.key === 'Enter') {
      this.handleSearchClick();
    }
  };

  handleCardClick = (id) => {
    this.props.navigate(`/challenge/${id}`);
  };

  handleAvatarClick = (id) => {
    this.props.navigate(`/user-profile/${id}`);
  };

  render() {
    return (
      <div>
        <div className='challenge-header'>
          <div className='challenge-header-item challenge-header-item-sort'>
            <Dropdown value={ this.state.sort } dropdownList={ translate('listChallenges.sortTypes').split(',') } handleChange={ this.handleChange }/>
          </div>
          <div className='challenge-header-item'>
            <span className='challenge-header-item__text'>{translate('listChallenges.headerText')}</span>
          </div>
          <div className='challenge-header-item'>
            <div className='challenge-search'>
              <Tooltip title={ translate('tooltip.searchChallenge') } placement='bottom'>
                <div>
                  <input
                    className='challenge-search__input'
                    value={ this.state.searchText }
                    onChange={ this.handleSearch }
                    maxLength={ 100 }
                    onKeyPress={ this.handleKeyPress }
                  />
                  <div className={ classNames('challenge-search__icon', {'challenge-search__icon-disabled': this.state.searchText.length <= 2 && this.state.searchText.length >= 1}) }>
                    <SearchIcon className='left-menu-search__icon-color' fontSize='large' onClick={ this.handleSearchClick }/>
                  </div>
                </div>
              </Tooltip>
            </div>
          </div>
        </div>

        {this.state.challenges.length > 0 ? <div className='list-challenge-card__wrapper'>
          {this.state.challenges.map((challenge) => {
            return (<div key={ challenge.id } className = { classNames('list-challenge-card',
              {'list-challenge-card__first': challenge.game === 'fortnite'},
              {'list-challenge-card__second': challenge.game === 'leagueOfLegends'},
              {'list-challenge-card__third': challenge.game === 'pubg'},
            ) }
            onClick={ () => this.handleCardClick(challenge.id) }>
              {
                challenge.timeToStart &&
                <div className='list-challenge-card__startDate'>
                  <span className='list-challenge-card__startDate-text'>{translate('listChallenges.starts')} {this.formatDate(challenge.timeToStart)}</span>
                </div>
              }
              <div className='list-challenge-card__ppyBounty'>
                <img className='list-challenge-card__ppyBounty-icon' src={ YellowBtn } alt=''/>
                <span className='list-challenge-card__ppyBounty-text'>{translate('listChallenges.usd')} {challenge ? ChallengeUtil.getTotalDonations(challenge).toFixed(2): null}</span>
              </div>
              <div className='list-challenge-card__profile'>
                <div className='list-challenge-card__profile-info'>
                  <div className='list-challenge-card__profile-info-img'>
                    <img onClick={ (e)=>{
                      e.stopPropagation();
                      this.handleAvatarClick(challenge.userId);
                    } } className='list-challenge-card__profile-info-img-icon' src={ (challenge.user && challenge.user.avatar) ||  profileDefault } alt=''/>
                  </div>
                  <div className='list-challenge-card__profile-info-text'>
                    <div className='list-challenge-card__profile-info-name'>
                      <span className='list-challenge-card__profile-info-name-text'>{challenge.name}</span> | <span>{challenge.game}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            );
          }).slice(this.state.offset, this.state.offset + this.state.perPage)}
        </div> : <div className='list-challenge-card__wrapper challenge-card__wrapper-error'>
          {translate('listChallenges.noResults')}
        </div> }

        <Paginate
          previousLabel={ '←' }
          nextLabel={ '→' }
          breakLabel={ '...' }
          breakClassName={ 'break-me' }
          pageCount={ this.state.challenges.length / 9 }
          pageRangeDisplayed={ 9 }
          marginPagesDisplayed={ 1 }
          forcePage={ 0 }
          containerClassName={ 'pagination' }
          subContainerClassName={ 'pages pagination' }
          activeClassName={ 'active' }
          onPageChange={ this.handlePageChange }
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  timeFormat: state.getIn(['profiles', 'currentAccount','timeFormat'])
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData,
    navigate: NavigateActions.navigate
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListChallenges);
