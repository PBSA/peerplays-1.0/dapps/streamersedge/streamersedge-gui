import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import classNames from 'classnames';
import {withStyles} from '@material-ui/core/styles';
import {Button} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';

import {ModalTypes} from '../../../constants';
import {ModalActions} from '../../../actions';
import {GenUtil} from '../../../utility';

import styles from './Mui.css';
import ConditionDropdown from './ConditionDropdown';
import ConditionStandard from './ConditionStandard';
import OperatorDropdown from './OperatorDropdown';

const trans = GenUtil.translate;

class ConditionForm extends Component {
  handleClickAdd = (index) => {
    if (this.props.conditions.length < 3) {
      this.props.onChangeConditions('add', index);
    } else {
      this.props.setModalType(ModalTypes.ERROR);
      this.props.setModalData({subText: trans('createChallenge.errors.condition.maximum')});
      this.props.toggleModal();
    }
  }

  handleClickDelete = (index) => {
    this.props.onChangeConditions('delete', index);
  }

  handleChange = (index, name, value) => {
    this.props.onChangeConditions('update', index, Object.assign(this.props.conditions[index], {[name]: value}));
  }

  render() {
    const {classes, conditions} = this.props;

    return (
      <>
        <div className='condition-form__wrapper'>
          <div className='condition-form__conditions'>
            <p>{trans('createChallenge.condition.label')}</p>

            {conditions.map((condition, index) => (
              <div key={ index } className='condition-form__condition'>
                <ConditionDropdown
                  index={ index }
                  join={ condition.join }
                  value={ condition.param }
                  gameStats={ this.props.gameStats }
                  onChangeParam={ (newParam) => this.handleChange(index, 'param', newParam) }
                />
                <OperatorDropdown value={ condition.operator } onChange={ (newOpr) => this.handleChange(index, 'operator', newOpr) } />
                <ConditionStandard
                  type='param'
                  size='small'
                  value={ condition.value }
                  onChange={ (newValue) => this.handleChange(index, 'value', newValue) }
                />
                <Button
                  className={ classNames({'condition-form__invisible': index !== conditions.length - 1}) }
                  classes={ {root: classes.addButton} }
                  onClick={ () => this.handleClickAdd(index) }
                >
                  {`+ ${trans('createChallenge.condition.add')}`}
                </Button>
                <Button
                  className={ classNames({'condition-form__invisible': conditions.length === 1}) }
                  classes={ {root: classes.deleteButton} }
                  onClick={ () => this.handleClickDelete(index) }
                >
                  <DeleteIcon fontSize='large' classes={ {root: classes.deleteIcon} } />
                </Button>
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData
  },
  dispatch
);

export default connect(
  null,
  mapDispatchToProps
)(withStyles(styles)(ConditionForm));
