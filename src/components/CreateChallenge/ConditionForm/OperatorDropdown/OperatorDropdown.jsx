import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import {Select, MenuItem} from '@material-ui/core';
import DropdownIcon from '@material-ui/icons/UnfoldMore';
import styles from './Mui.css';

const OPERATORS = [{'Greater Than':'>'}, {'Less Than':'<'}, {'Greater Than or Equal To':'>='}, {'Less Than or Equal To':'<='}, {'Equal To':'='}];

class OperatorDropdown extends Component {
  handleChangeValue = (e) => {
    const value = e.target.value;
    this.props.onChange(value);
  }

  renderAnchor = () => {
    return (
      <DropdownIcon classes={ {root: this.props.classes.dropdownAnchor} } color='primary' />
    );
  }

  render() {
    const {classes} = this.props;

    return (
      <>
        <div className='operator-dropdown'>
          <Select
            className='operator-dropdown--select'
            classes={ {
              root: classes.dropdown
            } }
            value={ this.props.value }
            IconComponent={ this.renderAnchor }
            MenuProps={ {
              classes: {paper: classes.dropdownStyle}, //overrides dropdown styles
              getContentAnchorEl: null
            } }
            onChange={ this.handleChangeValue }
          >
            {OPERATORS.map((operator) => (
              <MenuItem key={ Object.values(operator)[0] } className='dropdown__menuItem' value={ Object.values(operator)[0] }>{Object.keys(operator)[0]}</MenuItem>
            ))}
          </Select>
        </div>
      </>
    );
  }
}

export default withStyles(styles)(OperatorDropdown);