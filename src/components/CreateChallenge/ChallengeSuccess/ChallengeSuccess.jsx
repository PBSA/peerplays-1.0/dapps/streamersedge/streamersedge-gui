import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  WhatsappShareButton,
  WhatsappIcon,
  TelegramShareButton,
  TelegramIcon
} from 'react-share';

import {GenUtil} from '../../../utility';
import {ModalActions, NavigateActions} from '../../../actions';
import Config from '../../../utility/Config';
import {Copylink} from '../../../assets/images/challenge';
import {CrossIcon} from '../../../assets/images/ban';

const trans = GenUtil.translate;

class ChallengeSuccess extends Component {
  state = {
    shareUrl: this.props.modalData ? `${Config.baseRoute}/challenge/${this.props.modalData.challengeId}` : 0
  };

  componentDidMount() {
    //only start re-direct timer if we are on challenge create success, or donation success
    setTimeout(() => {
      if(this.props.isModalOpen && this.props.modalType === 'CREATE_CHALLENGE_SUCCESS') {
        this.redirectToDashboard();
      }
    }, 10000);
  }

  redirectToDashboard = () => {
    this.props.toggleModal();
    this.props.navigateToDashboard();
  }

  componentWillUnmount() {
    this.props.navigateToDashboard();
    clearInterval(this.timeId);
  }

  handleCopy = () => {
    navigator.clipboard.writeText(this.state.shareUrl);
  }

  handleClickHere = () => {
    this.redirectToDashboard();
  }

  renderHeader = () => {
    const {modalData} = this.props;

    if(modalData) {
      return (
      <>
        <p className='challenge-success__header'>{modalData.header}</p>
        <p className='challenge-success__subHeader'>{modalData.subText}</p>
      </>
      );
    }

    return;
  }

  render() {
    return (
      <>
        <div className='challenge-success'>
          <div className='challenge-success__cross' onClick={ this.props.toggleModal }>
            <img className='challenge-success__cross-img' src={ CrossIcon } alt=''/>
          </div>
          {this.renderHeader()}
          <div className='challenge-success__share'>
            <FacebookShareButton url={ this.state.shareUrl }>
              <FacebookIcon />
            </FacebookShareButton>
            <TwitterShareButton url={ this.state.shareUrl }>
              <TwitterIcon />
            </TwitterShareButton>
            <WhatsappShareButton url={ this.state.shareUrl }>
              <WhatsappIcon />
            </WhatsappShareButton>
            <TelegramShareButton url={ this.state.shareUrl }>
              <TelegramIcon />
            </TelegramShareButton>
          </div>
          <div className='challenge-success__link'>
            <p className='challenge-success__link-label'>{trans('createChallenge.success.link.label')}</p>
            <div className='challenge-success__link-box'>
              {this.state.shareUrl}
              <img className='challenge-success__link-copy' src={ Copylink } onClick={ this.handleCopy } alt='' />
            </div>
          </div>
          {this.props.modalType === 'CREATE_CHALLENGE_SUCCESS' ?
            <div className='challenge-success__redirect'>
              {trans('createChallenge.success.redirect.dashboard')}
              <span className='challenge-success__redirect-link' onClick={ this.handleClickHere }>
                {trans('createChallenge.success.redirect.here')}
              </span>
            </div> : null}
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  modalData: state.getIn(['modal', 'data']),
  isModalOpen: state.getIn(['modal', 'isOpen']),
  modalType: state.getIn(['modal', 'type'])
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    navigateToDashboard: NavigateActions.navigateToDashboard
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChallengeSuccess);
