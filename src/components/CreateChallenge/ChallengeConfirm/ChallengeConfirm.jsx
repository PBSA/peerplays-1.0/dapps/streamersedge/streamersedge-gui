import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {format} from 'date-fns';

import {ModalTypes} from '../../../constants';
import {ModalActions} from '../../../actions';
import {GenUtil} from '../../../utility';
import {ChallengeService} from '../../../services';

import {CancelButton, ContinueButton} from '../../../assets/images/challenge';

const trans = GenUtil.translate;

const OPERATORS = {'>':'is greater than','<':'is less than','>=':'is greater than or equal to','<=':'is less than or equal to','=':'is equal to'};


class ChallengeConfirm extends Component {
  state = {
    errors: []
  };

  openModal(challengeId, modalType) {
    this.props.toggleModal();

    if(challengeId.headerText) {
      this.props.setModalData(challengeId);
    }else {
      this.props.setModalData({headerText: trans('createChallenge.success.header'), subText: trans('createChallenge.success.subHeader'), challengeId: challengeId});
    }

    this.props.setModalType(modalType);
    this.props.toggleModal();
  }

  handleCancel = () => {
    this.props.toggleModal();
  }

  handleContinue = () => {
    const {challenge} = this.props;

    const data = {
      name: challenge.name,
      timeToStart: challenge.startDate.toISOString(),
      game: challenge.game,
      ppyAmount: challenge.ppyAmount,
      conditions: challenge.conditions
    };

    data.conditions[data.conditions.length - 1].join = 'END';
    ChallengeService
      .createChallenge(data)
      .then((result) => {
        this.openModal(result.id, ModalTypes.CREATE_CHALLENGE_SUCCESS);
      })
      .catch((err) => {

        if(err.status === 401) {
          this.props.toggleModal();
          this.props.setModalType(ModalTypes.ERROR);
          this.props.setModalData({
            headerText: trans('errors.loggedOut')
          });
          this.props.toggleModal();
        }else if(err.error.timeToStart){
          this.props.toggleModal();
          this.props.setModalType(ModalTypes.ERROR);
          this.props.setModalData({
            headerText: trans('createChallenge.modal.error'),
            subText: trans('createChallenge.errors.timeToStart')});
          this.props.toggleModal();
        } else if (err.status === 403) {
          this.props.toggleModal();
          this.props.setModalType(ModalTypes.BAN);
          this.props.toggleModal();
        } else {
          this.openModal({
            headerText: trans('createChallenge.modal.failedHeader'),
            subText: Object.values(err.error)
          }, ModalTypes.ERROR);
        }
      });
  }

  render() {
    const {challenge} = this.props;
    let timeFormat = this.props.timeFormat === '24h' ? 'HH:mm' : 'h:mm aa';
    let dateFormat = 'MMM d, yyyy ' + timeFormat;

    return challenge ? (
      <>
        <div className='challenge-confirm'>
          <p className='challenge-confirm__title'>
            {trans('createChallenge.confirm.header')}
          </p>
          <div className='challenge-confirm__radial'>
            <div className='challenge-confirm__radial-radial1' />
            <div className='challenge-confirm__radial-radial2' />
          </div>

          <div className='challenge-confirm__block'>
            <div className='challenge-confirm__label'>{challenge.name} |</div>
            <div className='challenge-confirm__value'>{challenge.game}</div>
          </div>
          <div className='challenge-confirm__block challenge-confirm__block--center'>
            <div className='challenge-confirm__label'>{trans('createChallenge.confirm.streamer')} </div>
            {this.props.avatar && (
              <img className='challenge-confirm__avatar' src={ this.props.avatar } alt={ this.props.username } />
            )}
            <div className='challenge-confirm__value'>{this.props.username}</div>
          </div>
          <div className='challenge-confirm__block'>
            <div className='challenge-confirm__label'>{trans('createChallenge.confirm.startTime')} </div>
            <div className='challenge-confirm__value'>{format(challenge.startDate, dateFormat)}</div>
          </div>
          <div className='challenge-confirm__block'>
            <div className='challenge-confirm__label'>{trans('createChallenge.confirm.rules')}</div>
            <div className='challenge-confirm__rules'>
              {challenge.conditions.map((condition, index) => {
                return (
                  <div key={ index }>
                    <div className='challenge-confirm__params'>
                      <span className='challenge-confirm__params-param'>
                        {condition.param} {OPERATORS[condition.operator]} {condition.param === 'winTime' ? condition.value + 's' : condition.value}
                      </span>
                    </div>
                    {index !== challenge.conditions.length - 1 && (
                      <div className='challenge-confirm__join'>
                        <span>{condition.join}</span>
                      </div>
                    )}
                  </div>
                );
              })}
            </div>
          </div>
          {this.state.errors.length > 0 && (
            <div className='create-challenge__error'>
              {this.state.errors.map((error, index) => <p key={ index }>{error}</p>)}
            </div>
          )}
          <div className='challenge-confirm__buttons'>
            <img className='challenge-confirm__buttons-button' src={ CancelButton } onClick={ this.handleCancel } alt='' />
            <img className='challenge-confirm__buttons-button' src={ ContinueButton } onClick={ this.handleContinue } alt='' />
          </div>
        </div>
      </>
    ) : null;
  }
}

const mapStateToProps = (state) => ({
  challenge: state.getIn(['modal', 'data', 'challenge']),
  username: state.getIn(['profiles', 'currentAccount', 'username']),
  avatar: state.getIn(['profiles', 'currentAccount', 'avatar']),
  timeFormat: state.getIn(['profiles', 'currentAccount', 'timeFormat'])
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChallengeConfirm);
