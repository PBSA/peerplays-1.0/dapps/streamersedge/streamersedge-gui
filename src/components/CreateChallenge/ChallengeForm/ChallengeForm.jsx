import React, {Component} from 'react';
import {FormControl} from '@material-ui/core';
import CustomInput from '../../CustomInput';
import GameAvatar from '../../GameAvatar';
import {GenUtil, ValidationUtil} from '../../../utility';

import {PUBG} from '../../../assets/images/challenge';
import {InvalidIcon} from '../../../assets/images/signup';

const trans = GenUtil.translate;
const GAMES = [
  {name: 'PUBG', value: 'pubg', src: PUBG}
];

class ChallengeForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchList: GAMES,
      isNameClicked: false
    };
  }

  handleChangeName = (newValue) => {
    this.setState({
      isNameClicked: true
    });
    this.props.onChange('name', newValue);
  }

  render() {
    const {searchList} = this.state;

    return (
      <>
        <div className='challenge-info'>
          <FormControl fullWidth>
            <p className='challenge-info__formlabel'>{ trans('createChallenge.name.label') }</p>
            <div className='challenge-info__input'>
              <CustomInput
                name='name'
                value={ this.props.challengeName }
                muiInputClass='inputRegister'
                hasActiveGlow={ true }
                maxLength={ 60 }
                placeholder={ trans('createChallenge.name.placeholder') }
                handleChange ={ this.handleChangeName }
                iconRightActive={ InvalidIcon }
                isValid={ () => {
                  if (this.state.isNameClicked) {
                    return ValidationUtil.challengeName(this.props.challengeName).success;
                  } else {
                    return true;
                  }
                } }
                handleRightIconClick={ () => {
                  return  ValidationUtil.challengeName(this.props.challengeName).errors;
                } }
                fullWidth
              />
            </div>
          </FormControl>
          <div className='challenge-info__game'>
            {!!searchList.length && searchList.map((game) => {
              const isSelected = game.value === 'pubg';

              return (
                <GameAvatar
                  key={ game.value }
                  name={ game.name }
                  value={ game.value }
                  src={ game.src }
                  selected={ isSelected }
                  onClick={ (value) => this.props.onChange('game', value) }
                />
              );
            })}
          </div>
        </div>
      </>
    );
  }
}

export default ChallengeForm;
