import React, {Component} from 'react';
import {connect} from 'react-redux';
import {format} from 'date-fns';
import DatePicker from 'react-datepicker';
import {bindActionCreators} from 'redux';
import {GenUtil} from '../../../utility';
import DateInput from './DateInput';
import moment from 'moment';
import {ModalTypes} from '../../../constants';
import {ModalActions} from '../../../actions';

const trans = GenUtil.translate;

class DateForm extends Component {
  state = {
    showStartDate: false,
    today: new Date()
  };

  toggleStartDate = () => {
    this.setState({
      showStartDate: !this.state.showStartDate
    });
  }

  handleChangeStartDate = (date) => {
    let timeComponent = moment(date);
    let currentTime = moment(new Date());

    if(timeComponent<currentTime){
      this.props.toggleModal();
      this.props.setModalType(ModalTypes.ERROR);
      this.props.setModalData({
        headerText: trans('createChallenge.modal.error'),
        subText: trans('createChallenge.errors.timeToStart')});
    } else {
      this.toggleStartDate();
      this.props.onChange('startDate', date);
    }
  }

  render() {
    let timeFormat = this.props.timeFormat === '24h' ? 'HH:mm' : 'h:mm aa';
    let dateFormat = 'MMM d, yyyy ' + timeFormat;

    return (
      <>
        <div className='date-form'>
          <p className='date-form__subheader'>{trans('createChallenge.date.subHeader')}</p>
          <div className='date-form__wrapper'>
            <div className='date-form__group'>
              <label className='date-form__label'>
                {trans('createChallenge.date.startDate')}
              </label>
              <DateInput
                value={ format(this.props.startDate, dateFormat) }
                onFocus={ this.toggleStartDate }
              />
            </div>
            {this.state.showStartDate && (
              <DatePicker
                inline
                showTimeSelect
                selected={ this.props.startDate }
                minDate={ this.state.today }
                timeFormat={ timeFormat }
                timeIntervals={ 10 }
                timeCaption='Time'
                dateFormat={ dateFormat }
                onChange={ this.handleChangeStartDate }
              />
            )}
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  timeFormat: state.getIn(['profiles', 'currentAccount', 'timeFormat'])
});


const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DateForm);
