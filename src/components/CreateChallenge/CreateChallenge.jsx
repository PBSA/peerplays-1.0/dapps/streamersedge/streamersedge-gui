import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {addHours} from 'date-fns';

import ChallengeFooter from './ChallengeFooter';
import ChallengeForm from './ChallengeForm';
import DateForm from './DateForm';
import ConditionForm from './ConditionForm';

import {ModalTypes} from '../../constants';
import {ModalActions, NavigateActions} from '../../actions';
import {GenUtil, ValidationUtil} from '../../utility';
import {GameService} from '../../services';
import CommonSelecter from '../../selectors/CommonSelector';

const trans = GenUtil.translate;

const DEFAULT_CONDITION = {
  param: '',
  join: 'AND',
  operator: '>',
  value: 5
};

class CreateChallenge extends Component {
  state = {
    currentStep: 1,
    isUpdatedConditions: false,
    gameStats: {},
    // Challenge Info
    name: '',
    game: 'pubg',
    conditions: [],
    ppyAmount: 100000,
    startDate: addHours(new Date(), 1),
    // Errors
    errors: []
  };

  componentDidMount() {
    if (!this.props.hasUnlockedChallengeCreate) {
      this.props.setModalType(ModalTypes.ERROR);
      this.props.setModalData({
        headerText: trans('createChallenge.errors.deny'),
        subText: trans('preferences.modal.redirectToDashboard'),
        redirect: '/dashboard'
      });
      this.props.toggleModal();
      return;
    }

    GameService.getGameStats().then((res) => {
      const entries = Object.entries(res);

      if (entries.length > 0) {
        Object.assign(DEFAULT_CONDITION, {param: entries[0][1]});
        this.setState({
          gameStats: res,
          conditions: [Object.assign({}, DEFAULT_CONDITION)]
        });
      }
    }).catch((err) => {
      this.setState({
        conditions: [Object.assign({}, DEFAULT_CONDITION)]
      });

      if(err.status === 401) {
        this.props.setModalType(ModalTypes.ERROR);
        this.props.setModalData({
          headerText: trans('errors.loggedOut')
        });
        this.props.toggleModal();
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    // Remove errors
    if ((prevState.name !== this.state.name)
      || (prevState.game !== this.state.game)
      || (prevState.currentStep !== this.state.currentStep)
    ) {
      this.setState({
        errors: []
      });
    }
  }

  handleBackClick = () => {
    this.setState((state) => ({
      currentStep: state.currentStep - 1
    }));
  }

  handleNextClick = () => {
    // Handle validation for each step
    if (this.state.currentStep === 1) {
      const valid = ValidationUtil.challengeNameAndGame(this.state.name, this.state.game);

      if (!valid.success) {
        this.setState({
          errors: valid.errors.filter((error) => !error.success).map((error) => error.errorString)
        });
        return;
      }
    }

    this.setState((state) => ({
      currentStep: state.currentStep + 1
    }));
  }

  handleCompleteClick = () => {
    this.props.setModalData({
      challenge: {
        name: this.state.name,
        game: this.state.game,
        startDate: this.state.startDate,
        ppyAmount: this.state.ppyAmount,
        conditions: this.state.conditions
      }
    });
    this.props.setModalType(ModalTypes.CHALLENGE_CONFIRM);
    this.props.toggleModal();
  }

  handleChangeConditions = (action, index = 0, newCondition = DEFAULT_CONDITION) => {
    switch (action) {
      case 'add':
        this.setState((state) => {
          return {
            isUpdatedConditions: true,
            errors: [],
            conditions: [
              ...state.conditions,
              Object.assign({}, newCondition)
            ]
          };
        });
        break;
      case 'delete':
        let conditions = this.state.conditions;

        conditions.splice(index, 1);
        this.setState({
          conditions,
          isUpdatedConditions: true,
          errors: []
        });
        break;
      case 'update':
        let newConditions = this.state.conditions;

        newConditions[index] = newCondition;
        this.setState({
          isUpdatedConditions: true,
          errors: [],
          conditions: newConditions
        });
        break;
      default:
        break;
    }
  }

  handleChange = (name, value) => {
    this.setState({
      [name]: value
    });
  }

  /*
   * Render component by the current step
   * step 1: ChallengeForm
   * step 2: DateForm
   * step 3: ConditionForm
   * otherwise: ChallengeForm
  */
  renderForm = () => {
    switch (this.state.currentStep) {
      case 1:
        return (
          <ChallengeForm
            challengeName={ this.state.name }
            challengeGame={ this.state.game }
            onChange={ this.handleChange }
          />
        );
      case 2:
        return (
          <DateForm
            startDate={ this.state.startDate }
            onChange={ this.handleChange }
          />
        );
      case 3:
        return (
          <ConditionForm
            conditions={ this.state.conditions }
            ppyAmount={ this.state.ppyAmount }
            gameStats={ this.state.gameStats }
            errors={ this.state.errors }
            onChange={ this.handleChange }
            onChangeConditions={ this.handleChangeConditions }
          />
        );
      // no default
    }
  }

  render() {
    const {errors} = this.state;

    return (
      <>
        <div className='create-challenge__content'>
          <div className='create-challenge__wrapper'>
            <div className='create-challenge__title'>{ trans('createChallenge.header') }</div>
            { this.renderForm() }
            {!!errors.length && (
              <div className='create-challenge__error'>
                {errors.map((error, index) => <p key={ index }>{error}</p>)}
              </div>
            )}
          </div>
        </div>
        <div className='create-challenge__footer'>
          <ChallengeFooter
            currentStep={ this.state.currentStep }
            lastStep={ 3 }
            onBack={ this.handleBackClick }
            onNext={ this.handleNextClick }
            onComplete={ this.handleCompleteClick }
          />
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  twitchUsername: state.getIn(['profiles', 'currentAccount', 'twitchUserName']),
  pubgUsername: state.getIn(['profiles', 'currentAccount', 'pubgUsername']),
  isModalOpen: state.getIn(['modal', 'isOpen']),
  hasUnlockedChallengeCreate: CommonSelecter.hasUserUnlockedChallengeCreate(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleModal: ModalActions.toggleModal,
    setModalType: ModalActions.setModalType,
    setModalData: ModalActions.setModalData,
    navigateToDashboard: NavigateActions.navigateToDashboard
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateChallenge);
