import React, {Component} from 'react';
import {ChallengeUtil} from '../../../utility';
import ChallengeCard from '../ChallengeCard';
import Paginate from 'react-paginate';
import {profileDefault} from '../../../assets/images/avatar';
import {translate} from '../../../utility/GeneralUtils';
import {NavigateActions} from '../../../actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class ChallengeGrid extends Component {

  state = {
    challenges: [],
    currentPage: 0,
    offset: 0,
    perPage: 9
  };

  changePage = (data) => {
    const selectedPage = data.selected;
    const offset = selectedPage * this.state.perPage;
    this.setState({currentPage: selectedPage, offset: offset});
  }

  handleAvatarClick = (event,id) => {
    const {isLoggedIn}=this.props;

    if(isLoggedIn) {
      this.props.navigate(`/user-profile/${id}`);
      event.stopPropagation();
    }
  };

  render() {
    let challenges = this.props.challenges;
    let challengeCount = 0;

    if (challenges) {

      const filteredChallenges = challenges.filter((challenge) => {
        return challenge.joinedUsers.length > 0 ? true : false;
      }).map((challenge) => {
        const donationSum = ChallengeUtil.getTotalDonations(challenge);
        return {...challenge, reward: donationSum};
      }).sort((a, b) => (a.reward < b.reward) ? 1 : -1);

      return filteredChallenges.length > 0 ? (
        <>
          <div className='challenge-card__container'>
            {filteredChallenges.map((challenge) => {
              challengeCount++;
              let avatar = profileDefault;

              if (challenge.user) {
                avatar = challenge.user.avatar;
              }

              return (
                <div className='grid__container' key={ challengeCount }>
                  <div className='grid__donate'>{translate('dashboard.donate')}</div>
                  <ChallengeCard
                    onCardClick={ (event)=>{
                      this.handleAvatarClick(event,challenge.userId);
                    } }
                    key={ challenge.id }
                    id={ challenge.id }
                    avatar={ avatar }
                    name={ challenge.name }
                    users={ challenge.joinedUsers }
                    reward={ challenge.reward } game={ challenge.game }
                    startTime = { challenge.timeToStart }
                    status = { challenge.status }/>
                </div>);
            }).slice(this.state.offset, this.state.offset + this.state.perPage)}
          </div>
                  <Paginate
                    previousLabel={ '←' }
                    nextLabel={ '→' }
                    breakLabel={ '...' }
                    breakClassName={ 'break-me' }
                    pageCount={ challengeCount / 9 }
                    forcePage={ this.state.currentPage }
                    marginPagesDisplay0ed={ 1 }
                    pageRangeDisplayed={ this.state.perPage }
                    onPageChange={ this.changePage }
                    containerClassName={ 'pagination' }
                    subContainerClassName={ 'pages pagination' }
                    activeClassName={ 'active' }
                  />
                </>
      ) : <span className='grid__empty'>{translate('userProfile.noChallenges')}</span>;
    }

  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.getIn(['profiles', 'isLoggedIn'])
  };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    navigate: NavigateActions.navigate
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChallengeGrid);
