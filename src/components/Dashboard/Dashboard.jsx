import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {NavigateActions} from '../../actions';
import {GenUtil} from '../../utility';
import {RouteConstants} from '../../constants';
import {withStyles} from '@material-ui/core/styles';
import {ChallengeService} from '../../services';
import ChallengeGrid from './ChallengeGrid';
import ChallengeCarousel from './ChallengeCarousel';
import LiveChallenges from './LiveChallenges';
import ViewMore from '../../assets/images/dashboard/viewMore.png';
import styles from './MUI.css';

const translate = GenUtil.translate;

class Dashboard extends Component {

  state = {
    challenges: []
  };

  viewMoreClick = (e, type) => {
    e.preventDefault();

    switch (type) {
      case 'streams':
        this.props.navigate(RouteConstants.STREAMS);
        break;
      case 'challenges':
        this.props.navigate(RouteConstants.CHALLENGES);
        break;
      // no default
    }
  };

  componentDidMount = () => {
    this.fetchChallenges();
  };

  fetchChallenges = () => {
    ChallengeService.getChallenges().then((challengeData) => {
      this.setState({
        challenges: challengeData
      });
    }).catch((e) => {
      console.error(e);
    });
  };

  viewMore = (type) => {
    return(
      <div className='dashboard__view-more' id={ type } >
        <img
          className='dashboard__view-more-img'
          src={ ViewMore }
          alt={ type }
          onClick={ (e) => this.viewMoreClick(e, type) }
        />
      </div>
    );
  };

  render() {

    return (
      <>
        <div className='dashboard'>
          <ChallengeCarousel challenges={ this.state.challenges }/>
          <LiveChallenges challenges={ this.state.challenges }/>

          <div className='dashboard__challenges'>
            <div className='dashboard__header'>
              <span className='dashboard__header-txt'>
                {translate('dashboard.recommended')}
                <span className='challenges'> {translate('dashboard.challenges')}</span>
              </span>
              {this.viewMore('challenges')}
              <div className='dashboard__header-bar'>
                <div className='dashboard__header-bar--green'/>
              </div>
              <ChallengeGrid challenges={ this.state.challenges }/>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    navigate: NavigateActions.navigate
  },
  dispatch
);

export default connect(
  null,
  mapDispatchToProps
)(withStyles(styles)(Dashboard));
