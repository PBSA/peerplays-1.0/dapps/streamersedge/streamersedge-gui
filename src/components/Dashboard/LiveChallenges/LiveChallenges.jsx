import React, {Component} from 'react';
import {GenUtil, ChallengeUtil} from '../../../utility';
import ChallengeCard from '../ChallengeCard';
import Slider from 'react-slick';
import {profileDefault} from '../../../assets/images/avatar';
import {bindActionCreators} from 'redux';
import {NavigateActions} from '../../../actions';
import {connect} from 'react-redux';


const translate = GenUtil.translate;

class LiveChallenges extends Component {

  state = {
    challenges: []
  };

   handleAvatarClick = (event,id) => {
     const {isLoggedIn}=this.props;

     if(isLoggedIn) {
       event.stopPropagation();
       this.props.navigate(`/user-profile/${id}`);
     }
   };

   render() {
     const settings = {
       centerMode: true,
       infinite: true,
       centerPadding: 0,
       slidesToShow: 3,
       speed: 500,
       variableWidth: false
     };

     let {challenges} = this.props;
     let rowkey = 0;

     challenges = challenges.filter((challenge) => {
       return challenge.status === 'live';
     });

     settings.infinite = challenges.length > 3;

     return (
      <>
      <div className='dashboard__categories'>
        <div className='dashboard__header'>
          <span className='dashboard__header-txt'>
            {translate('dashboard.live')}
            <span className='live-challenges'> {translate('dashboard.challenges')}</span>
          </span>
          <div className='dashboard__header-bar'>
            <div className='dashboard__header-bar--orange'/>
          </div>
          <div>
            <div className='live-challenges__slider'>
              <Slider { ...settings }>
                {challenges.map((challenge) => {

                  const donationSum = ChallengeUtil.getTotalDonations(challenge);
                  let avatar = profileDefault;

                  if (challenge.user) {
                    avatar = challenge.user.avatar;
                  }

                  return (
                    <div key={ rowkey++ }>
                      <div className='live-challenges__donate'>{translate('dashboard.donate')}</div>
                      <ChallengeCard
                        onCardClick={ (event)=>{
                          this.handleAvatarClick(event,challenge.userId);
                        } }
                        key={ challenge.id }
                        id={ challenge.id }
                        name={ challenge.name }
                        users={ challenge.joinedUsers }
                        reward={ donationSum }
                        game={ challenge.game }
                        avatar = { avatar }
                        startTime = { challenge.timeToStart }
                        status = { challenge.status }
                      />
                    </div>
                  );
                })}
              </Slider>
            </div>
          </div>
        </div>
      </div>
      </>
     );
   }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.getIn(['profiles', 'isLoggedIn'])
  };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    navigate: NavigateActions.navigate
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LiveChallenges);
