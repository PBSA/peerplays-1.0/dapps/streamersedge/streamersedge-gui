import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {NavigateActions} from '../../../actions';
import {withStyles} from '@material-ui/core/styles';
import {GenUtil} from '../../../utility';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import susdImg from '../../../assets/images/dashboard/susd_2x.png';
import dateIcon from '../../../assets/images/dashboard/date-icon.svg';
import pubg_placeholder from '../../../assets/images/dashboard/placeholder/pubg.jpg';
import Avatar from '../../Avatar';

import styles from './MUI.css';
const translate = GenUtil.translate;

class ChallengeCard extends Component {
  handleClick = (event) => {
    event.stopPropagation();

    this.props.navigate(`/challenge/${this.props.id}`);
  }

  formatDate(startDate) {
    //format date to output in the following format: October 29, 2019, hh:mm
    let date, options, formattedDate;
    date = new Date(startDate);
    options = {year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric'};

    if(this.props.timeFormat === '24h') {
      formattedDate = date.toLocaleDateString('en-ZA', options);
    } else {
      formattedDate = date.toLocaleDateString('en-US', options);
    }

    return formattedDate;
  }

  render() {
    const {classes, name, reward, identifier, onCardClick} = this.props;
    const cardClass = 'challenge-' + (identifier ? identifier : 'card');
    let cardMediaClass = 'challenge-card__card';
    let cardImageClass = classes.card;
    let cardRewardClass = 'challenge-card__reward';
    let cardMainClass = 'challenge-card__main';
    let crdBackgroundClass = 'challenge-card__background';
    let challengeTitleClass = 'challenge-card__title';
    let challengeStartingDateClass = 'challenge-card__startingdate';
    let cardDateIconClass = 'challenge-card__dateicon';
    const challengeNameClass = 'challenge-card__name';

    const image = pubg_placeholder;

    // Dynamically assign a class name based on identifier.
    if (identifier === 'card-1') {
      cardMediaClass = 'challenge-card__media-main';
      cardImageClass = classes.cardCarouselMain;
      cardRewardClass = 'challenge-card__reward-carousel';
      cardMainClass = 'challenge-card__main-carousel';
      challengeTitleClass = 'challenge-card__title-carousel';
      challengeStartingDateClass = 'challenge-card__startingdate-carousel';
      cardDateIconClass = 'challenge-card__dateicon-carousel';
      crdBackgroundClass = 'challenge-card__background-carousel';
    } else if (identifier) {
      cardMediaClass = 'challenge-card__media';
      cardImageClass = classes.cardCarousel;
      cardRewardClass = 'challenge-card__reward-carousel';
    }

    return (
      <>
        <div className={ cardClass }>
          <Card className={ cardMediaClass } onClick={ (event)=>this.handleClick(event) }>
            <CardActionArea>
              <CardMedia
                className={ cardImageClass }
                component='img'
                alt={ name }
                height='341'
                width='587'
                image={ image }
                title={ name }
                onClick={ this.handleClick }
              />
              <div className={ crdBackgroundClass }>
                <div className={ cardMainClass } onClick={ onCardClick }>
                  <Avatar avatar={ this.props.avatar } isChallengeAvatar={ true }/>
                  <div className={ challengeTitleClass }>
                    <div className={ challengeNameClass }>{ name }</div>
                    <div >
                      {this.props.status !== 'open' ?
                        <span className={ challengeStartingDateClass }>
                          <img className={ cardDateIconClass } src={ dateIcon } alt=''></img> {translate('dashboard.challenge.started')} {this.formatDate(this.props.startTime)}
                        </span> :
                        <span className={ challengeStartingDateClass }>
                          <img className={ cardDateIconClass } src={ dateIcon } alt=''></img> {translate('dashboard.challenge.starting')} {this.formatDate(this.props.startTime)}
                        </span>
                      }
                    </div>
                  </div>
                </div>
              </div>

              <div className={ cardRewardClass }>
                <img className = 'challenge-card__coin' src={ susdImg } alt='USD'/>
                {translate('dashboard.susd')} {Number(reward).toFixed(2)}
              </div>
            </CardActionArea>
          </Card>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    timeFormat: state.getIn(['profiles', 'currentAccount','timeFormat'])
  };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    navigate: NavigateActions.navigate
  },
  dispatch
);

ChallengeCard.propTypes = {
  id: PropTypes.number.isRequired,
  avatar: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  users: PropTypes.array.isRequired,
  reward: PropTypes.number.isRequired,
  game: PropTypes.string.isRequired,
  startTime: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(ChallengeCard));
