import React, {Component} from 'react';
import {DashboardConstants} from '../../../constants';
import ChallengeCard from '../ChallengeCard';
import ChallengeUtil from '../../../utility/ChallengeUtil';
import {profileDefault} from '../../../assets/images/avatar';
import Slider from 'react-slick';
import './slick.scss';
import {bindActionCreators} from 'redux';
import {NavigateActions} from '../../../actions';
import {connect} from 'react-redux';

class ChallengeCarousel extends Component {
  renderFeatured = () => {
    let {challenges, navigate} = this.props;

    challenges = challenges.filter((challenge) => {
      return challenge.joinedUsers.length > 0;
    }).map((challenge) => {
      const donationSum = ChallengeUtil.getTotalDonations(challenge);
      return {...challenge, reward: donationSum};
    }).sort((a, b) => (a.reward < b.reward) ? 1 : -1).slice(0, DashboardConstants.DASHBOARD_CHALLENGES_MAX-1);

    if (challenges.length < 3) {
      return null;
    }

    challenges = challenges.slice(0,3);

    const NextButton = (props) => {
      const {className, style, onClick} = props;

      return (
        <div
          className={ `${className} carousel__next` }
          style={ {style} }
          onClick={ onClick }
        />
      );
    };

    const PrevButton = (props) => {
      const {className, style, onClick} = props;
      return (
        <div
          className={ `${className} carousel__prev` }
          style={ {style} }
          onClick={ onClick }
        />
      );
    };

    const settings = {
      className: 'center',
      centerMode: true,
      infinite: true,
      centerPadding: '60px',
      slidesToShow: 1,
      speed: 500,
      nextArrow: <NextButton />,
      prevArrow: <PrevButton />
    };

    const handleAvatarClick = (event,id) => {
      const {isLoggedIn}=this.props;

      if(isLoggedIn) {
        event.stopPropagation();
        navigate(`/user-profile/${id}`);
      }
    };

    return (
      <div>
        <Slider { ...settings }>
          {challenges.map((challenge) => {
            const donationSum = ChallengeUtil.getTotalDonations(challenge);

            let avatar = profileDefault;

            if (challenge.user) {
              avatar = challenge.user.avatar;
            }

            return (
              <ChallengeCard
                identifier='card-1'
                onCardClick={ (event)=>{
                  handleAvatarClick(event,challenge.userId);
                } }
                key={ challenge.id }
                id={ challenge.id }
                name={ challenge.name }
                users={ challenge.joinedUsers }
                reward={ donationSum }
                game={ challenge.game }
                avatar={ avatar }
                startTime = { challenge.timeToStart }
                status = { challenge.status }
              />
            );
          })}
        </Slider>
      </div>
    );
  }

  render() {
    if (this.props.challenges.length) {
      return (
        <div className='dashboard__carousel'>
          {this.renderFeatured()}
        </div>
      );
    }

    return null;
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.getIn(['profiles', 'isLoggedIn'])
  };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    navigate: NavigateActions.navigate
  },
  dispatch
);


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChallengeCarousel);
